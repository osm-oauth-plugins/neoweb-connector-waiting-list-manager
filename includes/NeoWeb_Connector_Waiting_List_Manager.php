<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://neoweb.co.uk
 * @since      1.0.0
 *
 * @package    NeoWeb_Connector_Waiting_List_Manager
 * @subpackage NeoWeb_Connector_Waiting_List_Manager/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    NeoWeb_Connector_Waiting_List_Manager
 * @subpackage NeoWeb_Connector_Waiting_List_Manager/includes
 * @author     Jaco Mare <jaco.mare@neoweb.co.uk>
 */
class NeoWeb_Connector_Waiting_List_Manager {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      NeoWeb_Connector_Waiting_List_Manager_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected NeoWeb_Connector_Waiting_List_Manager_Loader $loader;

	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_data = get_option('neoweb-connector-waiting-list-manager');

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - NeoWeb_Connector_Waiting_List_Manager_Loader. Orchestrates the hooks of the plugin.
	 * - NeoWeb_Connector_Waiting_List_Manager_i18n. Defines internationalization functionality.
	 * - NeoWeb_Connector_Waiting_List_Manager_Admin. Defines all hooks for the admin area.
	 * - NeoWeb_Connector_Waiting_List_Manager_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

        /**
         * The class responsible for loading ACF PRO
         * of the plugin.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/NeoWeb_Connector_Waiting_List_Manager_Load_ACF_PRO.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/NeoWeb_Connector_Waiting_List_Manager_Loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/NeoWeb_Connector_Waiting_List_Manager_i18n.php';

        /**
         * The class responsible for loading OSM oAuth Caller
         * of the plugin.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/NeoWeb_Connector_Waiting_List_Manager_Auth_Caller.php';

        /**
		 * The class responsible for loading the OSM API endpoints.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints.php';

		/**
		 * The class responsible for loading the loggers.
		 */
		if (!class_exists('NeoWeb_Connector_Loggers'))
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/NeoWeb_Connector_Loggers.php';

		/**
		 * The class responsible for loading the notification manager.
		 */
		if (!class_exists('NeoWeb_Connector_Admin_Notifications'))
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/NeoWeb_Connector_Admin_Notifications.php';

        /**
         * The class responsible for loading the licence manager.
         */
		if (!class_exists('NeoWeb_Connector_Licence_Manager'))
            require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/NeoWeb_Connector_Licence_Manager.php';

		/**
		 * The class responsible for loading the transient cache manager.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/NeoWeb_Connector_Waiting_List_Manager_Transient_Manager.php';

        /**
         * The class responsible for loading all admin pages.
         */
		if (!class_exists('NeoWeb_Connector_Register_Product_Pages'))
            require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/acf-pages/NeoWeb_Connector_Register_Product_Pages.php';

        /**
         * The class responsible for loading the licence checker.
         */
        if (!class_exists("NeoWeb_Connector_Register_Licence_Page"))
            require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/acf-pages/NeoWeb_Connector_Register_Licence_Page.php';

		/**
		 * The class responsible for loading the debug page.
		 */
		if (!class_exists("NeoWeb_Connector_Register_Debug_Cache_Page"))
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/acf-pages/NeoWeb_Connector_Register_Debug_Cache_Page.php';

		/**
		 * The class responsible for loading the support page.
		 */
		if (!class_exists("NeoWeb_Connector_Register_Support_Page"))
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/acf-pages/NeoWeb_Connector_Register_Support_Page.php';

		/**
		 * The class responsible for loading the OSM Authentication Settings page.
		 */
		if (!class_exists("NeoWeb_Connector_Register_OSM_App_Settings_Page"))
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/acf-pages/NeoWeb_Connector_Register_OSM_App_Settings_Page.php';

		/**
		 * The class responsible for loading the OSM Resources page.
		 */
		if (!class_exists("NeoWeb_Connector_Register_Resources_Page"))
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/acf-pages/NeoWeb_Connector_Register_Resources_Page.php';

		/**
		 * The class responsible for loading the waiting search page.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/acf-pages/NeoWeb_Connector_WaitingList_Search.php';

		/**
		 * The class responsible for loading the waiting submit page.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/acf-pages/NeoWeb_Connector_WaitingList_Submit.php';

		/**
		 * The class responsible for loading the waiting submit page sections.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/acf-pages/NeoWeb_Connector_WaitingList_Submit_Sections.php';

        /**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/NeoWeb_Connector_Waiting_List_Manager_Admin.php';

        /**
		 * The class responsible for defining all admin pages.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/NeoWeb_Connector_Waiting_List_Manager_Admin_Page_Loader.php';

		/**
		 * The class responsible for defining all ajax handlers.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/NeoWeb_Connector_Waiting_List_Manager_AJAX_Callers.php';

		/**
		 * The class responsible for registering shortcode handlers.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/NeoWeb_Connector_Waiting_List_Manager_Register_Shortcodes.php';

		/**
		 * The class responsible for creating forms.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/NeoWeb_Connector_Form_Builder.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/NeoWeb_Connector_Waiting_List_Manager_Public.php';

		$this->loader = new NeoWeb_Connector_Waiting_List_Manager_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the NeoWeb_Connector_Waiting_List_Manager_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new NeoWeb_Connector_Waiting_List_Manager_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new NeoWeb_Connector_Waiting_List_Manager_Admin();

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

        //Trigger authentication once app settings has been saved
        $this->loader->add_action( 'acf/save_post', $plugin_admin, 'trigger_publish_click_scripts', 20);

		$flashNotice = new NeoWeb_Connector_Admin_Notifications();
		// We add our display_flash_notices function to the admin_notices
		$this->loader->add_action( 'admin_notices', $flashNotice, 'display_flash_notices', 12 );

        $plugin_admin_page_loader = new NeoWeb_Connector_Waiting_List_Manager_Admin_Page_Loader();
		$this->loader->add_action( 'admin_init', $plugin_admin_page_loader, 'jp_sync_acf_fields');
		$this->loader->add_action( 'admin_menu', $plugin_admin_page_loader, 'loadAdminPages', 20 );
        $this->loader->add_filter('acf/settings/load_json',$plugin_admin_page_loader,  'loadACFields');
        $this->loader->add_filter('acf/settings/save_json',$plugin_admin_page_loader,  'saveACFields');

		$ajaxCallers = new NeoWeb_Connector_Waiting_List_Manager_AJAX_Callers();
		$this->loader->add_action( 'wp_ajax_trigger_log_refresh_' .  str_replace("-", "_", $this->get_plugin_data('pluginSlug')), $ajaxCallers, 'trigger_log_refresh' );
		$this->loader->add_action( 'wp_ajax_trigger_transient_log_refresh_' .  str_replace("-", "_", $this->get_plugin_data('pluginSlug')), $ajaxCallers, 'trigger_transient_log_refresh' );
		$this->loader->add_action( 'wp_ajax_trigger_cache_refresh_' .  str_replace("-", "_", $this->get_plugin_data('pluginSlug')), $ajaxCallers, 'trigger_cache_refresh' );
		$this->loader->add_action( 'wp_ajax_trigger_create_licence_key_request_' . str_replace("-", "_", $this->get_plugin_data('pluginSlug')), $ajaxCallers, 'trigger_create_licence_key_request' );
		$this->loader->add_action( 'wp_ajax_trigger_activate_licence_key_request_' . str_replace("-", "_", $this->get_plugin_data('pluginSlug')), $ajaxCallers, 'trigger_activate_licence_key_request' );
		$this->loader->add_action( 'wp_ajax_trigger_api_test_' .  str_replace("-", "_", $this->get_plugin_data('pluginSlug')), $ajaxCallers, 'trigger_api_test' );
		$this->loader->add_action( 'wp_ajax_waitingListSearch', $ajaxCallers, 'fetch_osm_waiting_list_data' );
		$this->loader->add_action( 'wp_ajax_nopriv_waitingListSearch', $ajaxCallers, 'fetch_osm_waiting_list_data' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new NeoWeb_Connector_Waiting_List_Manager_Public();
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$registerShortCodes = new NeoWeb_Connector_Waiting_List_Manager_Register_Shortcodes();
        $this->loader->add_shortcode( 'OSM_WaitingList', $registerShortCodes, 'create_waiting_list_search_form' );
        $this->loader->add_shortcode( 'OSM-WaitingList', $registerShortCodes, 'create_waiting_list_search_form' );
        $this->loader->add_shortcode( 'OSM_WaitingList_Count', $registerShortCodes, 'create_waiting_list_count' );
        $this->loader->add_shortcode( 'OSM-WaitingList-Count', $registerShortCodes, 'create_waiting_list_count' );
        $this->loader->add_shortcode( 'OSM_WaitingList_Register', $registerShortCodes, 'create_waiting_list_submit_form' );
        $this->loader->add_shortcode( 'OSM-WaitingList-Register', $registerShortCodes, 'create_waiting_list_submit_form' );
        $this->loader->add_shortcode( 'test', $registerShortCodes, 'test' );

    }

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    NeoWeb_Connector_Waiting_List_Manager_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader(): NeoWeb_Connector_Waiting_List_Manager_Loader {
		return $this->loader;
	}
}
