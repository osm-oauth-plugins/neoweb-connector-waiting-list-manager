<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://neoweb.co.uk
 * @since      1.0.0
 *
 * @package    NeoWeb_Connector_Waiting_List_Manager
 * @subpackage NeoWeb_Connector_Waiting_List_Manager/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    NeoWeb_Connector_Waiting_List_Manager
 * @subpackage NeoWeb_Connector_Waiting_List_Manager/public
 * @author     Jaco Mare <jaco.mare@neoweb.co.uk>
 */
class NeoWeb_Connector_Waiting_List_Manager_Public {
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}


	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		$this->plugin_data = get_option('neoweb-connector-waiting-list-manager');
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in NeoWeb_Connector_Waiting_List_Manager_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The NeoWeb_Connector_Waiting_List_Manager_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->get_plugin_data('pluginName'), plugin_dir_url( __FILE__ ) . 'css/neoweb-connector-waiting-list-manager-public.css', array(), $this->get_plugin_data('pluginVersion'), 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in NeoWeb_Connector_Waiting_List_Manager_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The NeoWeb_Connector_Waiting_List_Manager_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->get_plugin_data('pluginName'), plugin_dir_url( __FILE__ ) . 'js/neoweb-connector-waiting-list-manager-public.js', array( 'jquery' ), $this->get_plugin_data('pluginVersion'), false );

	}

	public function fetchCustomFields ($field, $fieldCounter, $sectionID, $customFieldsValuesNice) {

		$html = "";
		ob_start();

		if ($field['available_custom_fields']['customFields_' . $sectionID] != "0") {
			if ($field['osm_field_type'] == 'text') { ?>

				<div class="form-group">
					<label for="Field_<?php echo $fieldCounter; ?>"><?php echo $field['osm_field_label']; ?>:<?php echo ($field['required_field'] == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
					<input type="text" class="form-control" name="Field_<?php echo $fieldCounter; ?>" id="Field_<?php echo $fieldCounter; ?>" placeholder="<?php echo $field['osm_field_placeholder']; ?>" <?php echo ($field['required_field'] == 1 ? 'required' : ''); ?> value="<?php echo ($customFieldsValuesNice > "" ? $customFieldsValuesNice[$field['osm_field_label']] : "" ); ?>">
					<?php if ($field['required_field'] == 1) { ?>
						<div class="invalid-feedback">
							This is a required field
						</div>
					<?php } ?>
				</div>
				<input type="hidden" name="FieldID_<?php echo $fieldCounter; ?>" id="FieldID_<?php echo $fieldCounter; ?>" value="<?php echo $field['available_custom_fields']['customFields_' . $sectionID]; ?>">

			<?php } else if ($field['osm_field_type'] == 'number') { ?>

				<div class="form-group">
					<label for="Field_<?php echo $fieldCounter; ?>"><?php echo $field['osm_field_label']; ?>:<?php echo ($field['required_field'] == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
					<input type="number" class="form-control" name="Field_<?php echo $fieldCounter; ?>" id="Field_<?php echo $fieldCounter; ?>" placeholder="<?php echo $field['osm_field_placeholder']; ?>" <?php echo ($field['required_field'] == 1 ? 'required' : ''); ?> value="<?php echo ($customFieldsValuesNice > "" ? $customFieldsValuesNice[$field['osm_field_label']] : "" ); ?>">
					<?php if ($field['required_field'] == 1) { ?>
						<div class="invalid-feedback">
							This is a required field
						</div>
					<?php } ?>
				</div>
				<input type="hidden" name="FieldID_<?php echo $fieldCounter; ?>" id="FieldID_<?php echo $fieldCounter; ?>" value="<?php echo $field['available_custom_fields']['customFields_' . $sectionID]; ?>">

			<?php } else if ($field['osm_field_type'] == 'select') { ?>

				<div class="form-group">
					<label for="Field_<?php echo $fieldCounter; ?>"><?php echo $field['osm_field_label']; ?>:<?php echo ($field['required_field'] == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
					<select id="Field_<?php echo $fieldCounter; ?>" name="Field_<?php echo $fieldCounter; ?>" class="form-control" <?php echo ($field['required_field'] == 1 ? 'required' : ''); ?>>
						<option value="">Please select...</option>
						<?php foreach ($field['osm_field_values'] as $fieldValue) { ?>
							<option value="<?php echo $fieldValue['text_value']; ?>" <?php if ($customFieldsValuesNice > "") if ($customFieldsValuesNice[$field['osm_field_label']] > "") echo ' selected="selected" '; ?>><?php echo $fieldValue['text_value']; ?></option>
						<?php } ?>
					</select>
					<?php if ($field['required_field'] == 1) { ?>
						<div class="invalid-feedback">
							This is a required field
						</div>
					<?php } ?>
				</div>
				<input type="hidden" name="FieldID_<?php echo $fieldCounter; ?>" id="FieldID_<?php echo $fieldCounter; ?>" value="<?php echo $field['available_custom_fields']['customFields_' . $sectionID]; ?>">

			<?php } else if ($field['osm_field_type'] == 'multiselect') { ?>

				<div class="form-group">
					<label for="Field_<?php echo $fieldCounter; ?>"><?php echo $field['osm_field_label']; ?>:<?php echo ($field['required_field'] == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
					<select multiple id="Field_<?php echo $fieldCounter; ?>" name="Field_<?php echo $fieldCounter; ?>[]" class="form-control" <?php echo ($field['required_field'] == 1 ? 'required' : ''); ?>>

						<?php foreach ($field['osm_field_values'] as $fieldValue) { ?>
							<option value="<?php echo $fieldValue['text_value']; ?>" <?php if ($customFieldsValuesNice > "") if ($customFieldsValuesNice[$field['osm_field_label']] > "") echo ' selected="selected" '; ?>><?php echo $fieldValue['text_value']; ?></option>
						<?php } ?>

					</select>
					<?php if ($field['required_field'] == 1) { ?>
						<div class="invalid-feedback">
							This is a required field
						</div>
					<?php } ?>
				</div>
				<input type="hidden" name="FieldID_<?php echo $fieldCounter; ?>" id="FieldID_<?php echo $fieldCounter; ?>" value="<?php echo $field['available_custom_fields']['customFields_' . $sectionID]; ?>">

			<?php }
		}
		$html .= ob_get_clean();
		return $html;

	}

}
