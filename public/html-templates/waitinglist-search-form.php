<?php if ($showCount == 1) { ?>
<div class="alert alert-counter" role="alert">
	<?php echo $countMsg; ?>
	<?php echo $counter; ?>
</div>
<?php } ?>


<div class="input-group">
	<span class="input-group-addon" id="waitingList-search-addon"><i class="fa fa-search"></i></span>
	<input class="form-control" type="text" id="search<?php echo $sectionID; ?>" name="search" placeholder="Search" />
</div>
<input type="hidden" id="sectionid" name="sectionid" value="<?php echo $sectionID; ?>" />
<input type="hidden" id="action" name="action" val="waitingList" />
<!-- Suggestions will be displayed in below div. -->
<div class="searchResults">
	<div class="resultMsg">
		<?php echo $resultMsg; ?>
	</div>
	<div id="display<?php echo $sectionID; ?>">

	</div>
</div>
<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small >Powered by: <a href="http://www.scoutsuk.org" target="_blank">NeoWeb Connector</a></small></p>

<script>
	
	jQuery(document).ready(function() {
		jQuery("#search<?php echo $sectionID; ?>").keyup(function() {
			jQuery('#display<?php echo $sectionID; ?>').html('');
			var name = jQuery('#search<?php echo $sectionID; ?>').val();
			if (name == "") {
				jQuery("#display<?php echo $sectionID; ?>").html("");
			} else {
				jQuery.ajax({
					type: "post",
					dataType: "json",
					url: ajax_var.url,
					data: {
						action: "waitingListSearch",
						sectionid: jQuery('#sectionid').val(),
						search: name
					},
					success: function(response) {
						jQuery('#display<?php echo $sectionID; ?>').html('');
						$html = '<div class="list-group">';
						jQuery.each(response, function($key, $yp) {

							$html += '<div class="list-group-item">';
                            $html +=    '<div class="d-flex w-100 justify-content-between">';
                            $html +=        '<span><span class="firstname">' + $yp.firstname + '</span><span class="lastname"> ' + $yp.lastname + '</span></span>';

                            //Show the members DOB
                            <?php if ($showDOB == 1) { ?>
                                $html +=        '<span class="pull-right"><strong>Date of birth:</strong> <span class="dob">' + $yp.dob + '</span></span>';
                            <?php } ?>

                            $html +=    '</div>';

                            //Show the members Joining Date
                            <?php if ($showJoining == 1) { ?>
                            $html +=    '<p class="mb-1"><strong>Expected Joining Date:</strong> <span class="joined">' + $yp.joined + '</span></p>';
                            <?php } ?>

                            //Show member Additional Detail
                            <?php if ($showCustomData == 1) { ?>
                                if ($yp.customField.length) {
                                    $html +=    '<p class="mb-1"><strong>Additional Information:</strong> <span class="customFields">' + $yp.customField + '</span></p>';
                                }
                            <?php } ?>

                            $html += '</div>';

						});
						$html += '</div>';
						jQuery('#display<?php echo $sectionID; ?>').append($html);
					},
					error: function(error_response) {
						jQuery('#display<?php echo $sectionID; ?>').html('');
						jQuery('#display<?php echo $sectionID; ?>').html(error_response.responseText);
					}
				});
			}
		});
	});
</script>