<?php

$waiting_list_form = array();

$formSections = array(
    'member' => array(
        "sectionHeading" => array(
	        "type" => "heading",
            "heading" => "Section Heading",
	        "label" => "Member Details"
        ),
        "firstName" => array(
            "label" => "First name", "placeholder" => "", "required" => true, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "lastName" => array(
            "label" => "Last name", "placeholder" => "", "required" => true, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "dateOfBirth" => array(
            "label" => "Date of birth", "placeholder" => "", "required" => true, "error-msg" => "", "show-datepicker" => true, "redact" => false, "redact-parent" => false, "type" => "date"
        ),
        "gender" => array(
            "visible" => false, "label" => "Gender", "placeholder" => "", "required" => false, "error-msg" => "", "redact" => false, "redact-parent" => false, "type" => "select",
	        "options" => array(
		        ''              => 'Please select...',
		        'female'        => 'Female',
		        'male'          => 'Male',
		        'self-identify' => 'Self Identify',
		        'not-said'      => 'Prefer not to say',
		        'other'         => 'Other',
		        'unspecified'   => 'Unspecified'
	        )
        ),
        "addressLine1" => array(
            "visible" => false, "label" => "Address line 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine2" => array(
            "visible" => false, "label" => "Address line 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine3" => array(
            "visible" => false, "label" => "Address line 3", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine4" => array(
            "visible" => false, "label" => "Address line 4", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "postcode" => array(
            "visible" => false, "label" => "Postcode", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "phoneNumber1" => array(
            "visible" => false, "label" => "Phone number 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "tel"
        ),
        "phoneNumber2" => array(
	        "visible" => false, "label" => "Phone number 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "tel"
        ),
        "emailAddress1" => array(
	        "visible" => false, "label" => "Email address 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "email"
        ),
        "emailAddress2" => array(
            "visible" => false, "label" => "Email address 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "email"
        )
    ),
    'primary' => array(
        "sectionHeading" => array(
	        "type" => "heading",
            "heading" => "Section Heading",
            "label" => "Primary Contact Details"
        ),
        "firstName" => array(
            "visible" => false, "label" => "First name", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "lastName" => array(
            "visible" => false, "label" => "Last name", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine1" => array(
            "visible" => false, "label" => "Address line 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine2" => array(
            "visible" => false, "label" => "Address line 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine3" => array(
            "visible" => false, "label" => "Address line 3", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine4" => array(
            "visible" => false, "label" => "Address line 4", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "postcode" => array(
            "visible" => false, "label" => "Postcode", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "phoneNumber1" => array(
            "visible" => false, "label" => "Phone number 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "tel"
        ),
        "phoneNumber2" => array(
            "visible" => false, "label" => "Phone number 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "tel"
        ),
        "emailAddress1" => array(
            "visible" => false, "label" => "Email address 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "email"
        ),
        "emailAddress2" => array(
            "visible" => false, "label" => "Email address 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "email"
        )
    ),
    'secondary' => array(
        "sectionHeading" => array(
	        "type" => "heading",
            "heading" => "Section Heading",
            "label" => "Secondary Contact Details"
        ),
        "firstName" => array(
	        "visible" => false, "label" => "First name", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "lastName" => array(
	        "visible" => false, "label" => "Last name", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine1" => array(
	        "visible" => false, "label" => "Address line 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine2" => array(
	        "visible" => false, "label" => "Address line 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine3" => array(
	        "visible" => false, "label" => "Address line 3", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine4" => array(
	        "visible" => false, "label" => "Address line 4", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "postcode" => array(
	        "visible" => false, "label" => "Postcode", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "phoneNumber1" => array(
	        "visible" => false, "label" => "Phone number 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "tel"
        ),
        "phoneNumber2" => array(
	        "visible" => false, "label" => "Phone number 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "tel"
        ),
        "emailAddress1" => array(
	        "visible" => false, "label" => "Email address 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "email"
        ),
        "emailAddress2" => array(
	        "visible" => false, "label" => "Email address 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "email"
        )
    ),
    'emergency' => array(
        "sectionHeading" => array(
	        "type" => "heading",
            "heading" => "Section Heading",
            "label" => "Emergency Contact Details"
        ),
        "firstName" => array(
	        "visible" => false, "label" => "First name", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "lastName" => array(
	        "visible" => false, "label" => "Last name", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine1" => array(
	        "visible" => false, "label" => "Address line 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine2" => array(
	        "visible" => false, "label" => "Address line 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine3" => array(
	        "visible" => false, "label" => "Address line 3", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine4" => array(
	        "visible" => false, "label" => "Address line 4", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "postcode" => array(
	        "visible" => false, "label" => "Postcode", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "phoneNumber1" => array(
	        "visible" => false, "label" => "Phone number 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "tel"
        ),
        "phoneNumber2" => array(
	        "visible" => false, "label" => "Phone number 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "tel"
        ),
        "emailAddress1" => array(
	        "visible" => false, "label" => "Email address 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "email"
        ),
        "emailAddress2" => array(
	        "visible" => false, "label" => "Email address 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "email"
        )
    ),
    'doctor' => array(
        "sectionHeading" => array(
	        "type" => "heading",
            "heading" => "Section Heading",
            "label" => "Surgery Details"
        ),
        "firstName" => array(
	        "visible" => false, "label" => "First name", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "lastName" => array(
	        "visible" => false, "label" => "Last name", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "Surgery" => array(
            "visible" => false, "label" => "Surgery", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine1" => array(
	        "visible" => false, "label" => "Address line 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine2" => array(
	        "visible" => false, "label" => "Address line 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine3" => array(
	        "visible" => false, "label" => "Address line 3", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "addressLine4" => array(
	        "visible" => false, "label" => "Address line 4", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "postcode" => array(
	        "visible" => false, "label" => "Postcode", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "text"
        ),
        "Phone Number 1" => array(
            "visible" => false, "label" => "Phone Number 1", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "tel"
        ),
        "Phone Number 2" => array(
            "visible" => false, "label" => "Phone Number 2", "placeholder" => "", "required" => false, "error-msg" => "", "min" => "", "max" => "", "redact" => false, "redact-parent" => false, "type" => "tel"
        )
    )
);

$fieldValues = array();

//print_r(acf_get_field_groups());

echo '<br>';
echo get_field('field_defaults_member_sectionHeading', 'options');
the_field('field_defaults_member_section_firstName', 'options');

echo '<br>';
if( have_rows('group_defaults_member_firstName', 'options') ):
	while( have_rows('group_defaults_member_firstName', 'options') ): the_row();
		echo get_sub_field('field_defaults_member_firstName_label');
		echo get_sub_field('field_defaults_member_firstName_placeholder');
	endwhile;
endif;

echo '<br>';

the_field('field_defaults_custom_sectionHeading', 'options');
the_field('field_defaults_custom_fields', 'options');

print_r(get_field('field_defaults_custom_fields', 'options'));

if( have_rows('field_defaults_custom_fields', 'options') ):
	while( have_rows('field_defaults_custom_fields', 'options') ): the_row();
		echo get_sub_field('field_defaults_label');
		echo get_sub_field('field_defaults_label');
	endwhile;
endif;




foreach ( $formSections as $formSection => $formSectionFields ) {
	foreach ( $formSectionFields as $fieldID  => $fieldDataSets ) {


			$defaultFormSectionID = 'field_defaults_' . $formSection . '_' . $fieldID;
			$overrideFormSectionID = 'field_' . $sectionID . '_' . $formSection . '_' . $fieldID;


			echo $defaultFormSectionID;
			echo '<br>';

			print_r(get_field($defaultFormSectionID, 'option'));
			echo '<br>';

			if( have_rows($defaultFormSectionID, 'option') ):
				while( have_rows($defaultFormSectionID, 'option') ): the_row();

				endwhile;
			endif;

			/*
			if (get_field($defaultsFormFieldID, 'option')) {
				$fieldValues[$formSection . '_' . $fieldID . '_' . $field_data_set_id ]   = get_field( $defaultsFormFieldID, 'option' );
            } else if (get_field($overrideFormFieldID, 'option')) {
                $fieldValues[$formSection . '_' . $fieldID . '_' . $field_data_set_id ] = get_field( $overrideFormFieldID, 'option' );
            } else {
				$fieldValues[$formSection . '_' . $fieldID . '_' . $field_data_set_id] = $field_data_set_value;
			}
			*/
	}
}

foreach ( $formSections as $formSection => $formSectionFields ) {
	foreach ( $formSectionFields as $fieldID  => $fieldDataSets ) {
		foreach ( $fieldDataSets as $field_data_set_id => $field_data_set_value ) {

			$defaultFormSectionID = 'field_defaults_' . $formSection . '_' . $fieldID;
			$overrideFormSectionID = 'field' . $sectionID . '_' . $formSection . '_' . $fieldID;

            //Check if the section is visible
            $defaultsFormSectionVisible = 'field_defaults_' . $formSection . '_visible';
            $overrideFormSectionVisible = 'field_' . $sectionID . '_' . $formSection . '_visible';


            //Check idf the field is visible
            $defaultsFormFieldVisible = $defaultFormSectionID .$defaultFormSectionID . '_visible';
            $overrideFormFieldVisible = $overrideFormSectionID .$overrideFormSectionID . '_visible';

			if ($formSection == 'member' && ($fieldID == 'sectionHeading' || $fieldID == 'firstName' || $fieldID == 'lastName' || $fieldID == 'dateOfBirth')) {
				$waiting_list_form[ $formSection . '_' . $fieldID ] = array(
					'title'         => (isset($fieldValues[$formSection . '_' . $fieldID . '_label']) ? $fieldValues[$formSection . '_' . $fieldID . '_label'] : ""),
					'type'          => (isset($fieldValues[$formSection . '_' . $fieldID . '_type']) ? $fieldValues[$formSection . '_' . $fieldID . '_type'] : ""),
					'placeholder'   => (isset($fieldValues[$formSection . '_' . $fieldID . '_placeholder']) ? $fieldValues[$formSection . '_' . $fieldID . '_placeholder'] : ""),
					'class'         => 'form-control',
					'required'      => (isset($fieldValues[$formSection . '_' . $fieldID . '_required']) ? $fieldValues[$formSection . '_' . $fieldID . '_required'] : ""),
					'validations'   => array( 'not_empty' ),
					'options'       => (isset($fieldValues[$formSection . '_' . $fieldID . '_options']) ? $fieldValues[$formSection . '_' . $fieldID . '_options'] : ""),
				);
			} else if (get_field($defaultsFormSectionVisible, 'option') || get_field($overrideFormSectionVisible, 'option')) {

				echo $formSection;
				echo '<br>';
				echo $fieldID;
				echo '<br>';

				if( have_rows($defaultFormSectionID, 'option') ):
					while( have_rows($defaultFormSectionID, 'option') ): the_row();

						echo get_sub_field('field_defaults_primary_firstName_visible');
						echo get_sub_field('field_defaults_primary_firstName_label');

					endwhile;
				endif;



				/*
				$waiting_list_form[ $formSection . '_' . $fieldID ] = array(
					'title'         => (isset($fieldValues[$formSection . '_' . $fieldID . '_label']) ? $fieldValues[$formSection . '_' . $fieldID . '_label'] : ""),
					'type'          => (isset($fieldValues[$formSection . '_' . $fieldID . '_type']) ? $fieldValues[$formSection . '_' . $fieldID . '_type'] : ""),
					'placeholder'   => (isset($fieldValues[$formSection . '_' . $fieldID . '_placeholder']) ? $fieldValues[$formSection . '_' . $fieldID . '_placeholder'] : ""),
					'class'         => 'form-control',
					'required'      => (isset($fieldValues[$formSection . '_' . $fieldID . '_required']) ? $fieldValues[$formSection . '_' . $fieldID . '_required'] : ""),
					'validations'   => array( 'not_empty' ),
					'options'       => (isset($fieldValues[$formSection . '_' . $fieldID . '_options']) ? $fieldValues[$formSection . '_' . $fieldID . '_options'] : ""),
				);*/

			}
		}
	}
}

//defaults_custom_capture
$defaultFormSectionID = 'defaults_custom';
$overrideFormSectionID = $sectionID . '_custom';

$defaultsFormFieldID = $defaultFormSectionID . '_capture';
$overrideFormFieldID = $overrideFormSectionID . '_capture';

if (get_field($defaultsFormFieldID, 'option') == 1 || get_field($overrideFormFieldID, 'option') == 1) {

	//Add the section heading
	//defaults_custom_sectionHeading
	$waiting_list_form['custom_sectionHeading'] = array(
		'title'         => (get_field($sectionID . '_custom_sectionHeading', 'option') ? get_field($sectionID . '_custom_sectionHeading', 'option') : (get_field('defaults_custom_sectionHeading', 'option') ? get_field('defaults_custom_sectionHeading', 'option') : 'Additional Details')),
		'type'          => 'heading',
		'placeholder'   => '',
		'class'         => 'form-control',
		'required'      => '',
		'validations'   => array(),
		'options'       => '',
	);

	var_dump(get_field('defaults_custom_fields', 'option'));

    if( have_rows('defaults_custom_fields', 'option') ):
        while( have_rows('defaults_custom_fields', 'option') ) : the_row();
            $title = get_sub_field('defaults_label');
            $placeholder = get_sub_field('defaults_placeholder');
            $required = get_sub_field('defaults_required');
            $error = get_sub_field('defaults_error');
            $min = get_sub_field('defaults_min');
            $max = get_sub_field('defaults_max');
            $type = get_sub_field('defaults_field_type');

            if ($type == 'multiselect') {
            	$type = 'select';
            	//Todo: Add a multi select attribute to the generated select field
            }

            $options = array();

	        if( have_rows('defaults_osm_options', 'options') ):
		        while( have_rows('defaults_osm_options', 'options') ) : the_row();
                    array_push($options, get_sub_field('defaults_osm_field_options'));
                endwhile;
            endif;


			$waiting_list_form[ 'customMappings_' . str_replace(" ", "", $title) ] = array(
				'title'         => $title,
				'type'          => $type,
				'placeholder'   => $placeholder,
				'class'         => 'form-control',
				'required'      => $required,
				'validations'   => array( 'not_empty' ),
				'options'       => $options,
			);


        endwhile;

    else :

    endif;
}


//Repeater:
//defaults_custom_fields

	//Fields:
	//

	//Repeater
		//defaults_osm_fields
		//defaults_osm_field_options

		//BLOCK
		//defaults_field_map
		//50652_field_mapping




	$waiting_list_form['submit'] = array(
		'title' => 'Submit me!',
		'class' => 'btn btn-primary',
		'type' => 'submit',
	);

    $form = new NeoWeb_Connector_Form_Builder($waiting_list_form);
    $form_html = $form->build();

    echo $form_html;