<!doctype html>
<html>
<head>
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Waiting List Confirmation</title>
	<style>
		/* -------------------------------------
			GLOBAL RESETS
		------------------------------------- */
		img {
			border: none;
			-ms-interpolation-mode: bicubic;
			max-width: 100%; }
		body {
			background-color: #f6f6f6;
			font-family: sans-serif;
			-webkit-font-smoothing: antialiased;
			font-size: 14px;
			line-height: 1.4;
			margin: 0;
			padding: 0;
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%; }
		table {
			border-collapse: separate;
			mso-table-lspace: 0pt;
			mso-table-rspace: 0pt;
			width: 100%; }
		table td {
			font-family: sans-serif;
			font-size: 14px;
			vertical-align: top; }
		/* -------------------------------------
			BODY & CONTAINER
		------------------------------------- */
		.body {
			background-color: #f6f6f6;
			width: 100%; }
		/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
		.container {
			display: block;
			Margin: 0 auto !important;
			/* makes it centered */
			max-width: 580px;
			padding: 10px;
			width: 580px; }
		/* This should also be a block element, so that it will fill 100% of the .container */
		.content {
			box-sizing: border-box;
			display: block;
			Margin: 0 auto;
			max-width: 580px;
			padding: 10px; }
		/* -------------------------------------
			HEADER, FOOTER, MAIN
		------------------------------------- */
		.main {
			background: #ffffff;
			border-radius: 3px;
			width: 100%; }
		.wrapper {
			box-sizing: border-box;
			padding: 20px; }
		.content-block {
			padding-bottom: 10px;
			padding-top: 10px;
		}
		.footer {
			clear: both;
			Margin-top: 10px;
			text-align: center;
			width: 100%; }
		.footer td,
		.footer p,
		.footer span,
		.footer a {
			color: #999999;
			font-size: 12px;
			text-align: center; }
		/* -------------------------------------
			TYPOGRAPHY
		------------------------------------- */
		h1,
		h2,
		h3,
		h4 {
			color: #000000;
			font-family: sans-serif;
			font-weight: 400;
			line-height: 1.4;
			margin: 0;
			Margin-bottom: 30px; }
		h1 {
			font-size: 35px;
			font-weight: 300;
			text-align: center;
			text-transform: capitalize; }
		p,
		ul,
		ol {
			font-family: sans-serif;
			font-size: 14px;
			font-weight: normal;
			margin: 0;
			Margin-bottom: 15px; }
		p li,
		ul li,
		ol li {
			list-style-position: inside;
			margin-left: 5px; }
		a {
			color: #3498db;
			text-decoration: underline; }
		/* -------------------------------------
			BUTTONS
		------------------------------------- */
		.btn {
			box-sizing: border-box;
			width: 100%; }
		.btn > tbody > tr > td {
			padding-bottom: 15px; }
		.btn table {
			width: auto; }
		.btn table td {
			background-color: #ffffff;
			border-radius: 5px;
			text-align: center; }
		.btn a {
			background-color: #ffffff;
			border: solid 1px #3498db;
			border-radius: 5px;
			box-sizing: border-box;
			color: #3498db;
			cursor: pointer;
			display: inline-block;
			font-size: 14px;
			font-weight: bold;
			margin: 0;
			padding: 12px 25px;
			text-decoration: none;
			text-transform: capitalize; }
		.btn-primary table td {
			background-color: #3498db; }
		.btn-primary a {
			background-color: #3498db;
			border-color: #3498db;
			color: #ffffff; }
		/* -------------------------------------
			OTHER STYLES THAT MIGHT BE USEFUL
		------------------------------------- */
		.last {
			margin-bottom: 0; }
		.first {
			margin-top: 0; }
		.align-center {
			text-align: center; }
		.align-right {
			text-align: right; }
		.align-left {
			text-align: left; }
		.clear {
			clear: both; }
		.mt0 {
			margin-top: 0; }
		.mb0 {
			margin-bottom: 0; }
		.preheader {
			color: transparent;
			display: none;
			height: 0;
			max-height: 0;
			max-width: 0;
			opacity: 0;
			overflow: hidden;
			mso-hide: all;
			visibility: hidden;
			width: 0; }
		.powered-by a {
			text-decoration: none; }
		hr {
			border: 0;
			border-bottom: 1px solid #f6f6f6;
			Margin: 20px 0; }
		/* -------------------------------------
			RESPONSIVE AND MOBILE FRIENDLY STYLES
		------------------------------------- */
		@media only screen and (max-width: 620px) {
			table[class=body] h1 {
				font-size: 28px !important;
				margin-bottom: 10px !important; }
			table[class=body] p,
			table[class=body] ul,
			table[class=body] ol,
			table[class=body] td,
			table[class=body] span,
			table[class=body] a {
				font-size: 16px !important; }
			table[class=body] .wrapper,
			table[class=body] .article {
				padding: 10px !important; }
			table[class=body] .content {
				padding: 0 !important; }
			table[class=body] .container {
				padding: 0 !important;
				width: 100% !important; }
			table[class=body] .main {
				border-left-width: 0 !important;
				border-radius: 0 !important;
				border-right-width: 0 !important; }
			table[class=body] .btn table {
				width: 100% !important; }
			table[class=body] .btn a {
				width: 100% !important; }
			table[class=body] .img-responsive {
				height: auto !important;
				max-width: 100% !important;
				width: auto !important; }}
		/* -------------------------------------
			PRESERVE THESE STYLES IN THE HEAD
		------------------------------------- */
		@media all {
			.ExternalClass {
				width: 100%; }
			.ExternalClass,
			.ExternalClass p,
			.ExternalClass span,
			.ExternalClass font,
			.ExternalClass td,
			.ExternalClass div {
				line-height: 100%; }
			.apple-link a {
				color: inherit !important;
				font-family: inherit !important;
				font-size: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
				text-decoration: none !important; }
			.btn-primary table td:hover {
				background-color: #34495e !important; }
			.btn-primary a:hover {
				background-color: #34495e !important;
				border-color: #34495e !important; } }
	</style>
</head>
<body class="">
<table border="0" cellpadding="0" cellspacing="0" class="body">
	<tr>
		<td>&nbsp;</td>
		<td class="container">
			<div class="content">

				<!-- START CENTERED WHITE CONTAINER -->
				<?php if ($autoSubmit == 1) { ?>
					<span class="preheader">The following information was automatically submitted to OSM.</span>
				<?php } else { ?>
					<span class="preheader">The following information has been submitted on your website and needs to be added to OSM.</span>
				<?php } ?>

				<table class="main">

					<!-- START MAIN CONTENT AREA -->
					<tr>
						<td class="wrapper">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>

										<?php if ($autoSubmit == 1) { ?>
											<p>The following information was automatically submitted to OSM. This is a copy purely for your information.</p>
										<?php } else { ?>
											<p>The following information has been submitted on your website and needs to be added to OSM.</p>
										<?php } ?>

										<h5>Members's Details:</h5>
										<table border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td width="50%">
													First name:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('member_FirstName_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['member_FirstName'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
													Last name:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('member_LastName_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['member_LastName'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
													Date of birth:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('member_DateofBirth_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['member_DateofBirth'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
													Gender:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('member_Gender_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                        echo ($redactField == 0 ? $formData['member_Gender'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
													Waiting list join date:
												</td>
												<td>
													<?php echo $formData['joinDate']; ?>
												</td>
											</tr>
											</tbody>
										</table>


                                        <?php $CaptureGroup = get_field('member_CaptureSection_' . $sectionID, 'option');?>
                                        <?php if ($CaptureGroup == 1) { ?>
                                        <h5>Member's contact details:</h5>
										<table border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td width="50%">
                                                    Address line1:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('member_AddressLine1_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['member_AddressLine1'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Address line2:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('member_AddressLine2_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['member_AddressLine2'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Postcode:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('member_Postcode_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['member_Postcode'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Mobile phone:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('member_MobilePhoneNumber_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['member_MobilePhoneNumber'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Home Phone number:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('member_PhoneNumber_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['member_PhoneNumber'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
													Email Address:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('member_EmailAddress1_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['member_EmailAddress1'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
													Email Address2:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('member_EmailAddress2_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['member_EmailAddress2'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											</tbody>
										</table>
                                        <?php } ?>

                                        <?php $CaptureGroup = get_field('primary_CaptureSection_', $sectionID, 'option');?>
                                        <?php if ($CaptureGroup == 1) { ?>
                                        <h5>Primary contact's details:</h5>
										<table border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td width="50%">
                                                    First name:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('primary_FirstName_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['primary_FirstName'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Last name:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('primary_LastName_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['primary_LastName'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Address line1:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('primary_AddressLine1_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['primary_AddressLine1'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Address line2:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('primary_AddressLine2_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['primary_AddressLine2'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Postcode:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('primary_Postcode_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['primary_Postcode'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Mobile phone:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('primary_MobilePhoneNumber_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['primary_MobilePhoneNumber'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Home Phone number:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('primary_PhoneNumber_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['primary_PhoneNumber'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
													Email Address:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('primary_EmailAddress1_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['primary_EmailAddress1'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
													Email Address2:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('primary_EmailAddress2_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['primary_EmailAddress2'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											</tbody>
										</table>
                                        <?php } ?>

                                        <?php $CaptureGroup = get_field('secondary_CaptureSection_', $sectionID, 'option');?>
                                        <?php if ($CaptureGroup == 1) { ?>
                                        <h5>Secondary contact's details:</h5>
										<table border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td width="50%">
                                                    First name:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('secondary_FirstName_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['secondary_FirstName'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Last name:
												</td>
												<td>

                                                    <?php
                                                    $redactField = get_field('secondary_LastName_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['secondary_LastName'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Address line1:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('secondary_AddressLine1_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['secondary_AddressLine1'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Address line2:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('secondary_AddressLine2_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['secondary_AddressLine2'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Postcode:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('secondary_Postcode_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['secondary_Postcode'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Mobile phone:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('secondary_MobilePhoneNumber_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['secondary_MobilePhoneNumber'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Home Phone number:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('secondary_PhoneNumber_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['secondary_PhoneNumber'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
													Email Address:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('secondary_EmailAddress1_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['secondary_EmailAddress1'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
													Email Address2:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('secondary_EmailAddress2_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['secondary_EmailAddress2'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											</tbody>
										</table>
                                        <?php } ?>

                                        <?php $CaptureGroup = get_field('emergency_CaptureSection_', $sectionID, 'option');?>
                                        <?php if ($CaptureGroup == 1) { ?>
                                        <h5>Emergency contact's details:</h5>
										<table border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td width="50%">
                                                    First name:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('emergency_FirstName_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['emergency_FirstName'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Last name:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('emergency_LastName_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['emergency_LastName'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Address line1:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('emergency_AddressLine1_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['emergency_AddressLine1'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Address line2:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('emergency_AddressLine2_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['emergency_AddressLine2'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Postcode:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('emergency_Postcode_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['emergency_Postcode'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Mobile phone:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('emergency_MobilePhoneNumber_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['emergency_MobilePhoneNumber'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Home Phone number:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('emergency_PhoneNumber_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['emergency_PhoneNumber'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
													Email Address:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('emergency_EmailAddress1_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['emergency_EmailAddress1'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
													Email Address2:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('emergency_EmailAddress2_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['emergency_EmailAddress2'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											</tbody>
										</table>
                                        <?php } ?>

                                        <?php $CaptureGroup = get_field('doctor_CaptureSection_', $sectionID, 'option');?>
                                        <?php if ($CaptureGroup == 1) { ?>
                                        <h5>Doctors contact's details:</h5>
										<table border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<tr>
												<td width="50%">
                                                    First name:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('doctor_FirstName_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['doctor_FirstName'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Last name:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('doctor_LastName_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['doctor_LastName'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Doctors Surgery:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('doctor_Surgery_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['doctor_Surgery'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Address line1:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('doctor_AddressLine1_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['doctor_AddressLine1'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Address line2:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('doctor_AddressLine2_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['doctor_AddressLine2'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Postcode:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('doctor_Postcode_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['doctor_Postcode'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Phone number:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('doctor_PhoneNumber1_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['doctor_PhoneNumber1'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											<tr>
												<td width="50%">
                                                    Phone number 2:
												</td>
												<td>
                                                    <?php
                                                    $redactField = get_field('doctor_PhoneNumber2_' . $sectionID . '_redact_from_admin_email' , 'option');
                                                    echo ($redactField == 0 ? $formData['doctor_PhoneNumber2'] : 'REDACTED');
                                                    ?>
												</td>
											</tr>
											</tbody>
										</table>
                                        <?php } ?>

										<h5>Additional details:</h5>
										<table border="0" cellpadding="0" cellspacing="0">
											<tbody>
											<?php foreach ($customFieldsValues as $key=>$value) { ?>
												<tr>
													<td width="50%">
														<?php echo $key; ?>:
													</td>
													<td>
														<?php
														if (is_array($value)) {
															foreach ($value as $fieldValue) {
																echo $fieldValue . '<br/>';
															}
														} else {
															echo $value;
														}
														?>
													</td>
												</tr>
											<?php } ?>
											<?php if (get_field("show_tandc_field", "options") == 1) { ?>
												<tr>
													<td width="50%">
														Accepted terms and conditions
													</td>
													<td>
														<?php echo ($tandc == "on" ? "YES" : "NO"); ?>
													</td>
												</tr>
											<?php } ?>
											</tbody>
										</table>

									</td>
								</tr>
							</table>
						</td>
					</tr>

					<!-- END MAIN CONTENT AREA -->
				</table>

				<!-- START FOOTER -->
				<div class="footer">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="content-block">

							</td>
						</tr>
						<tr>
							<td class="content-block powered-by">
								Powered by <a href="https://scoutsuk.org">NeoWeb OSM Authenticator</a>.
							</td>
						</tr>
					</table>
				</div>
				<!-- END FOOTER -->

				<!-- END CENTERED WHITE CONTAINER -->
			</div>
		</td>
		<td>&nbsp;</td>
	</tr>
</table>
</body>
</html>