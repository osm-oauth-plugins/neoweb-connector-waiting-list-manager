<?php
$recaptcha = get_field('recaptcha', 'options');
if (isset($recaptcha['googleSiteKey'])) {

    $requireOldCaptcha = $recaptcha['load_old_capthca'];
    $googleSiteKey = $recaptcha['googleSiteKey'];
    if ($requireOldCaptcha == 0) { ?>
        <script src='https://www.google.com/recaptcha/api.js?render=<?php echo $googleSiteKey; ?>'></script>
    <?php } else { ?>
        <script src='https://www.google.com/recaptcha/api.js' async defer></script>
    <?php } ?>

    <form action="<?php echo esc_url( $_SERVER['REQUEST_URI'] ) ?>" method="post" id="waitingListRegistrationForm" class="needs-validation" novalidate>
        <input type='hidden' class="form-control" name="join_date" id="join_date" />
        <input type='hidden' class="form-control" name="section-id" id="section-id" value="<?php echo $sectionID; ?>"/>

        <?php if (isset($errorMsg)) {
            if ($errorMsg > "") { ?>
                <p class="alert alert-danger"><?php echo $errorMsg; ?></p>
            <?php }
        } ?>

        <!--
            ###################################################################
            #########    Member Details Fields Start Here         #############
            ###################################################################
            -->
        <?php $CaptureGroup = get_field('capture_member', 'options');?>
        <?php $sectionHeading = get_field('capture_member_heading', 'options');?>

        <div class="well">
            <h3><?php echo $sectionHeading; ?></h3>

            <?php $FieldGroup = get_field('member_firstname_group', 'options');?>
            <?php $MaxFields = strval($FieldGroup['firstname_max']);?>
            <?php $MinFields = strval($FieldGroup['firstname_min']);?>
            <?php $field_label = strval($FieldGroup['firstname_label']);?>
            <?php $field_placeholder = strval($FieldGroup['firstname_placeholder']);?>
            <?php $error_msg = $FieldGroup['member_firstname_error_msg'];?>

            <div class="form-group">
                <label for="firstname"><?php echo $field_label; ?>: <span class="required" style="color: #ff0000;">*</span></label>
                <input type="text" class="form-control" name="firstname" id="firstname" placeholder="<?php echo $field_placeholder; ?>"
                       required="required" value="<?php echo (isset($member_FirstName) ? $member_FirstName : "" ); ?>"
                       maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>"/>
                <div class="invalid-feedback">
                    <?php echo $error_msg; ?>
                </div>
            </div>

            <?php $FieldGroup = get_field('member_lastname_group', 'options');?>
            <?php $MaxFields = strval($FieldGroup['lastname_max']);?>
            <?php $MinFields = strval($FieldGroup['lastname_min']);?>
            <?php $field_label = strval($FieldGroup['lastname_label']);?>
            <?php $field_placeholder = strval($FieldGroup['lastname_placeholder']);?>
            <?php $error_msg = $FieldGroup['member_lastname_error_msg'];?>

            <div class="form-group">
                <label for="lastname"><?php echo $field_label; ?>: <span class="required" style="color: #ff0000;">*</span> </label>
                <input type="text" class="form-control" name="lastname" id="lastname" placeholder="<?php echo $field_placeholder; ?>"
                       required="required" value="<?php echo (isset($member_LastName) ? $member_LastName : "" ); ?>"
                       maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>"/>
                <div class="invalid-feedback">
                    <?php echo $error_msg; ?>
                </div>
            </div>

            <?php $FieldGroup = get_field('member_dob_group', 'options');?>
            <?php $field_label = strval($FieldGroup['dob_label']);?>
            <?php $field_placeholder = strval($FieldGroup['dob_placeholder']);?>
            <?php $show_date_picker = strval($FieldGroup['show_date_picker']);?>
            <?php $error_msg = $FieldGroup['member_dob_error_msg'];?>

            <div class="form-group">
                <label for="dob"><?php echo $field_label; ?>: <span class="required" style="color: #ff0000;">*</span> </label>
                <?php if ($show_date_picker == 1) { ?>
                    <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker1"
                               readonly value="<?php echo (isset($member_DateofBirth) ? $member_DateofBirth : "" ); ?>"
                               name="member_DateofBirth" id="member_DateofBirth" required="required" placeholder="<?php echo $field_placeholder; ?>"/>
                        <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                            <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="input-group">
                        <input type="hidden" value="" name="member_DateofBirth" id="member_DateofBirth">
                        <input required autocomplete="off" placeholder="Day" type="number" aria-label="day" id="dob_day" min="1" max="31"  maxlength="2" minlength="1"
                               class="form-control" oninput="(validity.valid)||(value='');">
                        <input required autocomplete="off" placeholder="Month" type="number" aria-label="month" id="dob_month" min="1" max="12" maxlength="2" minlength="1"
                               class="form-control" oninput="(validity.valid)||(value='');">
                        <input required autocomplete="off" placeholder="Year" type="number" aria-label="year"  id="dob_year" min="" max="" maxlength="4" minlength="4"
                               class="form-control" oninput="(validity.valid)||(value='');">
                    </div>

                    <script>
                        <?php if (get_field("adult_register_" . $sectionID, 'options') == 1) { ?>
                        var $maxDate = moment().subtract(18, 'years').year();
                        var $minDate = moment().subtract(100, 'years').year();
                        <?php } else { ?>
                        var $maxDate = moment().year();
                        var $minDate = moment().subtract(18, 'years').year();
                        <?php } ?>

                        jQuery('#dob_year').attr({
                            'min': $minDate,
                            'max': $maxDate
                        });

                    </script>

                <?php } ?>
                <div class="invalid-feedback">
                    <?php echo $error_msg; ?>
                </div>
            </div>

            <?php if ($CaptureGroup == 1) { ?>

                <div class="form-group">

                    <?php $FieldGroup = get_field('member_Gender_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['member_Gender_required'];?>
                    <?php $field_label = strval($FieldGroup['member_Gender_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['member_Gender_placeholder']);?>
                    <?php $display = $FieldGroup['member_display_member_Gender'];?>
                    <?php $error_msg = $FieldGroup['member_Gender_error_msg'];?>
                    <?php if ($display == 1) { ?>

                        <label for="member_Gender"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?> </label>
                        <select class="form-control" name="member_Gender" id="member_Gender" <?php echo ($requiredFields == 1 ? 'required' : ''); ?>>
                            <option value=""><?php echo $field_placeholder; ?></option>
                            <option value="Female" <?php echo (isset($member_Gender) ? ($member_Gender=="Female" ? "selected" : "") : "selected" ); ?> >Female</option>
                            <option value="Male" <?php echo (isset($member_Gender) ? ($member_Gender=="Male" ? "selected" : "") : "" ); ?> >Male</option>
                            <option value="Other" <?php echo (isset($member_Gender) ? ($member_Gender=="Other" ? "selected" : "") : "" ); ?> >Other</option>
                            <option value="Unspecified" <?php echo (isset($member_Gender) ? ($member_Gender=="Unspecified" ? "selected" : "") : "" ); ?> >Unspecified</option>
                        </select>
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                Please enter the young person's gender
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('member_address1_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['address1_required'];?>
                    <?php $MaxFields = strval($FieldGroup['address1_max']);?>
                    <?php $MinFields = strval($FieldGroup['address1_min']);?>
                    <?php $field_label = strval($FieldGroup['address1_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['address1_placeholder']);?>
                    <?php $display = $FieldGroup['member_display_address1'];?>
                    <?php $error_msg = $FieldGroup['member_address1_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="member_Address1"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="member_Address1" id="member_Address1" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($member_AddressLine1) ? $member_AddressLine1 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('member_address2_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['address2_required'];?>
                    <?php $MaxFields = strval($FieldGroup['address2_max']);?>
                    <?php $MinFields = strval($FieldGroup['address2_min']);?>
                    <?php $field_label = strval($FieldGroup['address2_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['address2_placeholder']);?>
                    <?php $display = $FieldGroup['member_display_address2'];?>
                    <?php $error_msg = $FieldGroup['member_address2_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="member_Address2"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="member_Address2" id="member_Address2" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($member_AddressLine2) ? $member_AddressLine2 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('member_postcode_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['postcode_required'];?>
                    <?php $MaxFields = strval($FieldGroup['postcode_max']);?>
                    <?php $MinFields = strval($FieldGroup['postcode_min']);?>
                    <?php $field_label = strval($FieldGroup['postcode_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['postcode_placeholder']);?>
                    <?php $display = $FieldGroup['member_display_postcode'];?>
                    <?php $error_msg = $FieldGroup['member_postcode_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="member_PostCode"><?php echo $field_label; ?>:<?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="member_PostCode" id="member_PostCode" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($member_Postcode) ? $member_Postcode : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('member_mobile_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['mobile_required'];?>
                    <?php $MaxFields = strval($FieldGroup['mobile_max']);?>
                    <?php $MinFields = strval($FieldGroup['mobile_min']);?>
                    <?php $field_label = strval($FieldGroup['mobile_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['mobile_placeholder']);?>
                    <?php $display = $FieldGroup['member_display_mobile'];?>
                    <?php $error_msg = $FieldGroup['member_mobile_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="member_MobileNumber"><?php echo $field_label; ?>:<?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="tel" class="form-control" name="member_MobileNumber" id="member_MobileNumber" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($member_MobilePhoneNumber) ? $member_MobilePhoneNumber : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('member_home_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['home_required'];?>
                    <?php $MaxFields = strval($FieldGroup['home_max']);?>
                    <?php $MinFields = strval($FieldGroup['home_min']);?>
                    <?php $field_label = strval($FieldGroup['home_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['home_placeholder']);?>
                    <?php $display = $FieldGroup['member_display_home'];?>
                    <?php $error_msg = $FieldGroup['member_home_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="member_HomeNumber"><?php echo $field_label; ?>:<?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="tel" class="form-control" name="member_HomeNumber" id="member_HomeNumber" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($member_PhoneNumber) ? $member_PhoneNumber : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('member_email1_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['email1_required'];?>
                    <?php $MaxFields = strval($FieldGroup['email1_max']);?>
                    <?php $MinFields = strval($FieldGroup['email1_min']);?>
                    <?php $field_label = strval($FieldGroup['email1_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['email1_placeholder']);?>
                    <?php $display = $FieldGroup['member_display_email1'];?>
                    <?php $error_msg = $FieldGroup['member_email1_error_msg'];?>
                    <?php $error_msg2 = $FieldGroup['member_email1_error_msg2'];?>
                    <?php if ($display == 1) { ?>
                        <label for="member_EmailAddress"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="form-control"
                               name="member_EmailAddress" id="member_EmailAddress" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($member_EmailAddress1) ? $member_EmailAddress1 : "" ); ?>" maxlength="<?php echo $MaxFields;?>"
                               minlength="<?php echo $MinFields;?>">
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                        <div class="invalid-feedback invalidEmail">
                            <?php echo $error_msg2; ?>
                        </div>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('member_email2_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['email2_required'];?>
                    <?php $MaxFields = strval($FieldGroup['email2_max']);?>
                    <?php $MinFields = strval($FieldGroup['email2_min']);?>
                    <?php $field_label = strval($FieldGroup['email2_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['email2_placeholder']);?>
                    <?php $display = $FieldGroup['member_display_email2'];?>
                    <?php $error_msg = $FieldGroup['member_email2_error_msg'];?>
                    <?php $error_msg2 = $FieldGroup['member_email2_error_msg2'];?>
                    <?php if ($display == 1) { ?>
                        <label for="member_EmailAddress2"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="form-control"
                               name="member_EmailAddress2" id="member_EmailAddress2" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($member_EmailAddress2) ? $member_EmailAddress2 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                        <div class="invalid-feedback invalidEmail">
                            <?php echo $error_msg2; ?>
                        </div>
                    <?php } ?>

                </div>

            <?php } ?>

            <!--
            <button type="button" class="btn btn-primary syncAddress">Copy address to all other sections</button>
            <script>
                jQuery('.syncAddress').click(function (e) {
                    e.preventDefault();

                    //Primary
                    jQuery('#primary_AddressLine1').val(jQuery('#member_Address1').val());
                    jQuery('#primary_AddressLine2').val(jQuery('#member_Address2').val());
                    jQuery('#primary_Postcode').val(jQuery('#member_PostCode').val());

                    //Secondary
                    jQuery('#secondary_AddressLine1').val(jQuery('#member_Address1').val());
                    jQuery('#secondary_AddressLine2').val(jQuery('#member_Address2').val());
                    jQuery('#secondary_Postcode').val(jQuery('#member_PostCode').val());

                    //Emergency
                    jQuery('#emergency_AddressLine1').val(jQuery('#member_Address1').val());
                    jQuery('#emergency_AddressLine2').val(jQuery('#member_Address2').val());
                    jQuery('#emergency_Postcode').val(jQuery('#member_PostCode').val());

                });
            </script>
            -->
        </div>

        <!--
        ###################################################################
        #########    Primary Contact Fields Start Here      ###############
        ###################################################################
        -->
        <?php $CaptureGroup = get_field('capture_primary_contact1', 'options');?>
        <?php $sectionHeading = get_field('capture_primary_contact1_heading', 'options');?>
        <?php if ($CaptureGroup == 1) { ?>
            <div class="well">
                <h3><?php echo $sectionHeading; ?></h3>
                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_1_firstname_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary1_firstname_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary1_firstname_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary1_firstname_min']);?>
                    <?php $field_label = strval($FieldGroup['primary1_firstname_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary1_firstname_placeholder']);?>
                    <?php $display = $FieldGroup['primary1_display_firstname'];?>
                    <?php $error_msg = $FieldGroup['primary1_firstname_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="primary_FirstName"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?> </label>
                        <input type="text" class="form-control" name="primary_FirstName" id="primary_FirstName" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($primary_FirstName) ? $primary_FirstName : "" ); ?>" maxlength="<?php echo $MaxFields;?>"
                               minlength="<?php echo $MinFields;?>">
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_1_lastname_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary1_lastname_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary1_lastname_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary1_lastname_min']);?>
                    <?php $field_label = strval($FieldGroup['primary1_lastname_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary1_lastname_placeholder']);?>
                    <?php $display = $FieldGroup['primary1_display_lastname'];?>
                    <?php $error_msg = $FieldGroup['primary1_lastname_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="primary_LastName"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?> </label>
                        <input type="text" class="form-control" name="primary_LastName" id="primary_LastName" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($primary_LastName) ? $primary_LastName : "" ); ?>" maxlength="<?php echo $MaxFields;?>"
                               minlength="<?php echo $MinFields;?>">
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_1_address1_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary1_address1_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary1_address1_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary1_address1_min']);?>
                    <?php $field_label = strval($FieldGroup['primary1_address1_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary1_address1_placeholder']);?>
                    <?php $display = $FieldGroup['primary1_display_address1'];?>
                    <?php $error_msg = $FieldGroup['primary1_address1_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="primary_AddressLine1"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="primary_AddressLine1" id="primary_AddressLine1" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($primary_AddressLine1) ? $primary_AddressLine1 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_1_address2_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary1_address2_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary1_address2_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary1_address2_min']);?>
                    <?php $field_label = strval($FieldGroup['primary1_address2_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary1_address2_placeholder']);?>
                    <?php $display = $FieldGroup['primary1_display_address2'];?>
                    <?php $error_msg = $FieldGroup['primary1_address2_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="primary_AddressLine2"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="primary_AddressLine2" id="primary_AddressLine2" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($primary_AddressLine2) ? $primary_AddressLine2 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_1_postcode_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary1_postcode_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary1_postcode_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary1_postcode_min']);?>
                    <?php $field_label = strval($FieldGroup['primary1_postcode_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary1_postcode_placeholder']);?>
                    <?php $display = $FieldGroup['primary1_display_postcode'];?>
                    <?php $error_msg = $FieldGroup['primary1_postcode_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="primary_Postcode"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="primary_Postcode" id="primary_Postcode" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($primary_Postcode) ? $primary_Postcode : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_1_mobile_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary1_mobile_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary1_mobile_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary1_mobile_min']);?>
                    <?php $field_label = strval($FieldGroup['primary1_mobile_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary1_mobile_placeholder']);?>
                    <?php $display = $FieldGroup['primary1_display_mobile'];?>
                    <?php $error_msg = $FieldGroup['primary1_mobile_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="primary_MobilePhoneNumber"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="tel" class="form-control" name="primary_MobilePhoneNumber" id="primary_MobilePhoneNumber" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($primary_MobilePhoneNumber) ? $primary_MobilePhoneNumber : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_1_home_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary1_home_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary1_home_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary1_home_min']);?>
                    <?php $field_label = strval($FieldGroup['primary1_home_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary1_home_placeholder']);?>
                    <?php $display = $FieldGroup['primary1_display_home'];?>
                    <?php $error_msg = $FieldGroup['primary1_home_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="primary_PhoneNumber"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="tel" class="form-control" name="primary_PhoneNumber" id="primary_PhoneNumber" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($primary_PhoneNumber) ? $primary_PhoneNumber : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_1_email_group1', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary1_email1_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary1_email1_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary1_email1_min']);?>
                    <?php $field_label = strval($FieldGroup['primary1_email1_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary1_email1_placeholder']);?>
                    <?php $display = $FieldGroup['primary1_display_email1'];?>
                    <?php $error_msg = $FieldGroup['primary1_email1_error_msg'];?>
                    <?php $error_msg2 = $FieldGroup['primary1_email1_error_msg2'];?>
                    <?php if ($display == 1) { ?>
                        <label for="primary_EmailAddress1"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="form-control"
                               name="primary_EmailAddress1" id="primary_EmailAddress1" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($primary_EmailAddress1) ? $primary_EmailAddress1 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                        <div class="invalid-feedback invalidEmail">
                            <?php echo $error_msg2; ?>
                        </div>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_1_email_group2', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary1_email2_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary1_email2_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary1_email2_min']);?>
                    <?php $field_label = strval($FieldGroup['primary1_email2_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary1_email2_placeholder']);?>
                    <?php $display = $FieldGroup['primary1_display_email2'];?>
                    <?php $error_msg = $FieldGroup['primary1_email2_error_msg'];?>
                    <?php $error_msg2 = $FieldGroup['primary1_email2_error_msg2'];?>
                    <?php if ($display == 1) { ?>
                        <label for="primary_EmailAddress2"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="form-control"
                               name="primary_EmailAddress2" id="primary_EmailAddress2" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($primary_EmailAddress2) ? $primary_EmailAddress2 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                        <div class="invalid-feedback invalidEmail">
                            <?php echo $error_msg2; ?>
                        </div>
                    <?php } ?>

                </div>
            </div>
        <?php } ?>

        <!--
        ###################################################################
        #########    Secondary Contact Fields Start Here      #############
        ###################################################################
        -->
        <?php $CaptureGroup = get_field('capture_primary_contact2', 'options');?>
        <?php $sectionHeading = get_field('capture_primary_contact2_heading', 'options');?>
        <?php if ($CaptureGroup == 1) { ?>
            <div class="well">
                <h3><?php echo $sectionHeading; ?></h3>
                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_2_firstname_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary2_firstname_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary2_firstname_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary2_firstname_min']);?>
                    <?php $field_label = strval($FieldGroup['primary2_firstname_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary2_firstname_placeholder']);?>
                    <?php $display = $FieldGroup['primary2_display_firstname'];?>
                    <?php $error_msg = $FieldGroup['primary2_firstname_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="secondary_FirstName"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?> </label>
                        <input type="text" class="form-control" name="secondary_FirstName" id="secondary_FirstName" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($secondary_FirstName) ? $secondary_FirstName : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_2_lastname_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary2_lastname_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary2_lastname_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary2_lastname_min']);?>
                    <?php $field_label = strval($FieldGroup['primary2_lastname_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary2_lastname_placeholder']);?>
                    <?php $display = $FieldGroup['primary2_display_lastname'];?>
                    <?php $error_msg = $FieldGroup['primary2_lastname_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="secondary_LastName"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?> </label>
                        <input type="text" class="form-control" name="secondary_LastName" id="secondary_LastName" placeholder="<?php echo $field_placeholder; ?>" <?php echo ($requiredFields == 1 ? 'required' : ''); ?>
                               value="<?php echo (isset($secondary_LastName) ? $secondary_LastName : "" ); ?>" maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_2_address1_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary2_address1_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary2_address1_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary2_address1_min']);?>
                    <?php $field_label = strval($FieldGroup['primary2_address1_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary2_address1_placeholder']);?>
                    <?php $display = $FieldGroup['primary2_display_address1'];?>
                    <?php $error_msg = $FieldGroup['primary2_address1_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="secondary_AddressLine1"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="secondary_AddressLine1" id="secondary_AddressLine1" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($secondary_AddressLine1) ? $secondary_AddressLine1 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_2_address2_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary2_address2_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary2_address2_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary2_address2_min']);?>
                    <?php $field_label = strval($FieldGroup['primary2_address2_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary2_address2_placeholder']);?>
                    <?php $display = $FieldGroup['primary2_display_address2'];?>
                    <?php $error_msg = $FieldGroup['primary2_address2_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="secondary_AddressLine2"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="secondary_AddressLine2" id="secondary_AddressLine2" placeholder="<?php echo $field_placeholder; ?>" <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($secondary_AddressLine2) ? $secondary_AddressLine2 : "" ); ?>" maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_2_postcode_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary2_postcode_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary2_postcode_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary2_postcode_min']);?>
                    <?php $field_label = strval($FieldGroup['primary2_postcode_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary2_postcode_placeholder']);?>
                    <?php $display = $FieldGroup['primary2_display_postcode'];?>
                    <?php $error_msg = $FieldGroup['primary2_postcode_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="secondary_Postcode"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="secondary_Postcode" id="secondary_Postcode" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($secondary_Postcode) ? $secondary_Postcode : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_2_mobile_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary2_mobile_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary2_mobile_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary2_mobile_min']);?>
                    <?php $field_label = strval($FieldGroup['primary2_mobile_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary2_mobile_placeholder']);?>
                    <?php $display = $FieldGroup['primary2_display_mobile'];?>
                    <?php $error_msg = $FieldGroup['primary2_mobile_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="secondary_MobilePhoneNumber"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="tel" class="form-control" name="secondary_MobilePhoneNumber" id="secondary_MobilePhoneNumber" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($secondary_MobilePhoneNumber) ? $secondary_MobilePhoneNumber : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_2_home_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary2_home_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary2_home_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary2_home_min']);?>
                    <?php $field_label = strval($FieldGroup['primary2_home_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary2_home_placeholder']);?>
                    <?php $display = $FieldGroup['primary2_display_home'];?>
                    <?php $error_msg = $FieldGroup['primary2_home_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="secondary_PhoneNumber"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="tel" class="form-control" name="secondary_PhoneNumber" id="secondary_PhoneNumber" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($secondary_PhoneNumber) ? $secondary_PhoneNumber : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_2_email1_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary2_email1_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary2_email1_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary2_email1_min']);?>
                    <?php $field_label = strval($FieldGroup['primary2_email1_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary2_email1_placeholder']);?>
                    <?php $display = $FieldGroup['primary2_display_email1'];?>
                    <?php $error_msg = $FieldGroup['primary2_email1_error_msg'];?>
                    <?php $error_msg2 = $FieldGroup['primary2_email1_error_msg2'];?>
                    <?php if ($display == 1) { ?>
                        <label for="secondary_EmailAddress1"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="form-control" name="secondary_EmailAddress1"
                               id="secondary_EmailAddress1" placeholder="<?php echo $field_placeholder; ?>" <?php echo ($requiredFields == 1 ? 'required' : ''); ?>
                               value="<?php echo (isset($secondary_EmailAddress1) ? $secondary_EmailAddress1 : "" ); ?>" maxlength="<?php echo $MaxFields;?>"
                               minlength="<?php echo $MinFields;?>">
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                        <div class="invalid-feedback invalidEmail">
                            <?php echo $error_msg2; ?>
                        </div>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('primary_contact_2_email2_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['primary2_email2_required'];?>
                    <?php $MaxFields = strval($FieldGroup['primary2_email2_max']);?>
                    <?php $MinFields = strval($FieldGroup['primary2_email2_min']);?>
                    <?php $field_label = strval($FieldGroup['primary2_email2_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['primary2_email2_placeholder']);?>
                    <?php $display = $FieldGroup['primary2_display_email2'];?>
                    <?php $error_msg = $FieldGroup['primary2_email2_error_msg'];?>
                    <?php $error_msg2 = $FieldGroup['primary2_email2_error_msg2'];?>
                    <?php if ($display == 1) { ?>
                        <label for="secondary_EmailAddress2"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="form-control" name="secondary_EmailAddress2"
                               id="secondary_EmailAddress2" placeholder="<?php echo $field_placeholder; ?>" <?php echo ($requiredFields == 1 ? 'required' : ''); ?>
                               value="<?php echo (isset($secondary_EmailAddress2) ? $secondary_EmailAddress2 : "" ); ?>" maxlength="<?php echo $MaxFields;?>"
                               minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                        <div class="invalid-feedback invalidEmail">
                            <?php echo $error_msg2; ?>
                        </div>
                    <?php } ?>

                </div>
            </div>
        <?php } ?>

        <!--
        ###################################################################
        #########    Emergency Contact Fields Start Here      #############
        ###################################################################
        -->
        <?php $CaptureGroup = get_field('capture_emergency', 'options');?>
        <?php $sectionHeading = get_field('capture_emergency_heading', 'options');?>
        <?php if ($CaptureGroup == 1) { ?>
            <div class="well">
                <h3><?php echo $sectionHeading; ?></h3>
                <div class="form-group">

                    <?php $FieldGroup = get_field('emergency_firstname_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['emergency_firstname_required'];?>
                    <?php $MaxFields = strval($FieldGroup['emergency_firstname_max']);?>
                    <?php $MinFields = strval($FieldGroup['emergency_firstname_min']);?>
                    <?php $field_label = strval($FieldGroup['emergency_firstname_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['emergency_firstname_placeholder']);?>
                    <?php $display = $FieldGroup['emergency_display_firstname'];?>
                    <?php $error_msg = $FieldGroup['emergency_firstname_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="emergency_FirstName"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?> </label>
                        <input type="text" class="form-control" name="emergency_FirstName" id="emergency_FirstName" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($emergency_FirstName) ? $emergency_FirstName : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>" />
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('emergency_lastname_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['emergency_lastname_required'];?>
                    <?php $MaxFields = strval($FieldGroup['emergency_lastname_max']);?>
                    <?php $MinFields = strval($FieldGroup['emergency_lastname_min']);?>
                    <?php $field_label = strval($FieldGroup['emergency_lastname_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['emergency_lastname_placeholder']);?>
                    <?php $display = $FieldGroup['emergency_display_lastname'];?>
                    <?php $error_msg = $FieldGroup['emergency_lastname_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="emergency_LastName"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?> </label>
                        <input type="text" class="form-control" name="emergency_LastName" id="emergency_LastName" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($emergency_LastName) ? $emergency_LastName : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>" />
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('emergency_address1_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['emergency_address1_required'];?>
                    <?php $MaxFields = strval($FieldGroup['emergency_address1_max']);?>
                    <?php $MinFields = strval($FieldGroup['emergency_address1_min']);?>
                    <?php $field_label = strval($FieldGroup['emergency_address1_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['emergency_address1_placeholder']);?>
                    <?php $display = $FieldGroup['emergency_display_address1'];?>
                    <?php $error_msg = $FieldGroup['emergency_address1_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="emergency_AddressLine1"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="emergency_AddressLine1" id="emergency_AddressLine1" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($emergency_AddressLine1) ? $emergency_AddressLine1 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>" />
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('emergency_address2_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['emergency_address2_required'];?>
                    <?php $MaxFields = strval($FieldGroup['emergency_address2_max']);?>
                    <?php $MinFields = strval($FieldGroup['emergency_address2_min']);?>
                    <?php $field_label = strval($FieldGroup['emergency_address2_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['emergency_address2_placeholder']);?>
                    <?php $display = $FieldGroup['emergency_display_address2'];?>
                    <?php $error_msg = $FieldGroup['emergency_address2_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="emergency_AddressLine2"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="emergency_AddressLine2" id="emergency_AddressLine2" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($emergency_AddressLine2) ? $emergency_AddressLine2 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>"/>
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('emergency_postcode_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['emergency_postcode_required'];?>
                    <?php $MaxFields = strval($FieldGroup['emergency_postcode_max']);?>
                    <?php $MinFields = strval($FieldGroup['emergency_postcode_min']);?>
                    <?php $field_label = strval($FieldGroup['emergency_postcode_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['emergency_postcode_placeholder']);?>
                    <?php $display = $FieldGroup['emergency_display_postcode'];?>
                    <?php $error_msg = $FieldGroup['emergency_postcode_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="emergency_Postcode"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="emergency_Postcode" id="emergency_Postcode" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($emergency_Postcode) ? $emergency_Postcode : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>"/>
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('emergency_mobile_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['emergency_mobile_required'];?>
                    <?php $MaxFields = strval($FieldGroup['emergency_mobile_max']);?>
                    <?php $MinFields = strval($FieldGroup['emergency_mobile_min']);?>
                    <?php $field_label = strval($FieldGroup['emergency_mobile_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['emergency_mobile_placeholder']);?>
                    <?php $display = $FieldGroup['emergency_display_mobile'];?>
                    <?php $error_msg = $FieldGroup['emergency_mobile_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="emergency_MobilePhoneNumber"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="tel" class="form-control" name="emergency_MobilePhoneNumber" id="emergency_MobilePhoneNumber" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($emergency_MobilePhoneNumber) ? $emergency_MobilePhoneNumber : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('emergency_home_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['emergency_home_required'];?>
                    <?php $MaxFields = strval($FieldGroup['emergency_home_max']);?>
                    <?php $MinFields = strval($FieldGroup['emergency_home_min']);?>
                    <?php $field_label = strval($FieldGroup['emergency_home_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['emergency_home_placeholder']);?>
                    <?php $display = $FieldGroup['emergency_display_home'];?>
                    <?php $error_msg = $FieldGroup['emergency_home_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="emergency_PhoneNumber"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="tel" class="form-control" name="emergency_PhoneNumber" id="emergency_PhoneNumber" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($emergency_PhoneNumber) ? $emergency_PhoneNumber : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('emergency_email1_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['emergency_email1_required'];?>
                    <?php $MaxFields = strval($FieldGroup['emergency_email1_max']);?>
                    <?php $MinFields = strval($FieldGroup['emergency_email1_min']);?>
                    <?php $field_label = strval($FieldGroup['emergency_email1_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['emergency_email1_placeholder']);?>
                    <?php $display = $FieldGroup['emergency_display_email1'];?>
                    <?php $error_msg = $FieldGroup['emergency_email1_error_msg'];?>
                    <?php $error_msg2 = $FieldGroup['emergency_email1_error_msg2'];?>
                    <?php if ($display == 1) { ?>
                        <label for="emergency_EmailAddress1"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="form-control"
                               name="emergency_EmailAddress1" id="emergency_EmailAddress1" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($emergency_EmailAddress1) ? $emergency_EmailAddress1 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                        <div class="invalid-feedback invalidEmail">
                            <?php echo $error_msg2; ?>
                        </div>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('emergency_email2_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['emergency_email2_required'];?>
                    <?php $MaxFields = strval($FieldGroup['emergency_email2_max']);?>
                    <?php $MinFields = strval($FieldGroup['emergency_email2_min']);?>
                    <?php $field_label = strval($FieldGroup['emergency_email2_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['emergency_email2_placeholder']);?>
                    <?php $display = $FieldGroup['emergency_display_email2'];?>
                    <?php $error_msg = $FieldGroup['emergency_email2_error_msg'];?>
                    <?php $error_msg2 = $FieldGroup['emergency_email2_error_msg2'];?>
                    <?php if ($display == 1) { ?>
                        <label for="emergency_EmailAddress2"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="email" pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/" class="form-control"
                               name="emergency_EmailAddress2" id="emergency_EmailAddress2" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($emergency_EmailAddress2) ? $emergency_EmailAddress2 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                        <div class="invalid-feedback invalidEmail">
                            <?php echo $error_msg2; ?>
                        </div>
                    <?php } ?>

                </div>
            </div>
        <?php } ?>

        <!--
        ###################################################################
        #########    Doctors Contact Fields Start Here      ##############
        ###################################################################
        -->
        <?php $CaptureGroup = get_field('capture_doctor', 'options');?>
        <?php $sectionHeading = get_field('capture_doctor_heading', 'options');?>
        <?php if ($CaptureGroup == 1) { ?>
            <div class="well">
                <h3><?php echo $sectionHeading; ?></h3>
                <div class="form-group">

                    <?php $FieldGroup = get_field('doctor_firstname_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['doctor_firstname_required'];?>
                    <?php $MaxFields = strval($FieldGroup['doctor_firstname_max']);?>
                    <?php $MinFields = strval($FieldGroup['doctor_firstname_min']);?>
                    <?php $field_label = strval($FieldGroup['doctor_firstname_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['doctor_firstname_placeholder']);?>
                    <?php $display = $FieldGroup['doctor_display_firstname'];?>
                    <?php $error_msg = $FieldGroup['doctor_firstname_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="doctor_FirstName"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?> </label>
                        <input type="text" class="form-control" name="doctor_FirstName" id="doctor_FirstName" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($doctor_FirstName) ? $doctor_FirstName : "" ); ?>" maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('doctor_lastname_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['doctor_lastname_required'];?>
                    <?php $MaxFields = strval($FieldGroup['doctor_lastname_max']);?>
                    <?php $MinFields = strval($FieldGroup['doctor_lastname_min']);?>
                    <?php $field_label = strval($FieldGroup['doctor_lastname_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['doctor_lastname_placeholder']);?>
                    <?php $display = $FieldGroup['doctor_display_lastname'];?>
                    <?php $error_msg = $FieldGroup['doctor_lastname_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="doctor_LastName"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?> </label>
                        <input type="text" class="form-control" name="doctor_LastName" id="doctor_LastName" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($doctor_LastName) ? $doctor_LastName : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('doctor_surgery_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['doctor_surgery_required'];?>
                    <?php $MaxFields = strval($FieldGroup['doctor_surgery_max']);?>
                    <?php $MinFields = strval($FieldGroup['doctor_surgery_min']);?>
                    <?php $field_label = strval($FieldGroup['doctor_surgery_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['doctor_surgery_placeholder']);?>
                    <?php $display = $FieldGroup['doctor_display_surgery'];?>
                    <?php $error_msg = $FieldGroup['doctor_surgery_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="doctor_Surgery"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?> </label>
                        <input type="text" class="form-control" name="doctor_Surgery" id="doctor_Surgery" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($doctor_Surgery) ? $doctor_Surgery : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <div class="invalid-feedback">
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('doctor_address1_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['doctor_address1_required'];?>
                    <?php $MaxFields = strval($FieldGroup['doctor_address1_max']);?>
                    <?php $MinFields = strval($FieldGroup['doctor_address1_min']);?>
                    <?php $field_label = strval($FieldGroup['doctor_address1_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['doctor_address1_placeholder']);?>
                    <?php $display = $FieldGroup['doctor_display_address1'];?>
                    <?php $error_msg = $FieldGroup['doctor_address1_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="doctor_AddressLine1"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="doctor_AddressLine1" id="doctor_AddressLine1" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($doctor_AddressLine1) ? $doctor_AddressLine1 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>

                <div class="form-group">

                    <?php $FieldGroup = get_field('doctor_address2_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['doctor_address2_required'];?>
                    <?php $MaxFields = strval($FieldGroup['doctor_address2_max']);?>
                    <?php $MinFields = strval($FieldGroup['doctor_address2_min']);?>
                    <?php $field_label = strval($FieldGroup['doctor_address2_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['doctor_address2_placeholder']);?>
                    <?php $display = $FieldGroup['doctor_display_address2'];?>
                    <?php $error_msg = $FieldGroup['doctor_address2_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="doctor_AddressLine2"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="doctor_AddressLine2" id="doctor_AddressLine2" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($doctor_AddressLine2) ? $doctor_AddressLine2 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('doctor_postcode_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['doctor_postcode_required'];?>
                    <?php $MaxFields = strval($FieldGroup['doctor_postcode_max']);?>
                    <?php $MinFields = strval($FieldGroup['doctor_postcode_min']);?>
                    <?php $field_label = strval($FieldGroup['doctor_postcode_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['doctor_postcode_placeholder']);?>
                    <?php $display = $FieldGroup['doctor_display_postcode'];?>
                    <?php $error_msg = $FieldGroup['doctor_postcode_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="doctor_Postcode"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="text" class="form-control" name="doctor_Postcode" id="doctor_Postcode" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?>  value="<?php echo (isset($doctor_Postcode) ? $doctor_Postcode : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('doctor_mobile_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['doctor_mobile_required'];?>
                    <?php $MaxFields = strval($FieldGroup['doctor_mobile_max']);?>
                    <?php $MinFields = strval($FieldGroup['doctor_mobile_min']);?>
                    <?php $field_label = strval($FieldGroup['doctor_mobile_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['doctor_mobile_placeholder']);?>
                    <?php $display = $FieldGroup['doctor_display_mobile'];?>
                    <?php $error_msg = $FieldGroup['doctor_mobile_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="doctor_PhoneNumber1"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="tel" class="form-control" name="doctor_PhoneNumber1" id="doctor_PhoneNumber1" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($doctor_PhoneNumber1) ? $doctor_PhoneNumber1 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
                <div class="form-group">

                    <?php $FieldGroup = get_field('doctor_home_group', 'options');?>
                    <?php $requiredFields = $FieldGroup['doctor_home_required'];?>
                    <?php $MaxFields = strval($FieldGroup['doctor_home_max']);?>
                    <?php $MinFields = strval($FieldGroup['doctor_home_min']);?>
                    <?php $field_label = strval($FieldGroup['doctor_home_label']);?>
                    <?php $field_placeholder = strval($FieldGroup['doctor_home_placeholder']);?>
                    <?php $display = $FieldGroup['doctor_display_home'];?>
                    <?php $error_msg = $FieldGroup['doctor_home_error_msg'];?>
                    <?php if ($display == 1) { ?>
                        <label for="doctor_PhoneNumber2"><?php echo $field_label; ?>: <?php echo ($requiredFields == 1 ? '<span class="required" style="color: #ff0000;">*</span>' : ''); ?></label>
                        <input type="tel" class="form-control" name="doctor_PhoneNumber2" id="doctor_PhoneNumber2" placeholder="<?php echo $field_placeholder; ?>"
                            <?php echo ($requiredFields == 1 ? 'required' : ''); ?> value="<?php echo (isset($doctor_PhoneNumber2) ? $doctor_PhoneNumber2 : "" ); ?>"
                               maxlength="<?php echo $MaxFields;?>" minlength="<?php echo $MinFields;?>">
                        <?php if ($requiredFields == 1) { ?>
                            <div class="invalid-feedback">
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>

                </div>
            </div>
        <?php } ?>

        <!--
        ###################################################################
        #########           Custom Fields Start Here      #################
        ###################################################################
        -->

        <?php
        $customFields = get_field('custom_fields', 'options');
        if (isset($customFields)) {
            if (is_array($customFields) || is_object($customFields)) { ?>
                <div class="well">
                    <h3>Additional Information</h3>
                    <?php
                    $fieldCounter = 1;
                    foreach ($customFields as $field) {
                        if (isset($customFieldsValuesNice)) {
                            echo fetchCustomFields ($field, $fieldCounter, $sectionID, $customFieldsValuesNice);
                        } else {
                            echo fetchCustomFields ($field, $fieldCounter, $sectionID, "");
                        }
                        $fieldCounter++;
                    }
                    ?>
                </div>
            <?php } ?>
        <?php } ?>


        <div class="well">
            <?php
            $TandCFields = get_field('terms_conditions', 'options');
            if ( $TandCFields['show_tandc_field'] == 1 && !empty($TandCFields['tc_text'])) {
                ?>

                <h3>Terms &amp; Conditions</h3>
                <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="tandc" id="tandc" required>
                            <?php echo $TandCFields['tc_text']; ?>
                            <?php if ($TandCFields['tc_link']) { ?>
                                <a class="tandcLink" href="<?php echo $TandCFields['tc_link']['url']; ?>" target="<?php echo $TandCFields['tc_link']['target']; ?>"><?php echo $TandCFields['tc_link']['title']; ?></a>
                            <?php } ?>
                        </label>
                        <div class="invalid-feedback">
                            Please agree to these terms and conditions
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if ($requireOldCaptcha == 0) { ?>
                <script>
                    grecaptcha.ready(function() {
                        grecaptcha.execute('<?php echo $googleSiteKey; ?>', {action: 'waitinglistregister'}).then(function(token) {
                            jQuery('#waitingListRegistrationForm').prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');
                        });
                    });
                </script>
            <?php } else if ($requireOldCaptcha == 1) { ?>
                <div class="g-recaptcha" data-sitekey="<?php echo $googleSiteKey; ?>"></div>
            <?php } ?>

            <div class="text-right">
                <button id="submitWaitingList" name="submitWaitingList" type="submit" class="btn btn-primary">Submit</button>
                <input type="hidden" name="submitWaitingList" value="Save" />
            </div>

        </div>

        <p style="font-size: 12px; margin-top: 10px;" class="text-right"><small >Powered by: <a href="http://www.scoutsuk.org" target="_blank">NeoWeb OSM Authenticator</a></small></p>

    </form>

    <script type="text/javascript">

        function validateEmail(email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if( !emailReg.test( email ) ) {
                return false;
            } else {
                return true;
            }
        }

        jQuery(document).ready(function() {

            jQuery('.submitAlert').each(function() {
                jQuery(this).insertBefore('#waitingListRegistrationForm').fadeIn();
            });

            var d = new Date();

            var month = d.getMonth()+1;
            var day = d.getDate();

            var output = (day<10 ? '0' : '') + day + "/"
                + (month<10 ? '0' : '') + month + '/'
                + d.getFullYear();

            jQuery('#join_date').val( output );

            <?php if ($show_date_picker == 1) { ?>

            <?php if (get_field("adult_register_" . $sectionID, 'options') == 1) { ?>
            var $maxDate = moment().subtract(18, 'years').toDate();
            var $minDate = moment().subtract(100, 'years').toDate();
            <?php } else { ?>
            var $maxDate = moment().toDate();
            var $minDate = moment().subtract(18, 'years').toDate();
            <?php } ?>

            jQuery('#datetimepicker1').datetimepicker({
                icons: {
                    time: 'fa fa-clock',
                    date: 'fa fa-calendar-alt',
                    up: 'fa fa-arrow-up',
                    down: 'fa fa-arrow-down',
                    previous: 'fa fa-arrow-left',
                    next: 'fa fa-arrow-right',
                    today: 'glyphicon glyphicon-screenshot',
                    clear: 'glyphicon glyphicon-trash',
                    close: 'fa fa-times'
                },
                format: 'DD-MM-YYYY',
                useCurrent: false,
                maxDate: $maxDate,
                minDate: $minDate,
                ignoreReadonly: true,
                viewMode: 'years',
                <?php if (get_field('datepicker_debug', 'options') == 1) { echo 'debug:true'; } ?>
            });
            <?php } ?>

        });

        jQuery('#waitingListRegistrationForm').on('submit', function (e) {

            var $valid = true;

            jQuery(".invalid-feedback").hide();

            jQuery('#waitingListRegistrationForm input, #waitingListRegistrationForm select').each(function() {

                if (jQuery(this).is('[type=email]')) {
                    if (validateEmail(jQuery(this).val())) {
                        jQuery(this).removeClass('is-invalid');
                        if ( jQuery(this).attr('required') && jQuery(this).val() === "") {
                            jQuery(this).addClass('is-invalid');
                            jQuery(this).next(".invalid-feedback").show();
                            jQuery(this).parent().find(".invalid-feedback").show();
                            $valid = false;
                        } else {
                            jQuery(this).addClass('is-valid');
                        }
                    } else {
                        if (jQuery(this).val() != "") {
                            jQuery(this).addClass('is-invalid');
                            jQuery(this).find(".invalidEmail").show();
                            jQuery(this).parent().next(".invalidEmail").show();
                            $valid = false;
                        } else {
                            jQuery(this).addClass('is-valid');
                        }
                    }
                } else {
                    jQuery(this).removeClass('is-invalid');
                    if ( jQuery(this).attr('required') && jQuery(this).val() === "") {
                        jQuery(this).addClass('is-invalid');
                        jQuery(this).next(".invalid-feedback").show();
                        jQuery(this).parent().next(".invalid-feedback").show();
                        $valid = false;
                    } else {
                        jQuery(this).addClass('is-valid');
                    }
                }
            });

            <?php if ($show_date_picker == 0) { ?>
            if (jQuery('#dob_day').val() ==  "" || jQuery('#dob_month').val() == "" || jQuery('#dob_year').val() == "") {
                $valid = false;
                jQuery('#dob_day').parent().parent().find(".invalid-feedback").show();
            } else {
                $dob = jQuery('#dob_day').val() + "-" + jQuery('#dob_month').val() + "-" + jQuery('#dob_year').val();
                jQuery('#member_DateofBirth').val($dob);
            }
            <?php } ?>

            <?php
            $TandCFields = get_field('terms_conditions', 'options');
            if ( $TandCFields['show_tandc_field'] == 1 ) {
            ?>

            var isChecked = jQuery('#tandc').is(":checked");
            if ( !isChecked ) {
                jQuery('#tandc').addClass('is-invalid');
                jQuery('#tandc').next(".invalid-feedback").show();
                jQuery('#tandc').parent().next(".invalid-feedback").show();
                $valid = false;
            }

            <?php } ?>

            if (!$valid) {
                return false;
            }

            <?php if ($requireOldCaptcha == 0) { ?>
            var frm = this;
            e.preventDefault();
            grecaptcha.execute('<?php echo $googleSiteKey; ?>', {action: 'contact'}).then(function(token) {
                jQuery('#response').val(token);
                frm.submit();
            });
            <?php } ?>

        });

    </script>
<?php

} else {
    echo '<p class="alert alert-danger">This form has encountered an error. Please contact the site owner to investigate.</p>';
}

?>