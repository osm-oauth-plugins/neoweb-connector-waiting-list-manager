<?php


class NeoWeb_Connector_Waiting_List_Manager_Admin_Page_Loader {

	private NeoWeb_Connector_Loggers $logger;
	private NeoWeb_Connector_Licence_Manager $licenceManager;
	private NeoWeb_Connector_Waiting_List_Manager_Auth_Caller $oAuthCaller;
	/**
	 * @param $key
	 *
	 * @return string
	 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * __constructor.
	 *
	 */
	public function __construct() {


		$this->plugin_data = get_option('neoweb-connector-waiting-list-manager');

		$this->logger = new NeoWeb_Connector_Loggers(
			plugin_dir_path( dirname( __FILE__ ) )
		);
		$this->licenceManager = new NeoWeb_Connector_Licence_Manager();
		$this->oAuthCaller = new NeoWeb_Connector_Waiting_List_Manager_Auth_Caller();

	}

	public function loadAdminPages () {

		$productPageLoader = new NeoWeb_Connector_Register_Product_Pages();
		$productPageLoader->registerProductPages();
		$productPageLoader->registerProductPageLogos();


		//Add application licence option page and fields
		$applicationLicencePage = new NeoWeb_Connector_Register_Licence_Page();
		$applicationLicencePage->registerLicencePersonalDetailsFields();
		$applicationLicencePage->registerLicenceProductDetailsFields();

		$licenseCheck = $this->licenceManager->checkLicenceKey();
		if ($licenseCheck) {

			//Register Protected Product Pages & Logos(Needs a licence)
			$productPageLoader->registerProtectedProductPages();
			$productPageLoader->registerProtectedProductPageLogos();

			//Add OSM authentication settings page and fields
			$applicationSettingsPage = new NeoWeb_Connector_Register_OSM_App_Settings_Page();
			$applicationSettingsPage->registerGenericInstructions();
			$applicationSettingsPage->registerApplicationFields();

			/**
			 * Check we have app details saved and that we have an access token from a previous authentication call,
			 * then try to renew the accessToken if needed.
			 **/
			if (get_field($this->get_plugin_data('pluginSlug') . "_osm_oauth_client_id", "option") &&
			    get_field($this->get_plugin_data('pluginSlug') . "_osm_oauth_secret", "option")) {

				$accessToken = $this->oAuthCaller->get_access_token();
				if (!empty($accessToken)) {
					//$osm_oauth_accessToken = $accessToken;
                    $groups = $this->oAuthCaller->getAvailableWaitingLists();
                    $sectionsMetaData = $this->oAuthCaller->getTransientMetaData('resourceData');

					//Register Plugin Resources Option Page
					$resourcesPage = new NeoWeb_Connector_Register_Resources_Page();
					$resourcesPage->prepareResourcesFields($groups, $sectionsMetaData);

					//Register Waiting List search page
                    $applicationSearchPage = new NeoWeb_Connector_WaitingList_Search();
                    $applicationSearchPage->registerSettingsPage();
                    $applicationSearchPage->registerPluginLogo();
                    $applicationSearchPage->registerSettingsPageDefaultFields();

                    //Register Waiting List register page
					$applicationSubmitPage = new NeoWeb_Connector_WaitingList_Submit();
					$applicationSubmitPage->registerSettingsPages();
					$applicationSubmitPage->registerPluginLogo();
					$applicationSubmitPage->registerSettingsPageDefaultFields();

					//Register Waiting List register page
					$applicationSubmitPage = new NeoWeb_Connector_WaitingList_Submit();

					$syncRequired = false;
					foreach ( $groups as $group ) {
						foreach ( $group as $section ) {
							$customData = $this->oAuthCaller->getCustomData($section['section_id']);
							$applicationSearchPage->registerCustomFields($customData, $section['section_name'], $section['section_id']);
                            $applicationSearchPage->registerShortCodes($section['section_name'], $section['section_id']);

                            //Register Waiting List register page settings
							$applicationSubmitPageSections = new NeoWeb_Connector_WaitingList_Submit_Sections(
								$section['section_name'], $section['section_id'], $customData
							);
							$applicationSubmitPageSections->registerCustomFields();

							$sectionOverride = get_field($section['section_id'] . '_override', 'option');
							if ($sectionOverride == 1) {

								//We need to add a new menu to get override values for this section
								$syncRequired = $applicationSubmitPageSections->registerOverrideFields($syncRequired);
								$syncRequired = $applicationSubmitPageSections->registerCustomOverrideFields($syncRequired);
								$applicationSubmitPageSections->registerCustomFields();
							}

							$applicationSubmitPage->registerShortCodes($section['section_name'], $section['section_id']);

						}
					}

					if ($syncRequired) {
						//$applicationSubmitPage->runAutoSync();
					}

				}
			}

			$applicationDebugPage = new NeoWeb_Connector_Register_Debug_Cache_Page();
			$applicationDebugPage->registerDebugSettingsPageDebugFields();

			$applicationSupportPage = new NeoWeb_Connector_Register_Support_Page();
			$applicationSupportPage->registerDefaultSupportFields();
			$applicationSupportPage->registerPluginSupportFields();
		}
	}

	public function loadACFields( $paths ) {

		//Add local path to ACF so that we can sync all existing fields back to the database when we run the sync
        $paths[] = plugin_dir_path( dirname( __FILE__ ) ) . 'admin/acf-json';

        // return
        return $paths;

    }

    public function saveACFields ( $path ) {

	    //Add local path to ACF so that we can sync all existing fields back to the database when we run the sync
        $path = plugin_dir_path( dirname( __FILE__ ) ) . 'admin/acf-json';

        // return
        return $path;

    }

    public function jp_sync_acf_fields() {

        $groups = acf_get_field_groups();
        $sync 	= array();

        //Bail early if no field groups
        if( empty( $groups ) )
            return;

	    $this->logger->error_logger($groups);

        // find JSON field groups which have not yet been imported
        foreach( $groups as $group ) {

            // vars
            $local 		= acf_maybe_get( $group, 'local', false );
            $modified 	= acf_maybe_get( $group, 'modified', 0 );
            $private 	= acf_maybe_get( $group, 'private', false );

            // ignore DB / PHP / private field groups
            if( $local !== 'json' || $private ) {
                // do nothing
            } elseif( ! $group[ 'ID' ] ) {
                $sync[ $group[ 'key' ] ] = $group;

            } elseif( $modified && $modified > get_post_modified_time( 'U', true, $group[ 'ID' ], true ) ) {
                $sync[ $group[ 'key' ] ]  = $group;
            }
        }

	    $this->logger->error_logger($sync);

        // bail if no sync needed
        if( empty( $sync ) )
            return;
        if( ! empty( $sync ) ) { //if( ! empty( $keys ) ) {

            // vars
            $new_ids = array();

            foreach( $sync as $key => $v ) { //foreach( $keys as $key ) {

                // append fields
                if( acf_have_local_fields( $key ) ) {

                    $sync[ $key ][ 'fields' ] = acf_get_local_fields( $key );

                }
                // import
                $field_group = acf_import_field_group( $sync[ $key ] );
            }
        }
    }
}