<?php


class NeoWeb_Connector_WaitingList_Submit
{
    private string $pageID;

	private array $menuTitles;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * __constructor.
	 *
	 */
    public function __construct()
    {
	    $this->plugin_data = get_option('neoweb-connector-waiting-list-manager');
        $this->pageID = 'waiting-list-register';
	    $this->menuTitles = array(
	    	'notification-settings' => 'Notification Settings',
		    'captcha-settings' => 'Captcha Settings',
		    'terms-and-conditions-settings' => 'Terms and Conditions Settings',
		    'restrictions' => 'Restrictions',
		    'osm-sections' => 'OSM Sections',
		    'osm-custom-field-maps' => 'OSM Custom Field Maps',
		    'shortcodes' => 'Shortcodes'
	    );
    }

    public function registerSettingsPages () {
	    $img_folder_path = plugin_dir_url( dirname(__FILE__) );
        if( function_exists('acf_add_options_page') ):

	        acf_add_options_page(array(
		        'page_title' => $this->get_plugin_data('pluginName'),
		        'menu_title' => str_replace("NeoWeb Connector - ", "", $this->get_plugin_data('pluginName')) . " (Register)",
		        'menu_slug' => $this->get_plugin_data('pluginSlug') . '-register-parent',
		        'capability' => 'manage_options',
		        'position' => '',
		        'parent_slug' => '',
		        'icon_url' => $img_folder_path . '/images/product-icon.png',
		        'redirect' => true,
	        ));

	        foreach ( $this->menuTitles as $menu_slug => $menu_title ) {
		        acf_add_options_sub_page(array(
			        'page_title'  => $this->get_plugin_data('pluginName') . ' - ' . $menu_title,
			        'menu_title'  => ucfirst($menu_title),
			        'menu_slug'   => 'neoweb-' . $menu_slug,
			        'parent_slug' => $this->get_plugin_data('pluginSlug') . '-register-parent',
		        ));
	        }

        endif;
    }

	public function registerPluginLogo($sectionID = "defaults") {
		$img_folder_path = plugin_dir_url( dirname(__FILE__) );
		foreach ( $this->menuTitles as $menu_slug => $menu_title ) {
			if ( function_exists( 'acf_add_local_field_group' ) ):
				acf_add_local_field_group( array(
					'key'       => 'group_' . $menu_slug . '-group_logo' . $sectionID,
					'name'      => $menu_slug . '-group_logo' . $sectionID,
					'title'     => 'plugin_logo',
					'fields'    => array(
						array(
							'key'     => 'field_' . 'logo_' . $menu_slug,
							'label'   => '',
							'name'    => 'logo_' . $menu_slug,
							'type'    => 'message',
							'message' => '<div class="logoWrapper">
                                <img alt="neoweb-logo" src="' . $img_folder_path . '/images/logo.png"></div>',
						)
					),
					'location' => array(
						array(
							array(
								'param'    => 'options_page',
								'operator' => '==',
								'value'    => 'neoweb-' . $menu_slug,
							),
						),
					),
					'menu_order'            => -1,
					'position'              => 'acf_after_title',
					'style'                 => 'seamless',
					'label_placement'       => 'top',
					'instruction_placement' => 'field',
					'hide_on_screen'        => '',
					'active'                => true,
					'description'           => '',
				) );
			endif;
		}
	}

    public function registerSettingsPageDefaultFields($sectionID = "defaults") {
        if( function_exists('acf_add_local_field_group') ):

            foreach ( $this->menuTitles as $menu_slug => $menu_title ) {
				//We exclude shortcodes here as its done further down per section rather then per menu_title
            	if ($menu_title != 'Shortcodes' && $menu_title != 'OSM Sections' && $menu_title != 'OSM Custom Field Maps') {
		            $parent_slug = 'group_' . 'waiting_list_options_' . $menu_slug . '_' . $sectionID;

		            $title  = ucfirst($menu_title) . ' - Default Values';

		            acf_add_local_field_group(array(
			            'key' => $parent_slug,
			            'title' => $title,
			            'location' => array(
				            array(
					            array(
						            'param' => 'options_page',
						            'operator' => '==',
						            'value' => 'neoweb-' . $menu_slug
					            ),
				            ),
			            ),
			            'menu_order' => 0,
			            'position' => 'normal',
			            'style' => 'default',
			            'label_placement' => 'top',
			            'instruction_placement' => 'field',
		            ));



		            switch($menu_slug) {
			            case 'notification-settings':
				            $this->getNotificationFields($sectionID, $parent_slug);
				            break;
			            case 'captcha-settings':
				            $this->getGoogleCaptchaFields($sectionID, $parent_slug);
				            break;
			            case 'terms-and-conditions-settings':
				            $this->getTandCFields($sectionID, $parent_slug);
				            break;
			            case 'restrictions':
				            $this->getRestrictionsFields($sectionID, $parent_slug);
				            break;
		            }
	            }
            }
        endif;
    }

    private function getNotificationFields ($sectionID, $parent) {
        //notificationTabGroup

	    //Todo: Add wrapper to style and increase font
        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_admin_msg',
            'label' => 'Admin Email Settings',
            'name' => $sectionID . '_admin_msg',
            'type' => 'accordion',
            'parent' => $parent,
        ));

	    acf_add_local_field(array(
		    'key' => 'field_' . $sectionID . '_auto_submit',
		    'label' => '',
		    'name' => $sectionID . '_auto_submit',
		    'type' => 'true_false',
		    'message' => 'Auto submit to OSM Waiting List',
		    'instructions' => 'If checked, all waiting list submissions will automatically be submitted to OSM with a backup notification via email to the below email address.
                If left unchecked, all submissions will only be done to the email address below and data will have to be added to OSM manually.',
		    'parent' => $parent,
	    ));

	    acf_add_local_field(array(
		    'key' => 'field_' . $sectionID . '_admin_redact',
		    'label' => 'Exclude entered member data from admin email',
		    'name' => $sectionID . '_admin_redact',
		    'type' => 'true_false',
		    'message' => 'Exclude entered member data from admin email',
		    'instructions' => 'If checked, all waiting list data entered by the user will be excluded from the email to 
		        the admin and only the message below will be sent to the admin.',
		    'parent' => $parent,
		    'conditional_logic' => array(
			    array(
				    array(
					    'field' => 'field_' . $sectionID . '_auto_submit',
					    'operator' => '==',
					    'value' => '1',
				    )
			    )
		    )
	    ));

	    acf_add_local_field(array(
		    'key' => 'field_' .  $sectionID . '_admin_email',
		    'label' => 'Submission notification email address',
		    'name' => $sectionID . '_admin_email',
		    'type' => 'email',
		    'instructions' => 'The email address the successful waiting list submission/notification will be sent to.',
		    'required' => 1,
		    'parent' => $parent,
	    ));

	    acf_add_local_field(array(
		    'key' => 'field_' . $sectionID . '_admin_subject',
		    'label' => 'Submission notification subject line',
		    'name' => $sectionID . '_admin_subject',
		    'type' => 'text',
		    'instructions' => 'Subject line for the waiting list submission/notification email message.
                PLEASE NOTE:
                Adding <strong>{{sectionname}}</strong> will allow you to include the name of the waiting list the young person was added too.
                Adding <strong>{{memberName}</strong> and / or <strong>{{memberLastName}}</strong> will allow for including the young persons first and last names in the email subject.
                For example: <strong>{{memberName}</strong>} was just added to the <strong>{{sectionname}}</strong> in OSM',
		    'parent' => $parent,
	    ));

	    acf_add_local_field(array(
		    'key' => 'field_' . $sectionID . '_admin_message',
		    'label' => 'Admin email notification message',
		    'name' => $sectionID . '_admin_message',
		    'type' => 'wysiwyg',
		    'tabs' => 'all',
		    'toolbar' => 'full',
		    'parent' => $parent,
	    ));

		//Todo: Add wrapper to style and increase font
        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_parent_msg',
            'label' => 'Parent/Member Email Settings',
            'name' => $sectionID . '_parent_msg',
            'type' => 'accordion',
            'message' => '',
            'parent' => $parent,
        ));

	    acf_add_local_field(array(
		    'key' => 'field_' . $sectionID . '_notify_parent',
		    'label' => '',
		    'name' => $sectionID . '_notify_parent',
		    'type' => 'true_false',
		    'parent' => $parent,
		    'message' => 'Notify parent by email of successful submission?'
	    ));

	    acf_add_local_field(array(
		    'key' => 'field_' . $sectionID . '_parent_email',
		    'label' => 'Email address to use',
		    'name' => $sectionID . '_parent_email',
		    'type' => 'radio',
		    'instructions' => '<strong>IMPORTANT: This field should be marked as visible in that section.</strong>',
		    'required' => 1,
		    'choices' => array(
			    'member' => 'Member Email Address',
			    'primary' => 'Primary Contact Email Address',
			    'secondary' => 'Secondary Contact Email Address',
			    'emergency' => 'Emergency Contact Email Address',
		    ),
		    'default_value' => 'member',
		    'parent' => $parent,
		    'conditional_logic' => array(
			    array(
				    array(
					    'field' => 'field_' . $sectionID . '_notify_parent',
					    'operator' => '==',
					    'value' => '1',
				    )
			    )
		    )
	    ));

	    acf_add_local_field(array(
		    'key' => 'field_' . $sectionID . '_parent_subject',
		    'label' => 'Parent email notification subject line',
		    'name' => $sectionID . '_parent_subject',
		    'type' => 'text',
		    'instructions' => 'Subject Line for parent message.
                PLEASE NOTE:
                Adding <strong>{{sectionname}}</strong> will allow you to include the name of the waiting list the young person was added too.
                Adding <strong>{{memberName}</strong>} and / or <strong>{{memberLastName}}</strong> will allow for including the young persons first and last names in the email subject.
                For example: <strong>{{memberName}</strong>} was successfully added to our <strong>{{sectionname}}</strong> in OSM',
		    'parent' => $parent,
		    'conditional_logic' => array(
			    array(
				    array(
					    'field' => 'field_' . $sectionID . '_notify_parent',
					    'operator' => '==',
					    'value' => '1',
				    )
			    )
		    )
	    ));

	    acf_add_local_field(array(
		    'key' => 'field_' . $sectionID . '_parent_message',
		    'label' => 'Parent email notification message',
		    'name' => $sectionID . '_parent_message',
		    'type' => 'wysiwyg',
		    'tabs' => 'all',
		    'toolbar' => 'full',
		    'parent' => $parent,
		    'conditional_logic' => array(
			    array(
				    array(
					    'field' => 'field_' . $sectionID . '_notify_parent',
					    'operator' => '==',
					    'value' => '1',
				    )
			    )
		    )
	    ));

	    //Todo: Add wrapper to style and increase font
	    acf_add_local_field(array(
		    'key' => 'field_' . $sectionID . 'onscreen_msg',
		    'label' => 'On-Screen Notification Settings',
		    'name' => $sectionID . 'onscreen_msg',
		    'type' => 'accordion',
		    'message' => '',
		    'parent' => $parent,
	    ));

        acf_add_local_field(array(
	        'key' => 'field_' . $sectionID . '_onscreen_notification',
            'label' => 'Parent on-screen notification message',
	        'key' => $sectionID . '_onscreen_notification',
            'type' => 'textarea',
            'instructions' => 'The on-screen message shown to the user on the successful submission of the waiting list.',
            'parent' => $parent,
        ));

    }

	private function getGoogleCaptchaFields($sectionID, $parent) {
		//googleCaptchaTabGroup
		acf_add_local_field(array(
			'key' => 'field_' . $sectionID . '_2.0',
			'label' => '',
			'name' => $sectionID . '_2.0',
			'type' => 'true_false',
			'message' => 'Revert to version 2.0 of Google CAPTCHA',
			'parent' => $parent,
		));

		acf_add_local_field(array(
			'key' => 'field_' . $sectionID . 'captcha_instructions',
			'label' => 'Instructions',
			'name' => $sectionID . 'captcha_instructions',
			'type' => 'message',
			'message' => '<p class="neowebNotice">To obtain a google site &amp; secret key, head over to <a href="https://www.google.com/recaptcha" target="_blank">https://www.google.com/recaptcha</a>. <br/>
                                                    Login with your gMail account or create a new one <br/>
                                                    In the register new site section select reCAPTCHA v3 <strong>(Select v2 if you opted to use version 2 above)</strong> <br/>
                                                    Enter your domain name (with and without www) <br/>
                                                    Accept the reCaptcha T&amp;C <br/>
                                                    Click Register <br/>
                                                    Copy the site and secret keys provided into the fields below.</p>',
			'parent' => $parent,
		));

		acf_add_local_field(array(
			'key' => 'field_' . $sectionID . '_google_site_key',
			'label' => 'Google Site Key',
			'name' => $sectionID . '_google_site_key',
			'type' => 'text',
			'required' => 1,
			'parent' => $parent,
		));

		acf_add_local_field(array(
			'key' => 'field_' . $sectionID . '_google_secret_key',
			'label' => 'Google Secret Key',
			'name' => $sectionID . '_google_secret_key',
			'type' => 'text',
			'required' => 1,
			'parent' => $parent,
		));
	}

	private function getTandCFields($sectionID, $parent) {
		//termsAndConditionsTabGroup
		acf_add_local_field(array(
			'key' => 'field_' . $sectionID . '_show_tc',
			'label' => '',
			'name' => $sectionID . '_show_tc',
			'type' => 'true_false',
			'message' => 'Show Terms and Conditions checkbox?',
			'parent' => $parent,
		));

		acf_add_local_field(array(
			'key' => 'field_' . $sectionID . '_tc_text',
			'label' => 'T&C Text',
			'name' => $sectionID . '_tc_text',
			'type' => 'text',
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_' . $sectionID . '_show_tc',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'parent' => $parent,
		));

		acf_add_local_field(array(
			'key' => 'field_' . $sectionID . '_tc_link',
			'label' => 'T&C Link',
			'name' => $sectionID . '_tc_link',
			'type' => 'link',
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_' . $sectionID . '_show_tc',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'return_format' => 'array',
			'parent' => $parent,
		));
	}

	private function getRestrictionsFields($sectionID, $parent) {
		//whiteSpaceTabGroup
		acf_add_local_field(array(
			'key' => 'field_' . $sectionID . '_remove_whitespace',
			'label' => '',
			'name' => $sectionID . '_remove_whitespace',
			'type' => 'true_false',
			'message' => 'Remove white space from postcode',
			'parent' => $parent,
		));
	}

	public function registerShortCodes($sectionName, $sectionID) {
        if( function_exists('acf_add_local_field_group') ):

	        acf_add_local_field_group(array(
		        'key' => 'group_' . $sectionID . '_shortcodes',
		        'title' => "Short code and override settings for $sectionName",
		        'fields' => array (),
		        'location' => array(
			        array(
				        array(
					        'param' => 'options_page',
					        'operator' => '==',
					        'value' => 'neoweb-shortcodes'
				        ),
			        ),
		        ),
	        ));


	        acf_add_local_field( array(
		        'key'           => 'field_' . $sectionID . 'waiting_list_register_shortcode',
		        "label"         => "Registration form for:<br/>Section Name: <strong>$sectionName</strong> (Section ID: $sectionID)",
		        'name'          => $sectionID . 'waiting_list_register_shortcode',
		        'type'          => 'text',
		        'wrapper' => array(
			        'class' => 'shortCodeCopy',
		        ),
		        'readonly'      => 1,
		        'default_value' => "[OSM_WaitingList_Register sectionid='$sectionID' sectionname='$sectionName']",
		        'parent'        => 'group_' .$sectionID . '_shortcodes'
	        ) );

	        acf_add_local_field(array(
		        'key' => 'field_' . $sectionID . '_override',
		        'label' => 'Override default settings',
		        'name' => $sectionID . '_override',
		        'type' => 'true_false',
		        'message' => 'Activate overrides for this waiting list?',
		        'instructions' => 'To activate overrides for this waiting list, select this field and then safe the page by clicking the "Update" button. 
		            A new top level menu will be added for this waiting list',
		        'parent' => 'group_' .$sectionID . '_shortcodes'
	        ));

        endif;
    }

	private function acf_sync_import_json_field_groups($directory) {

		$dir = new DirectoryIterator( $directory );

		foreach( $dir as $file ) {
			//Check if the file is one of mine

			if ( strpos( $file->getFilename(), 'group_waiting_list_' ) !== false ) {

				if ( ! $file->isDot() && $file->getExtension() == 'json' ) {

					$json = json_decode( file_get_contents( $file->getPathname() ), true );

					// What follows is basically a copy of import() in ACF admin/settings-export.php

					// if importing an auto-json, wrap field group in array
					if ( isset( $json['key'] ) ) {

						$json = array( $json );

					}

					// vars
					$added   = array();
					$ignored = array();
					$ref     = array();
					$order   = array();

					foreach ( $json as $field_group ) {

						// remove fields
						$fields = acf_extract_var( $field_group, 'fields' );


						// format fields
						$fields = acf_prepare_fields_for_import( $fields );


						// save field group
						$field_group = acf_update_field_group( $field_group );


						// add to ref
						$ref[ $field_group['key'] ] = $field_group['ID'];


						// add to order
						$order[ $field_group['ID'] ] = 0;


						// add fields
						foreach ( $fields as $field ) {

							// add parent
							if ( empty( $field['parent'] ) ) {

								$field['parent'] = $field_group['ID'];

							} elseif ( isset( $ref[ $field['parent'] ] ) ) {

								$field['parent'] = $ref[ $field['parent'] ];

							}

							// add field menu_order
							if ( ! isset( $order[ $field['parent'] ] ) ) {

								$order[ $field['parent'] ] = 0;

							}

							$field['menu_order'] = $order[ $field['parent'] ];
							$order[ $field['parent'] ] ++;

							// save field
							$field = acf_update_field( $field );

						}

					}
				}
			}
		}

	}

	public function runAutoSync() {
		// Remove previous field groups in DB
		$args = array(
			'post_type'         => 'acf-field-group',
			's'                 => 'NeoWeb Connector - OSM',
			'post_status'       => 'any',
			'posts_per_page'    => -1
		);

		$query = new WP_Query( $args );

		foreach ( $query->posts as $acf_group ) {
			wp_delete_post( $acf_group->ID, true);
		}

		//Get all json save points
		$json_dirs = acf_get_setting('load_json');

		foreach ( $json_dirs as $dir ) {
			$this->acf_sync_import_json_field_groups($dir);
		}
	}
}