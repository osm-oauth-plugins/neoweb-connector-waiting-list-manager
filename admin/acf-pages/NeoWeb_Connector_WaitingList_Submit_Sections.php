<?php


class NeoWeb_Connector_WaitingList_Submit_Sections
{
    private string $defaultsFileName;
	private string $overrideFileName;
	private string $section_id;
	private string $section_title;
	private array $customData;
	private string $customFieldDefaultsFileName;
	private string $customFieldOverrideFileName;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * __constructor.
	 *
	 * @param $sectionTitle
	 * @param $sectionID
	 * @param $customData
	 */
    public function __construct( $sectionTitle, $sectionID, $customData )
    {
	    $this->plugin_data = get_option('neoweb-connector-waiting-list-manager');

	    $this->section_id = $sectionID;
        $this->section_title = $sectionTitle;
        $this->customData = $customData;

	    $this->defaultsFileName = plugin_dir_path( dirname( __FILE__ ) ) . 'acf-json/group_waiting_list_defaults.json';
	    $this->overrideFileName = plugin_dir_path( dirname( __FILE__ ) ) . 'acf-json/group_waiting_list_' . $this->get_section_id() . '.json';

	    $this->customFieldDefaultsFileName = plugin_dir_path( dirname( __FILE__ ) ) . 'acf-json/group_waiting_list_custom_defaults.json';
	    $this->customFieldOverrideFileName = plugin_dir_path( dirname( __FILE__ ) ) . 'acf-json/group_waiting_list_custom_' . $this->get_section_id() . '.json';

    }

    private function replace_in_array(&$value, $key, $replacements) {
	    $value = str_replace( array_keys($replacements), $replacements, $value );
    }

    private function createOverrideJSON($overrideData, $fileName) {
	    $img_folder_path = plugin_dir_url( dirname(__FILE__) );

	    $replacements = array(
		    'defaults' => $this->get_section_id(),
		    'NeoWeb Connector - OSM Sections - Default Settings' => 'NeoWeb Connector - OSM Sections - ' . $this->get_section_title(),
		    'NeoWeb Connector - OSM Custom Field Mappings - Defaults' => 'NeoWeb Connector - OSM Custom Field Mappings - ' . $this->get_section_title(),
		    '$$IMAGE_FOLDER_PATH$$' => $img_folder_path
	    );

	    array_walk_recursive( $overrideData, array($this, 'replace_in_array'), $replacements );

	    $fp = fopen($fileName, 'w');
	    fwrite($fp, json_encode($overrideData));
	    fclose($fp);
    }

    public function registerOverrideFields($syncRequired) {

	    //check if the defaults file exists
	    //it should but just in case!!
	    if (file_exists($this->defaultsFileName)) {
		    $defaultFieldValues = file_get_contents($this->defaultsFileName);
		    $defaultData = json_decode($defaultFieldValues, true);

		    //check if the override file exists
		    if (file_exists($this->overrideFileName)) {

			    //Override exists lets check its version number
			    $overrideFieldValues = file_get_contents($this->overrideFileName);
			    $overrideData = json_decode($overrideFieldValues, true);

			    if (isset($overrideData[0]['version_number'])) {
				    //Get the version number form the override file we found
				    $version_number = $overrideData[0]['version_number'];

				    /**
				     * Check if the override version number is less than the defaults,
				     * if it is we need to recreate the override to get the new default configuration values,
				     * if they match we do not need to do anything
				     **/
				    if ($version_number < $defaultData[0]['version_number']) {
					    $this->createOverrideJSON($defaultData, $this->overrideFileName);
					    $syncRequired = true;
				    }
			    } else {
			    	//The file did not have a version number so recreate anyway
				    $this->createOverrideJSON($defaultData, $this->overrideFileName);
				    $syncRequired = true;
			    }
		    } else {
			    //Override does not exist so lets create it and sync it
			    $this->createOverrideJSON($defaultData, $this->overrideFileName);
			    $syncRequired = true;
		    }
	    } else {
	    	//TODO: should probably add error here to reinstall plugin as a vital file is missing
	    }

	    return $syncRequired;

    }

    public function registerCustomOverrideFields($syncRequired) {

	    //check if the defaults file exists
	    //it should but just in case!!
	    if (file_exists($this->customFieldDefaultsFileName)) {
		    $defaultFieldValues = file_get_contents($this->customFieldDefaultsFileName);
		    $defaultData = json_decode($defaultFieldValues, true);

		    //check if the override file exists
		    if (file_exists($this->customFieldOverrideFileName)) {

			    //Override exists lets check its version number
			    $overrideFieldValues = file_get_contents($this->customFieldOverrideFileName);
			    $overrideData = json_decode($overrideFieldValues, true);

			    if (isset($overrideData[0]['version_number'])) {
				    //Get the version number form the override file we found
				    $version_number = $overrideData[0]['version_number'];

				    /**
				     * Check if the override version number is less than the defaults,
				     * if it is we need to recreate the override to get the new default configuration values,
				     * if they match we do not need to do anything
				     **/
				    if ($version_number < $defaultData[0]['version_number']) {
					    $this->createOverrideJSON($defaultData, $this->customFieldOverrideFileName);
					    $syncRequired = true;
				    }
			    } else {
			    	//The file did not have a version number so recreate anyway
				    $this->createOverrideJSON($defaultData, $this->customFieldOverrideFileName);
				    $syncRequired = true;
			    }
		    } else {
			    //Override does not exist so lets create it and sync it
			    $this->createOverrideJSON($defaultData, $this->customFieldOverrideFileName);
			    $syncRequired = true;
		    }
	    } else {
	    	//TODO: should probably add error here to reinstall plugin as a vital file is missing
	    }

	    return $syncRequired;

    }

	public function registerCustomFields () {

		$fieldOptions = array();
		$fieldOptions[0] = "Please select...";
		if (is_array($this->get_custom_data()) || is_object($this->get_custom_data())) {
			foreach ($this->get_custom_data()['data'] as $customData) {
				foreach ($customData['fields'] as $key => $field) {
					if ($field['is_core'] == false) {
						$customFieldID = explode(":", $key);
						$groupLabel = "";
						switch ($customData['group_id']) {
							//Primary Contact1
							case 0:
								$groupLabel = "Primary Contact 1";
								break;

							//Secondary Contact
							case 1:
								$groupLabel = "Primary Contact 2";
								break;

							//Emergency Contact
							case 2:
								$groupLabel = "Emergency Contact";
								break;

							//Doctor
							case 3:
								$groupLabel = "Doctor";
								break;

							//Member
							case 4:
								$groupLabel = "Member";
								break;

							//Custom Date
							case 5:
								$groupLabel = "Additional";
								break;

						}
						$fieldOptions[$customFieldID[1] . ":" . $field['column_id'] . ":" . $customData['group_id']] = $groupLabel . ": " . $field['label'];
					}
				}
			}

			/*
			//Add the fiend to the defaults
			acf_add_local_field(array(
				'key' => 'field_defaults_' . 'field_mapping_' . $this->get_section_id(),
				"label" => $this->get_section_title(),
				'name' => 'field_defaults_' . 'field_mapping_' . $this->get_section_id(),
				'type' => 'select',
				'default_value' => '',
				'parent' => 'field_defaults_field_map',
				'choices' => $fieldOptions
			));
			*/
/*
			//Add the field to the overrides for this section
			acf_add_local_field(array(
				'key' => 'field_' . 'field_mapping_' . $this->get_section_id(),
				"label" => $this->get_section_title(),
				'name' => 'field_' . 'field_mapping_' . $this->get_section_id(),
				'type' => 'select',
				'default_value' => '',
				'parent' => 'field_' . $this->get_section_id() . '_field_map' ,
				'choices' => $fieldOptions
			));
*/
		}
	}

	private function get_section_id(): string {
    	return $this->section_id;
	}

	private function get_section_title(): string {
    	return $this->section_title;
	}

	private function get_custom_data(): array {
    	return $this->customData;
	}
}