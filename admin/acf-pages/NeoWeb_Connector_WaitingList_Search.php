<?php


class NeoWeb_Connector_WaitingList_Search
{

    private NeoWeb_Connector_Loggers $logger;

    private string $pageID;

    private array $menuTitles;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 *  __constructor.
	 *
	 */
    public function __construct()
    {
	    $this->plugin_data = get_option('neoweb-connector-waiting-list-manager');
        $this->pageID = 'neoweb-waiting-list-search-';
        $this->menuTitles = array(
        	'search-settings' => 'Search Settings'
        );

    }

    public function registerSettingsPage ($section = "Defaults", $sectionID = "defaults") {
	    $img_folder_path = plugin_dir_url( dirname(__FILE__) );
        if( function_exists('acf_add_options_page') ):

	        acf_add_options_page(array(
		        'page_title' => $this->get_plugin_data('pluginName'),
		        'menu_title' => str_replace("NeoWeb Connector - ", "", $this->get_plugin_data('pluginName')) . " (Search)",
		        'menu_slug' => $this->get_plugin_data('pluginSlug') . '-search-parent',
		        'capability' => 'manage_options',
		        'position' => '',
		        'parent_slug' => '',
		        'icon_url' => $img_folder_path . '/images/product-icon.png',
		        'redirect' => true,
	        ));

	        foreach ( $this->menuTitles as $menu_slug => $menu_title ) {
		        acf_add_options_sub_page(array(
			        'page_title'  => $this->get_plugin_data('pluginName') . ' - ' . $menu_title,
			        'menu_title'  => $menu_title,
			        'menu_slug'   => 'neoweb-' . $menu_slug,
			        'parent_slug' => $this->get_plugin_data('pluginSlug') . '-search-parent',
		        ));
            }

        endif;
    }

	public function registerPluginLogo() {
		$img_folder_path = plugin_dir_url( dirname(__FILE__) );
		foreach ( $this->menuTitles as $menu_slug => $menu_title ) {
			if ( function_exists( 'acf_add_local_field_group' ) ):
				acf_add_local_field_group( array(
					'key'                   => 'group_' . $menu_slug . '-group_logo',
					'name'                   => $menu_slug . '-group_logo',
					'title'                 => 'Search Settings',
					'fields'                => array(
						array(
							'key'     => 'logo_' . $menu_slug,
							'label'   => '',
							'name'    => 'logo_' . $menu_slug,
							'type'    => 'message',
							'message' => '<div class="logoWrapper">
        <img alt="neoweb-logo" src="' . $img_folder_path . '/images/logo.png"/></div>',
						)
					),
					'location'              => array(
						array(
							array(
								'param'    => 'options_page',
								'operator' => '==',
								'value'    => 'neoweb-' . $menu_slug,
							),
						),
					),
					'menu_order'            => 0,
					'position'              => 'normal',
					'style'                 => 'seamless',
					'label_placement'       => 'top',
					'instruction_placement' => 'field',
					'hide_on_screen'        => '',
					'active'                => true,
					'description'           => '',
				) );

			endif;
		}
	}

    public function registerSettingsPageDefaultFields($section = "Defaults", $sectionID = "_defaults") {
        if( function_exists('acf_add_local_field_group') ):
	        foreach ( $this->menuTitles as $menu_key => $menu_title) {
		        acf_add_local_field_group( array(
			        'key'                   => 'group_' . 'waiting_list_search_options' . $sectionID,
			        'title'                 => 'Waiting list search options (' . $section . ')',
			        'fields'                => array(
				        array(
					        'key'               => 'show_list_count_' . $sectionID,
					        'label'             => 'Show list counter',
					        'name'              => 'show_list_count_' . $sectionID,
					        'type'              => 'true_false',
					        'instructions'      => '',
					        'required'          => 0,
					        'conditional_logic' => 0,
					        'wrapper'           => array(
						        'width' => '',
						        'class' => '',
						        'id'    => '',
					        ),
					        'message'           => '',
					        'default_value'     => 0,
					        'ui'                => 0,
					        'ui_on_text'        => '',
					        'ui_off_text'       => '',
				        ),
				        array(
					        'key'               => 'message_to_show_before_waiting_list_count' . $sectionID,
					        'label'             => 'Message shown before waiting list count',
					        'name'              => 'message_to_show_before_waiting_list_count' . $sectionID,
					        'type'              => 'text',
					        'instructions'      => '',
					        'required'          => 0,
					        'conditional_logic' => 0,
					        'wrapper'           => array(
						        'width' => '',
						        'class' => '',
						        'id'    => '',
					        ),
					        'default_value'     => '',
					        'placeholder'       => '',
					        'prepend'           => '',
					        'append'            => '',
					        'maxlength'         => '',
				        ),
				        array(
					        'key'               => 'message_to_show_before_waiting_list_search' . $sectionID,
					        'label'             => 'Message shown before search results',
					        'name'              => 'message_to_show_before_waiting_list_search' . $sectionID,
					        'type'              => 'text',
					        'instructions'      => '',
					        'required'          => 0,
					        'conditional_logic' => 0,
					        'wrapper'           => array(
						        'width' => '',
						        'class' => '',
						        'id'    => '',
					        ),
					        'default_value'     => '',
					        'placeholder'       => '',
					        'prepend'           => '',
					        'append'            => '',
					        'maxlength'         => '',
				        ),
				        array(
					        'key'               => 'include_dob_in_search_results' . $sectionID,
					        'label'             => 'Include members\'s DOB in search results (Format shown YYYY-MM-**)',
					        'name'              => 'include_dob_in_search_results' . $sectionID,
					        'type'              => 'true_false',
					        'instructions'      => '',
					        'required'          => 0,
					        'conditional_logic' => 0,
					        'wrapper'           => array(
						        'width' => '',
						        'class' => '',
						        'id'    => '',
					        ),
					        'message'           => '',
					        'default_value'     => 0,
					        'ui'                => 0,
					        'ui_on_text'        => '',
					        'ui_off_text'       => '',
				        ),
				        array(
					        'key'               => 'include_joining_date_in_search_results' . $sectionID,
					        'label'             => 'Include members\'s expected joining date as set in OSM',
					        'name'              => 'include_joining_date_in_search_results' . $sectionID,
					        'type'              => 'true_false',
					        'instructions'      => '',
					        'required'          => 0,
					        'conditional_logic' => 0,
					        'wrapper'           => array(
						        'width' => '',
						        'class' => '',
						        'id'    => '',
					        ),
					        'message'           => '',
					        'default_value'     => 0,
					        'ui'                => 0,
					        'ui_on_text'        => '',
					        'ui_off_text'       => '',
				        ),
				        array(
					        'key'               => 'include_custom_data_in_search_results' . $sectionID,
					        'label'             => 'Include custom field from OSM',
					        'name'              => 'include_custom_data_in_search_results' . $sectionID,
					        'type'              => 'true_false',
					        'instructions'      => '',
					        'required'          => 0,
					        'conditional_logic' => 0,
					        'wrapper'           => array(
						        'width' => '',
						        'class' => '',
						        'id'    => '',
					        ),
					        'message'           => '',
					        'default_value'     => 0,
					        'ui'                => 0,
					        'ui_on_text'        => '',
					        'ui_off_text'       => '',
				        )
			        ),
			        'location'              => array(
				        array(
					        array(
						        'param'    => 'options_page',
						        'operator' => '==',
						        'value'    => 'neoweb-' . $menu_key,
					        ),
				        ),
			        ),
			        'menu_order'            => 0,
			        'position'              => 'normal',
			        'style'                 => 'default',
			        'label_placement'       => 'left',
			        'instruction_placement' => 'label',
			        'hide_on_screen'        => '',
			        'active'                => true,
			        'description'           => '',
		        ) );
	        }
        endif;
    }

    public function registerCustomFields ($custom_data, $sectionName, $sectionID) {

        acf_add_local_field_group(array(
            'key' => 'group_' . $sectionID . '_osm_custom_fields',
            'title' => 'OSM Custom Field Mappings - ' . $sectionName,
            'fields' => array(
                array(
                    'key' => 'field_' . $sectionID . '_custom_capture',
                    'label' => 'Display this section on the user form?',
                    'name' => 'field_' . $sectionID . '_custom_capture',
                    'type' => 'true_false',
                    'instructions' => '',
                    'required' => false,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => 'Capture data into a custom OSM field?',
                    'default_value' => 0,
                    'ui' => 0,
                    'ui_on_text' => '',
                    'ui_off_text' => '',
                ),
                array(
                    'key' => 'field_' . $sectionID . '_custom_sectionHeading',
                    'label' => 'Section Heading',
                    'name' => 'field_' . $sectionID . '_custom_sectionHeading',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => false,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_' . $sectionID . '_custom_capture',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => '',
                    'default_value' => '',
                    'maxlength' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                ),
                array(
                    'key' => 'field_' . $sectionID . '_custom_fields',
                    'label' => 'Custom fields and OSM field mappings',
                    'name' => 'field_' . $sectionID . '_custom_fields',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_' . $sectionID . '_custom_capture',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'collapsed' => '',
                    'min' => 0,
                    'max' => 0,
                    'layout' => 'block',
                    'button_label' => 'Add a custom field mapping',
                    'sub_fields' => array(
                        array(
                            'key' => 'field_' . $sectionID . '_label',
                            'label' => 'Label',
                            'name' => 'field_' . $sectionID . '_label',
                            'type' => 'text',
                            'instructions' => 'Label shown on the form.',
                            'required' => 1,
                            'conditional_logic' => false,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'maxlength' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                        ),
                        array(
                            'key' => 'field_' . $sectionID . '_placeholder',
                            'label' => 'Field Placeholder Text',
                            'name' => 'field_' . $sectionID . '_placeholder',
                            'type' => 'text',
                            'instructions' => 'This value will be printed inside the input field on the form as an indication/reminder to the user of what needs to be entered.',
                            'required' => false,
                            'conditional_logic' => false,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'maxlength' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                        ),
                        array(
                            'key' => 'field_' . $sectionID . '_required',
                            'label' => '',
                            'name' => 'field_' . $sectionID . '_required',
                            'type' => 'true_false',
                            'instructions' => '',
                            'required' => false,
                            'conditional_logic' => false,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'message' => 'Required field?',
                            'default_value' => 0,
                            'ui' => 0,
                            'ui_on_text' => '',
                            'ui_off_text' => '',
                        ),
                        array(
                            'key' => 'field_' . $sectionID . '_error',
                            'label' => 'Error message for missing value',
                            'name' => 'field_' . $sectionID . '_error',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => false,
                            'conditional_logic' => array(
                                array(
                                    array(
                                        'field' => 'field_' . $sectionID . '_required',
                                        'operator' => '==',
                                        'value' => '1',
                                    ),
                                ),
                            ),
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'maxlength' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                        ),
                        array(
                            'key' => 'field_' . $sectionID . '_min',
                            'label' => 'Minimum number of characters',
                            'name' => 'field_' . $sectionID . '_min',
                            'type' => 'number',
                            'instructions' => '',
                            'required' => false,
                            'conditional_logic' => array(
                                array(
                                    array(
                                        'field' => 'field_' . $sectionID . '_required',
                                        'operator' => '==',
                                        'value' => '1',
                                    ),
                                ),
                            ),
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'min' => '',
                            'max' => '',
                            'step' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                        ),
                        array(
                            'key' => 'field_' . $sectionID . '_max',
                            'label' => 'Maximum number of characters',
                            'name' => 'field_' . $sectionID . '_max',
                            'type' => 'number',
                            'instructions' => '',
                            'required' => false,
                            'conditional_logic' => array(
                                array(
                                    array(
                                        'field' => 'field_' . $sectionID . '_required',
                                        'operator' => '==',
                                        'value' => '1',
                                    ),
                                ),
                            ),
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'min' => '',
                            'max' => '',
                            'step' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                        ),
                        array(
                            'key' => 'field_' . $sectionID . '_redact',
                            'label' => '',
                            'name' => 'field_' . $sectionID . '_redact',
                            'type' => 'true_false',
                            'instructions' => '',
                            'required' => false,
                            'conditional_logic' => false,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'message' => 'Redact field from admin email?',
                            'default_value' => 0,
                            'ui' => 0,
                            'ui_on_text' => '',
                            'ui_off_text' => '',
                        ),
                        array(
                            'key' => 'field_' . $sectionID . '_redact_parent',
                            'label' => '',
                            'name' => 'field_' . $sectionID . '_redact_parent',
                            'type' => 'true_false',
                            'instructions' => '',
                            'required' => false,
                            'conditional_logic' => false,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'message' => 'Redact field from parent/member email?',
                            'default_value' => 0,
                            'ui' => 0,
                            'ui_on_text' => '',
                            'ui_off_text' => '',
                        ),
                        array(
                            'key' => 'field_' . $sectionID . '_field_type',
                            'label' => 'Field Type',
                            'name' => 'field_' . $sectionID . '_field_type',
                            'type' => 'radio',
                            'instructions' => '',
                            'required' => false,
                            'conditional_logic' => false,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'choices' => array(
                                'text' => 'Text Field',
                                'number' => 'Number Field',
                                'select' => 'Select Field (Dropdown Field)',
                                'multiselect' => 'Multi Select Field',
                            ),
                            'default_value' => 'text',
                            'layout' => 'horizontal',
                            'return_format' => 'value',
                            'other_choice' => 0,
                            'save_other_choice' => 0,
                            'allow_null' => 0,
                        ),
                        array(
                            'key' => 'field_' . $sectionID . '_osm_options',
                            'label' => 'Select / Multi select dropdown options',
                            'name' => 'field_' . $sectionID . '_osm_options',
                            'type' => 'repeater',
                            'instructions' => '',
                            'required' => 1,
                            'conditional_logic' => array(
                                array(
                                    array(
                                        'field' => 'field_' . $sectionID . '_field_type',
                                        'operator' => '==',
                                        'value' => 'select',
                                    ),
                                ),
                                array(
                                    array(
                                        'field' => 'field_' . $sectionID . '_field_type',
                                        'operator' => '==',
                                        'value' => 'multiselect',
                                    ),
                                ),
                            ),
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'collapsed' => '',
                            'min' => 0,
                            'max' => 0,
                            'layout' => 'block',
                            'button_label' => 'Add new option',
                            'sub_fields' => array(
                                array(
                                    'key' => 'field_' . $sectionID . '_osm_field_options',
                                    'label' => 'Label',
                                    'name' => 'field_' . $sectionID . '_osm_field_options',
                                    'type' => 'text',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array(
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'maxlength' => '',
                                ),
                            ),
                        ),
                        array(
                            'key' => 'field_' . $sectionID . '_field_map',
                            'label' => 'Available custom fields in OSM per waiting list',
                            'name' => 'field_' . $sectionID . '_field_map',
                            'type' => 'group',
                            'instructions' => 'These values will be used to map entered/selected data to fields in OSM',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'layout' => 'block',
                            'sub_fields' => array(
                            ),
                        ),
                    ),
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'neoweb-custom',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => array(
            ),
            'active' => true,
            'description' => '',
        ));


        /*
        acf_add_local_field_group(array(
            'key' => 'group_'.$sectionID.'_osm_custom_fields',
            'title' => 'OSM Custom Field Mappings - ' . $sectionName,
            'fields' => array (
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'neoweb-custom',
                    ),
                ),
            ),
        ));

        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_custom_capture',
            'label' => 'Display this section on the user form?',
            'name' => 'field_' . $sectionID . '_custom_capture',
            'type' => 'true_false',
            'message' => 'Capture data into a custom OSM field?',
            'conditional_logic' => 0,
            'parent' => 'group_'.$sectionID.'_osm_custom_fields'
        ));

        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_custom_sectionHeading',
            'label' => 'Section Heading',
            'name' => 'field_' . $sectionID . '_custom_sectionHeading',
            'type' => 'text',
            'message' => '',
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_' . $sectionID . '_custom_capture',
                        'operator' => '==',
                        'value' => '1',
                    )
                )
            ),
            'parent' => 'group_'.$sectionID.'_osm_custom_fields'
        ));

        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_custom_fields' ,
            'label' => 'Custom fields and OSM field mappings',
            'name' => 'field_' . $sectionID . '_custom_fields' ,
            'type' => 'repeater',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_' . $sectionID . '_custom_capture',
                        'operator' => '==',
                        'value' => '1',
                    )
                )
            ),
            'collapsed' => '',
            'min' => 0,
            'max' => 0,
            'layout' => 'block',
            'button_label' => 'Add a custom field mapping',
            'parent' => 'group_'.$sectionID.'_osm_custom_fields'
        ));


        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_label',
            'label' => 'Label',
            'name' => 'field_' . $sectionID . '_label',
            'type' => 'text',
            'instructions' => 'Label shown on the form.',
            'required' => 1,
            'parent' => 'field_' . $sectionID . '_custom_fields'
        ));

        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_placeholder',
            'label' => 'Field Placeholder Text',
            'name' => 'field_' . $sectionID . '_placeholder',
            'type' => 'text',
            'instructions' => 'This value will be printed inside the input field on the form as an indication/reminder to the user of what needs to be entered.',
            'parent' => 'field_' . $sectionID . '_custom_fields'
        ));


        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_required',
            'label' => '',
            'name' => 'field_' . $sectionID . '_required',
            'type' => 'true_false',
            'message' => 'Required field?',
            'parent' => 'field_' . $sectionID . '_custom_fields'
        ));


        acf_add_local_field(array(
            'key' =>  'field_' . $sectionID . '_error',
            'label' => 'Error message for missing value',
            'name' => 'field_' . $sectionID . '_error',
            'type' => 'text',
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_' . $sectionID . '_required',
                        'operator' => '==',
                        'value' => '1',
                    )
                )
            ),
            'parent' => 'field_' . $sectionID . '_custom_fields'
        ));

        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_min',
            'label' => 'Minimum number of characters',
            'name' => 'field_' . $sectionID . '_min',
            'type' => 'number',
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_' . $sectionID . '_required',
                        'operator' => '==',
                        'value' => '1',
                    )
                )
            ),
            'parent' => 'field_' . $sectionID . '_custom_fields'
        ));

        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_max',
            'label' => 'Maximum number of characters',
            'name' => 'field_' . $sectionID . '_max',
            'type' => 'number',
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_' . $sectionID . '_required',
                        'operator' => '==',
                        'value' => '1',
                    )
                )
            ),
            'parent' => 'field_' . $sectionID . '_custom_fields'
        ));

        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_redact',
            'label' => '',
            'name' => 'field_' . $sectionID . '_redact',
            'type' => 'true_false',
            'message' => 'Redact field from admin email?',
            'parent' => 'field_' . $sectionID . '_custom_fields'
        ));

        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_redact_parent',
            'label' => '',
            'name' => 'field_' . $sectionID . '_redact_parent',
            'type' => 'true_false',
            'message' => 'Redact field from parent/member email?',
            'parent' => 'field_' . $sectionID . '_custom_fields'
        ));

        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_field_type',
            'label' => 'Field Type',
            'name' => 'field_' . $sectionID . '_field_type',
            'type' => 'radio',
            'choices' => array(
                'text' => 'Text Field',
                'number' => 'Number Field',
                'select' => 'Select Field (Dropdown Field)',
                'multiselect' => 'Multi Select Field',
            ),
            'default_value' => 'text',
            'layout' => 'horizontal',
            'return_format' => 'value',
            'parent' => 'field_' . $sectionID . '_custom_fields'
        ));

        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_osm_options',
            'label' => 'Select / Multi select dropdown options',
            'name' => 'field_' . $sectionID . '_osm_options',
            'type' => 'repeater',
            'required' => 1,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_' . $sectionID . '_field_type',
                        'operator' => '==',
                        'value' => 'select',
                    ),
                ),
                array(
                    array(
                        'field' => 'field_' . $sectionID . '_field_type',
                        'operator' => '==',
                        'value' => 'multiselect',
                    ),
                ),
            ),
            'collapsed' => '',
            'min' => 0,
            'max' => 0,
            'layout' => 'block',
            'button_label' => 'Add new option',
            'parent' => 'field_' . $sectionID . '_custom_fields'
        ));

        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_osm_field_options',
            'label' => 'Label',
            'name' => 'field_' . $sectionID . '_osm_field_options',
            'type' => 'text',
            'required' => 0,
            'conditional_logic' => 0,
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'parent' =>	'field_' . $sectionID . '_osm_options',
        ));

        acf_add_local_field(array(
            'key' => 'field_' . $sectionID . '_field_map',
            'label' => 'Available custom fields in OSM per waiting list',
            'name' => 'field_' . $sectionID . '_field_map',
            'type' => 'group',
            'instructions' => 'These values will be used to map entered/selected data to fields in OSM',
            'required' => 1,
            'conditional_logic' => 0,
            'layout' => 'block',
            'parent' => 'field_' . $sectionID . '_custom_fields'
        ));




        $fieldOptions    = array();
	    $fieldOptions[0] = "Please select...";
	    if ( is_array( $custom_data ) || is_object( $custom_data ) ) {
		    foreach ( $custom_data['data'] as $customData ) {
			    foreach ( $customData['fields'] as $key => $field ) {
				    if ( $field['is_core'] == false ) {
					    $customFieldID = explode( ":", $key );
					    $groupLabel    = "";
					    switch ( $customData['group_id'] ) {
						    //Primary Contact1
						    case 0:
							    $groupLabel = "Primary Contact 1";
							    break;

						    //Secondary Contact
						    case 1:
							    $groupLabel = "Primary Contact 2";
							    break;

						    //Emergency Contact
						    case 2:
							    $groupLabel = "Emergency Contact";
							    break;

						    //Doctor
						    case 3:
							    $groupLabel = "Doctor";
							    break;

						    //Member
						    case 4:
							    $groupLabel = "Member";
							    break;

						    //Custom Date
						    case 5:
							    $groupLabel = "Additional";
							    break;

					    }
					    $fieldOptions[ $customFieldID[1] . ":" . $field['column_id'] . ":" . $customData['group_id'] ] = $groupLabel . ": " . $field['label'];
				    }
			    }
		    }

		    acf_add_local_field( array(
			    'key'           => 'neoweb_connector_show_custom_field_' . $sectionID,
			    "label"         => $sectionName,
			    'name'          => 'neoweb_connector_show_custom_field_' . $sectionID,
			    'type'          => 'select',
			    'default_value' => '',
			    'instructions'  => 'Display the value of this custom field as part of the search results for this section.',
			    'parent'        => 'waiting_list_search_options_defaults',
			    'choices'       => $fieldOptions,
		    ));
	    }
        */
    }

    public function registerShortCodes($sectionName, $sectionID) {
        if( function_exists('acf_add_local_field_group') ):

            $this->registerShortCodes_defaults($sectionName, $sectionID);

            acf_add_local_field_group(array(
                'key' => 'group_' . 'waiting_list_search_shortcode_' . $sectionID,
                'title' => 'Available [Short-Codes] for ' . $sectionName,
                'fields' => array(
                    array(
                        'key' => 'field_available_shortcodes_for_' . $sectionID,
                        'label' => 'Available shortcodes for this section:',
                        'name' => 'field_available_shortcodes_for_' . $sectionID,
                        'type' => 'message',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '',
                        'new_lines' => 'wpautop',
                        'esc_html' => 0,
                    )
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => 'neoweb-search-settings',
                        ),
                    ),
                ),
                'menu_order' => 50,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'field',
                'hide_on_screen' => '',
                'active' => 1,
                'description' => '',
            ));

            acf_add_local_field( array(
                'key'           => 'waiting_list_search_form_' . $sectionID,
                "label"         => "Search form",
                'name'          => 'waiting_list_search_form_' . $sectionID,
                'type'          => 'text',
                'wrapper' => array(
                    'class' => 'shortCodeCopy',
                ),
                'readonly'      => 1,
                'default_value' => '[OSM-WaitingList sectionid="' . $sectionID . '"]',
                'parent'        => 'group_' . 'waiting_list_search_shortcode_' . $sectionID,
            ) );

            acf_add_local_field( array(
                'key'           => 'waiting_list_count_form_' . $sectionID,
                "label"         => "Waiting List Count",
                'name'          => 'waiting_list_count_form_' . $sectionID,
                'type'          => 'text',
                'wrapper' => array(
                    'class' => 'shortCodeCopy',
                ),
                'readonly'      => 1,
                'default_value' => '[OSM-WaitingList-Count sectionid="' . $sectionID . '"]',
                'parent'        => 'group_' . 'waiting_list_search_shortcode_' . $sectionID
            ) );

            //TODO Add functionality to override search settings per waiting list
	        acf_add_local_field(array(
		        'key' => 'field_' . $sectionID . '_override_search',
		        'label' => 'Override default settings?',
		        'name' => $sectionID . '_override_search',
		        'type' => 'true_false',
		        'message' => 'Activate overrides for this waiting list?',
		        'instructions' => 'To activate overrides for this waiting list, select this field and then safe the page by clicking the "Update" button. 
		            A new top level menu will be added for this waiting list',
		        'parent' => 'group_' . 'waiting_list_search_shortcode_' . $sectionID
	        ));

        endif;
    }

    public function registerShortCodes_defaults($sectionName, $sectionID) {
        if( function_exists('acf_add_local_field_group') ):
	        foreach ( $this->menuTitles as $menu_key => $menu_title ) {
		        acf_add_local_field_group( array(
			        'key'                   => 'group_' . 'waiting_list_search_shortcode_' . $sectionID,
			        'title'                 => 'Available [Short-Codes] for ' . $sectionName,
			        'fields'                => array(
				        array(
					        'key'               => 'field_available_shortcodes_for_' . $sectionID,
					        'label'             => 'Available shortcodes for this section:',
					        'name'              => 'field_available_shortcodes_for_' . $sectionID,
					        'type'              => 'message',
					        'instructions'      => '',
					        'required'          => 0,
					        'conditional_logic' => 0,
					        'wrapper'           => array(
						        'width' => '',
						        'class' => '',
						        'id'    => '',
					        ),
					        'message'           => '',
					        'new_lines'         => 'wpautop',
					        'esc_html'          => 0,
				        )
			        ),
			        'location'              => array(
				        array(
					        array(
						        'param'    => 'options_page',
						        'operator' => '==',
						        'value'    => 'neoweb-' . $menu_key,
					        ),
				        ),
			        ),
			        'menu_order'            => 50,
			        'position'              => 'normal',
			        'style'                 => 'default',
			        'label_placement'       => 'top',
			        'instruction_placement' => 'field',
			        'hide_on_screen'        => '',
			        'active'                => 1,
			        'description'           => '',
		        ) );

		        acf_add_local_field( array(
			        'key'           => 'field_waiting_list_search_form_' . $sectionID,
			        "label"         => "Search form",
			        'name'          => 'field_waiting_list_search_form_' . $sectionID,
			        'type'          => 'text',
			        'wrapper'       => array(
				        'class' => 'shortCodeCopy',
			        ),
			        'readonly'      => 1,
			        'default_value' => '[OSM_WaitingList sectionid="' . $sectionID . '"]',
			        'parent'        => 'group_' . 'waiting_list_search_shortcode_' . $sectionID
		        ) );

		        acf_add_local_field( array(
			        'key'           => 'field_waiting_list_count_form_' . $sectionID,
			        "label"         => "Waiting List Count",
			        'name'          => 'field_waiting_list_count_form_' . $sectionID,
			        'type'          => 'text',
			        'wrapper'       => array(
				        'class' => 'shortCodeCopy',
			        ),
			        'readonly'      => 1,
			        'default_value' => '[OSM_WaitingList_Count sectionid="' . $sectionID . '"]',
			        'parent'        => 'group_' . 'waiting_list_search_shortcode_' . $sectionID
		        ) );

		        acf_add_local_field(array(
			        'key' => 'field_' . $sectionID . '_override_search',
			        'label' => 'Override default settings?',
			        'name' => $sectionID . '_override_search',
			        'type' => 'true_false',
			        'message' => 'Activate overrides for this waiting list?',
			        'instructions' => 'To activate overrides for this waiting list, select this field and then safe the page by clicking the "Update" button. 
		            A new top level menu will be added for this waiting list',
			        'parent' => 'group_' . 'waiting_list_search_shortcode_' . $sectionID
		        ));

	        }
        endif;
    }
}