<?php


class NeoWeb_Connector_Form_Builder
{

    private array $elements;
    private int $form_number = 1;


    public function __construct($elements){
        $this->elements = $elements;
    }

    /**
     * @return array
     */
    public function getElements(): array
    {
        return $this->elements;
    }

    /**
     * @return int
     */
    public function getFormNumber(): int
    {
        return $this->form_number;
    }

    /**
     * Form class method to dump object elements
     *
     * The method just dumps the elements of the form passed to the instantiation.
     *
     * @return void
     *
     */
    public function dumpElements() {
        var_dump($this->elements);
    }

    /**
     * Form class method to build a form from an array
     *
     *
     * @return string $output contains the form as HTML
     *
     */
    public function build(): string
    {
        $output = '';

        // For multiple forms, create a counter.
        $this->form_number++;

        // Loop through each form element and render it.
        foreach ($this->elements as $name => $elements) {
            $label = '<label for="' . $name . '">' . $elements['title'] . '</label>';
            switch ($elements['type']) {
                case 'heading':
                    $heading = '<h2>' . $elements['title'] . '</h2>';
                    $label = "";
                    $input = "";
                    break;
                case 'textarea':
                    $heading = "";
                    $input = '<textarea name="' . $name . '" id="' . $name . '" class="'.$elements['class'].'" '. ($elements['required'] ? ' required ' : '') .'></textarea>';
                    break;
                case 'submit':
                    $heading = "";
                    $label = '';
                    $input = '<input type="submit" name="' . $name . '" id="' . $name . '" value="' . $elements['title'] . '" class="'.$elements['class'].'" />';
                    break;
                case 'select':
                    $heading = "";
                    $input = '<select type="' . $elements['type'] . '" name="' . $name . '" id="' . $name . '"  class="'.$elements['class'].'" '. ($elements['required'] ? ' required ' : '') .'>';
                        foreach ($elements['options'] as $optionValue => $optionLabel) {
                            $input .= '<option value="'.$optionValue.'">'.$optionLabel.'</option>';
                        }
                    $input .= '</select>';
                    break;
                default:
                    $heading = "";
                    $input = '<input 
                        type="' . $elements['type'] . '" 
                        name="' . $name . '" 
                        id="' . $name . '"  
                        placeholder="' . $elements['placeholder'] . '"  
                        class="'.$elements['class'].'" '.
                        ($elements['required'] ? ' required ' : '') .'/>';
                    break;
            }
            $output .= $heading . '<div>' . $label . '<p>' . $input . '</p></div>';
        }

        // Wrap a form around the inputs.
        $output = '
          <form action="' . $_SERVER['PHP_SELF'] . '" method="post">
            <input type="hidden" name="action" value="submit_' . $this->form_number . '" />
            ' . $output . '
          </form>';

        // Return the form.
        return $output;
    }

}