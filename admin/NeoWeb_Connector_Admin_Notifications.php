<?php


class NeoWeb_Connector_Admin_Notifications {

	public function add_flash_notice( $notice = "", $type = "warning", $dismissible = true ) {
		// Here we return the notices saved on our option, if there are not notices, then an empty array is returned
		$notices = get_option("neoweb_connector_flash_notices", array() );
		if (!is_array($notices)) {
			$notices = array();
		}

		$dismissible_text = ( $dismissible ) ? "is-dismissible" : "";

		// We add our new notice.
		array_push( $notices, array(
			"notice" => $notice,
			"type" => $type,
			"dismissible" => $dismissible_text
		) );

		// Then we update the option with our notices array
		update_option("neoweb_connector_flash_notices", $notices );
	}

	/**
	 * Function executed when the 'admin_notices' action is called, here we check if there are notices on
	 * our database and display them, after that, we remove the option to prevent notices being displayed forever.
	 * @return void
	 */

	public function display_flash_notices() {
		$notices = get_option("neoweb_connector_flash_notices", array() );

		if( ! empty( $notices ) ) {
			// Iterate through our notices to be displayed and print them.
			foreach ( $notices as $notice ) {
				printf('<div class="notice notice-%1$s %2$s"><p>%3$s</p></div>',
					$notice['type'],
					$notice['dismissible'],
					$notice['notice']
				);
			}

			// Now we reset our options to prevent notices being displayed forever.
			delete_option( "neoweb_connector_flash_notices" );
		}
	}

}