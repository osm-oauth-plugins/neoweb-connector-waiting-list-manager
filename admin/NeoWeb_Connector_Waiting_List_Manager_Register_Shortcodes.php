<?php


class NeoWeb_Connector_Waiting_List_Manager_Register_Shortcodes
{

    private NeoWeb_Connector_Waiting_List_Manager_Auth_Caller $oAuthCaller;
    private NeoWeb_Connector_Loggers $logger;

	/**
	 * __contructor
	 *
	 * @since    1.0.0
	 */
    public function __construct() {
        $this->oAuthCaller = new NeoWeb_Connector_Waiting_List_Manager_Auth_Caller();
    }

    public function test($attr) {

	    $sectionID = $attr['sectionid'];

        $html = "";
        ob_start();
        include( OSM_WAITING_LIST_PLUGIN_PATH . 'public/html-templates/waiting_list_register_fields.php' );
        $html .= ob_get_clean();

        return $html;
    }

    public function create_waiting_list_search_form($attr) {
        $sectionID = $attr['sectionid'];

	    $showCount = get_field('show_list_count_' . $sectionID, 'option');
	    if ($showCount == 1) {
		    $countMsg = get_field('message_to_show_before_waiting_list_count_' . $sectionID, 'option');

		    $sectionType = "waiting"; //TODO: if NZ support add a check for "nzwaiting"
		    $currentTermID = -1;

		    //getAllMembers
		    $url = NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints::getAllMembers;
		    $formattedURL = (new NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $currentTermID, $sectionType);

		    $transientID = 'waitingList_data_' . $sectionID;
		    $allMemberData = json_decode($this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 12, array()), true);

		    if (is_array($allMemberData)) {
			    $counter = count($allMemberData['items']);
		    } else {
			    $counter = 0;
		    }
	    }

	    $resultMsgDefault = get_field('message_to_show_before_waiting_list_search_defaults', 'option');
	    $showDOBDefault = get_field('include_dob_in_search_results_defaults', 'option');
	    $showJoiningDefault = get_field('include_joining_date_in_search_results_defaults', 'option');

	    //TODO: Overrides
	    $resultMsg = get_field('message_to_show_before_waiting_list_search_' . $sectionID, 'option');
	    $showDOB = get_field('include_dob_in_search_results_' . $sectionID, 'option');
	    $showJoining = get_field('include_joining_date_in_search_results_' . $sectionID, 'option');


	    if (isset($resultMsg)) {
		    $resultMsg = $resultMsg;
	    } else {
		    $resultMsg = $resultMsgDefault;
	    }
	    if (isset($showDOB)) {
		    $showDOB = $showDOB;
	    } else {
		    $showDOB = $showDOBDefault;
	    }
	    if (isset($showJoining)) {
		    $showJoining = $showJoining;
	    } else {
		    $showJoining = $showJoiningDefault;
	    }

	    $showCustomDataDefault = get_field('neoweb_connector_show_custom_field_' . $sectionID, 'option');
	    $showCustomData = get_field('include_custom_data_in_search_results_override_' . $sectionID, 'option');
	    if (isset($showCustomData)) {
		    $showCustomData = $showCustomData;
	    } else {
		    $showCustomData = $showCustomDataDefault;
	    }

	    $html = "";
	    ob_start();
	    include( OSM_WAITING_LIST_PLUGIN_PATH . 'public/html-templates/waitinglist-search-form.php' );
	    $html .= ob_get_clean();

	    return $html;
	    
    }

    public function create_waiting_list_count($attr) {
        $sectionID = $attr['sectionid'];
        $defaultMsg = get_field('message_to_show_before_waiting_list_count_defaults', 'option');
	    $overrideMsg = get_field('message_to_show_before_waiting_list_count_' . $sectionID, 'option');
	    if (isset($overrideMsg)) {
		    $countMsgPre = $overrideMsg;
	    } else {
	    	$countMsgPre = $defaultMsg;
	    }

	    $sectionType = "waiting"; //TODO: if NZ support add a check for "nzwaiting"
	    $currentTermID = -1;

	    //getAllMembers
	    $url = NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints::getAllMembers;
	    $formattedURL = (new NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $currentTermID, $sectionType);

	    $transientID = 'waitingList_data_' . $sectionID;
	    $allMemberData = json_decode($this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 12, array()), true);

	    if (is_array($allMemberData)) {
		    $counter = count($allMemberData['items']);
	    } else {
		    $counter = 0;
	    }
	    $html = "";
	    ob_start();
	    include( OSM_WAITING_LIST_PLUGIN_PATH . 'public/html-templates/waitinglist-count.php' );
	    $html .= ob_get_clean();

	    return $html;
        
    }

    public function create_waiting_list_submit_form($attr) {
        $sectionID = $attr['sectionid'];

	    if (isset($attr['sectionname'])) {
		    $sectionName = $attr['sectionname'];
	    } else {
		    $sectionName = "";
	    }

	    $this->submit_waiting_list_form ($sectionName, $sectionID);
	    $html = "";
	    ob_start();
	    include(OSM_WAITING_LIST_PLUGIN_PATH . 'public/html-templates/waiting-list-register.php');
	    $html .= ob_get_clean();

	    return $html;
        
    }
    
    private function submit_waiting_list_form ($sectionName, $sectionID) {

	    if ( isset( $_POST['submitWaitingList'] ) ) {

		    $errorMsg = "";

		    //Captcha Details
		    $requireOldCaptcha = get_field('revert_to_20_'. $sectionID, 'option');

		    $responseKeys = [];

		    //child fields
		    $member_FirstName = sanitize_text_field($_POST["member_FirstName"]);
		    $member_LastName = sanitize_text_field($_POST["member_LastName"]);
		    $member_DateofBirth = sanitize_text_field($_POST["member_DateofBirth"]);
		    $member_Gender = (isset($_POST['member_Gender']) ? sanitize_text_field($_POST["member_Gender"]) : "");
		    $joinDate = sanitize_text_field($_POST["join_date"]);

		    //Member Contact fields
		    $member_AddressLine1 = ( isset($_POST['member_Address1']) ? sanitize_text_field($_POST["member_Address1"]) : "");
		    $member_AddressLine2 = ( isset($_POST['member_Address2']) ? sanitize_text_field($_POST["member_Address2"]) : "");
		    $member_Postcode = ( isset($_POST['member_PostCode']) ? sanitize_text_field($_POST["member_PostCode"]) : "");
		    $member_MobilePhoneNumber = ( isset($_POST['member_MobileNumber']) ? sanitize_text_field($_POST["member_MobileNumber"]) : "");
		    $member_PhoneNumber = ( isset($_POST['member_HomeNumber']) ? sanitize_text_field($_POST["member_HomeNumber"]) : "");
		    $member_EmailAddress1 = ( isset($_POST['member_EmailAddress']) ? sanitize_email($_POST["member_EmailAddress"]) : "");
		    $member_EmailAddress2 = ( isset($_POST['member_EmailAddress2']) ? sanitize_email($_POST["member_EmailAddress2"]) : "");

		    //Primary Contact fields
		    $primary_FirstName = ( isset($_POST['primary_FirstName']) ? sanitize_text_field($_POST["primary_FirstName"]) : "");
		    $primary_LastName = ( isset($_POST['primary_LastName']) ? sanitize_text_field($_POST["primary_LastName"]) : "");
		    $primary_AddressLine1 = ( isset($_POST['primary_AddressLine1']) ? sanitize_text_field($_POST["primary_AddressLine1"]) : "");
		    $primary_AddressLine2 = ( isset($_POST['primary_AddressLine2']) ? sanitize_text_field($_POST["primary_AddressLine2"]) : "");
		    $primary_Postcode = ( isset($_POST['primary_Postcode']) ? sanitize_text_field($_POST["primary_Postcode"]) : "");
		    $primary_MobilePhoneNumber = ( isset($_POST['primary_MobilePhoneNumber']) ? sanitize_text_field($_POST["primary_MobilePhoneNumber"]) : "");
		    $primary_PhoneNumber = ( isset($_POST['primary_PhoneNumber']) ? sanitize_text_field($_POST["primary_PhoneNumber"]) : "");
		    $primary_EmailAddress1 = ( isset($_POST['primary_EmailAddress1']) ? sanitize_email($_POST["primary_EmailAddress1"]) : "");
		    $primary_EmailAddress2 = ( isset($_POST['primary_EmailAddress2']) ? sanitize_email($_POST["primary_EmailAddress2"]) : "");

		    //Secondary Contact fields
		    $secondary_FirstName = ( isset($_POST['secondary_FirstName']) ? sanitize_text_field($_POST["secondary_FirstName"]) : "");
		    $secondary_LastName = ( isset($_POST['secondary_LastName']) ? sanitize_text_field($_POST["secondary_LastName"]) : "");
		    $secondary_AddressLine1 = ( isset($_POST['secondary_AddressLine1']) ? sanitize_text_field($_POST["secondary_AddressLine1"]) : "");
		    $secondary_AddressLine2 = ( isset($_POST['secondary_AddressLine2']) ? sanitize_text_field($_POST["secondary_AddressLine2"]) : "");
		    $secondary_Postcode = ( isset($_POST['secondary_Postcode']) ? sanitize_text_field($_POST["secondary_Postcode"]) : "");
		    $secondary_MobilePhoneNumber = ( isset($_POST['secondary_MobilePhoneNumber']) ? sanitize_text_field($_POST["secondary_MobilePhoneNumber"]) : "");
		    $secondary_PhoneNumber = ( isset($_POST['secondary_PhoneNumber']) ? sanitize_text_field($_POST["secondary_PhoneNumber"]) : "");
		    $secondary_EmailAddress1 = ( isset($_POST['secondary_EmailAddress1']) ? sanitize_email($_POST["secondary_EmailAddress1"]) : "");
		    $secondary_EmailAddress2 = ( isset($_POST['secondary_EmailAddress2']) ? sanitize_email($_POST["secondary_EmailAddress2"]) : "");

		    //Emergency Contact fields
		    $emergency_FirstName = ( isset($_POST['emergency_FirstName']) ? sanitize_text_field($_POST["emergency_FirstName"]) : "");
		    $emergency_LastName = ( isset($_POST['emergency_LastName']) ? sanitize_text_field($_POST["emergency_LastName"]) : "");
		    $emergency_AddressLine1 = ( isset($_POST['emergency_AddressLine1']) ? sanitize_text_field($_POST["emergency_AddressLine1"]) : "");
		    $emergency_AddressLine2 = ( isset($_POST['emergency_AddressLine2']) ? sanitize_text_field($_POST["emergency_AddressLine2"]) : "");
		    $emergency_Postcode = ( isset($_POST['emergency_Postcode']) ? sanitize_text_field($_POST["emergency_Postcode"]) : "");
		    $emergency_MobilePhoneNumber = ( isset($_POST['emergency_MobilePhoneNumber']) ? sanitize_text_field($_POST["emergency_MobilePhoneNumber"]) : "");
		    $emergency_PhoneNumber = ( isset($_POST['emergency_PhoneNumber']) ? sanitize_text_field($_POST["emergency_PhoneNumber"]) : "");
		    $emergency_EmailAddress1 = ( isset($_POST['emergency_EmailAddress1']) ? sanitize_email($_POST["emergency_EmailAddress1"]) : "");
		    $emergency_EmailAddress2 = ( isset($_POST['emergency_EmailAddress2']) ? sanitize_email($_POST["emergency_EmailAddress2"]) : "");

		    //Emergency Contact fields
		    $doctor_FirstName = ( isset($_POST['doctor_FirstName']) ? sanitize_text_field($_POST["doctor_FirstName"]) : "");
		    $doctor_LastName = ( isset($_POST['doctor_LastName']) ? sanitize_text_field($_POST["doctor_LastName"]) : "");
		    $doctor_Surgery = ( isset($_POST['doctor_Surgery']) ? sanitize_text_field($_POST["doctor_Surgery"]) : "");
		    $doctor_AddressLine1 = ( isset($_POST['doctor_AddressLine1']) ? sanitize_text_field($_POST["doctor_AddressLine1"]) : "");
		    $doctor_AddressLine2 = ( isset($_POST['doctor_AddressLine2']) ? sanitize_text_field($_POST["doctor_AddressLine2"]) : "");
		    $doctor_Postcode = ( isset($_POST['doctor_Postcode']) ? sanitize_text_field($_POST["doctor_Postcode"]) : "");
		    $doctor_PhoneNumber1 = ( isset($_POST['doctor_PhoneNumber1']) ? sanitize_text_field($_POST["doctor_PhoneNumber1"]) : "");
		    $doctor_PhoneNumber2 = ( isset($_POST['doctor_PhoneNumber2']) ? sanitize_text_field($_POST["doctor_PhoneNumber2"]) : "");

		    $formData = compact("member_FirstName", "member_LastName", "member_DateofBirth", "member_Gender", "joinDate", "sectionID", "member_AddressLine1", "member_AddressLine2", "member_Postcode",
			    "member_MobilePhoneNumber", "member_PhoneNumber", "member_EmailAddress1", "member_EmailAddress2", "primary_FirstName", "primary_LastName", "primary_AddressLine1",
			    "primary_AddressLine2", "primary_Postcode", "primary_MobilePhoneNumber", "primary_PhoneNumber", "primary_EmailAddress1", "primary_EmailAddress2", "secondary_FirstName",
			    "secondary_LastName", "secondary_AddressLine1", "secondary_AddressLine2", "secondary_Postcode", "secondary_MobilePhoneNumber", "secondary_PhoneNumber", "secondary_EmailAddress1",
			    "secondary_EmailAddress2", "emergency_FirstName", "emergency_LastName", "emergency_AddressLine1", "emergency_AddressLine2", "emergency_Postcode", "emergency_MobilePhoneNumber",
			    "emergency_PhoneNumber", "emergency_EmailAddress1", "emergency_EmailAddress2", "doctor_FirstName", "doctor_LastName", "doctor_Surgery", "doctor_AddressLine1", "doctor_AddressLine2",
			    "doctor_Postcode", "doctor_PhoneNumber1", "doctor_PhoneNumber2" );

		    //Additional Details
		    $customFields = get_field('osmCustomFieldTabGroup_' . $sectionID, 'option');
		    $customFieldsValues = array();
		    $customFieldsValuesNice = array();
		    if (isset($customFields)) {
			    if (is_array($customFields) || is_object($customFields)) {
				    $fieldCounter = 1;
				    foreach ($customFields as $field) {

					    if ($field['osm_field_type'] == 'text' || $field['osm_field_type'] == 'number') {
						    $customFieldsValues[$_POST["FieldID_" . $fieldCounter]] = sanitize_text_field($_POST["Field_" . $fieldCounter]);
						    $customFieldsValuesNice[$field["osm_field_label"]] = sanitize_text_field($_POST["Field_" . $fieldCounter]);
					    } else if ($field['osm_field_type'] == 'select') {
						    $customFieldsValues[$_POST["FieldID_" . $fieldCounter]] = $_POST["Field_" . $fieldCounter];
						    $customFieldsValuesNice[$field["osm_field_label"]] = $_POST["Field_" . $fieldCounter];
					    } else {
						    if (isset($_POST["Field_" . $fieldCounter])) {

							    if (is_array($_POST["Field_" . $fieldCounter])) {
								    $optionsSelected = "";
								    foreach ($_POST["Field_" . $fieldCounter] as $selectedOption) {
									    $optionsSelected .= $selectedOption . "||";
								    }
								    $customFieldsValues[$_POST["FieldID_" . $fieldCounter]] = substr($optionsSelected, 0, -2);
								    $customFieldsValuesNice[$field["osm_field_label"]] = substr($optionsSelected, 0, -2);
							    } else {
								    $customFieldsValues[$_POST["FieldID_" . $fieldCounter]] = $_POST["Field_" . $fieldCounter];
								    $customFieldsValuesNice[$field["osm_field_label"]] = $_POST["Field_" . $fieldCounter];
							    }

						    }
					    }
					    $fieldCounter++;
				    }
			    }
		    }

		    if (isset($_POST["tandc"])) {
			    //Terms and Conditions
			    $tandc = $_POST["tandc"];
		    } else {
			    $tandc = 0;
		    }

		    //Option Data
		    $autoSubmit = get_field('auto_submit_' . $sectionID, 'option');

		    $adminSubject = get_field('submission_notification_subject_line_' . $sectionID, 'option');
		    $adminSubject = str_replace("{{sectionname}}", $sectionName, $adminSubject);
		    $adminSubject = str_replace("{{memberName}}", $member_FirstName, $adminSubject);
		    $adminSubject = str_replace("{{childName}}", $member_FirstName, $adminSubject);
		    $adminSubject = str_replace("{{memberLastName}}", $member_LastName, $adminSubject);
		    $adminSubject = str_replace("{{childLastName}}", $member_LastName, $adminSubject);

		    if ($adminSubject == "") {
			    $adminSubject = "Waiting List Submission";
		    }

		    $DefaultToAddress = get_field('submission_notification_email_address_defaults', 'option');
		    $OverrideEmailAddress = get_field('submission_notification_email_address_' . $sectionID, 'option');

		    if ($OverrideEmailAddress != "") {
			    $adminToAddress = $OverrideEmailAddress;
		    } else {
			    $adminToAddress = $DefaultToAddress;
		    }

		    if ($requireOldCaptcha == 0) {
			    $googleSecretKey = get_field("google_site_key_" . $sectionID, 'option');
			    if(isset($_POST['g-recaptcha-response'])){
				    $captcha=$_POST['g-recaptcha-response'];
			    }

			    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$googleSecretKey."&response=".$captcha);
			    $responseKeys = json_decode($response,true);

			    if(!$responseKeys["success"]) {

				    $errorMsg = 'You did not pass the google captcha check and your form will not be submitted to the server.';

				    $html = "";
				    ob_start();
				    include(OSM_WAITING_LIST_PLUGIN_PATH . 'public/html-templates/waiting-list-register.php');
				    $html .= ob_get_clean();

				    echo $html;

				    exit;

			    }

		    } else if ($requireOldCaptcha == 1) {

			    $secretKey = get_field('google_secret_key_' . $sectionID, 'option');
			    $captcha = null;
			    if(isset($_POST['g-recaptcha-response'])){
				    $captcha=$_POST['g-recaptcha-response'];
			    }

			    if(!$captcha){

				    $errorMsg = 'Please check the the captcha form.';

				    $html = "";
				    ob_start();
				    include(OSM_WAITING_LIST_PLUGIN_PATH . 'public/html-templates/waiting-list-register.php');
				    $html .= ob_get_clean();

				    echo $html;

				    exit;

			    }

			    $ip = $_SERVER['REMOTE_ADDR'];
			    // post request to server
			    $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
			    $response = file_get_contents($url);
			    $responseKeys = json_decode($response,true);
			    // should return JSON with success as true
			    if(!$responseKeys["success"]) {

				    $errorMsg = 'You did not pass the google captcha check and your form will not be submitted to the server.';

				    $html = "";
				    ob_start();
				    include(OSM_WAITING_LIST_PLUGIN_PATH . 'public/html-templates/waiting-list-register.php');
				    $html .= ob_get_clean();

				    echo $html;

				    exit;

			    }

		    }

		    $emailParent = get_field('notify_parent_by_email_of_successful_submission_' . $sectionID, 'option');
		    if ($emailParent == 1) {
			    $message = get_field('parent_email_notification_message_' . $sectionID, 'option');
			    $parentMessage = $this->compileParentEmailMessage($message);

			    $parentMessage = str_replace("{{sectionname}}", $sectionName, $parentMessage);
			    $parentMessage = str_replace("{{memberName}}", $member_FirstName, $parentMessage);
			    $parentMessage = str_replace("{{childName}}", $member_FirstName, $parentMessage);
			    $parentMessage = str_replace("{{memberLastName}}", $member_LastName, $parentMessage);
			    $parentMessage = str_replace("{{childLastName}}", $member_LastName, $parentMessage);

			    $parentSubject = get_field('parent_email_notification_subject_line_' . $sectionID, 'option');
			    $parentSubject = str_replace("{{sectionname}}", $sectionName, $parentSubject);
			    $parentSubject = str_replace("{{memberName}}", $member_FirstName, $parentSubject);
			    $parentSubject = str_replace("{{childName}}", $member_FirstName, $parentSubject);
			    $parentSubject = str_replace("{{memberLastName}}", $member_LastName, $parentSubject);
			    $parentSubject = str_replace("{{childLastName}}", $member_LastName, $parentSubject);

			    if ($parentSubject == "") {
				    $parentSubject = "Waiting List Submission";
			    }

		    } else {
			    $parentSubject = "";
			    $parentMessage = "";
		    }


		    //Our YYYY-MM-DD date.
		    //Convert it into a timestamp.
		    $timestamp = strtotime($joinDate);

		    //Convert it to DD-MM-YYYY
		    $dmyJoinDate = date("d/m/Y", $timestamp);

		    //TODO: calculate member age
		    $adminMessage = $this->compileEmailMessage($formData, $customFieldsValuesNice, $tandc, $autoSubmit);

		    if ($autoSubmit == 1) {

			    $this->submit_waiting_list_form_to_osm($formData, $customFieldsValues,
				    $adminToAddress, $adminSubject, $adminMessage, $parentSubject, $parentMessage, $sectionID);

		    } else {

			    $onScreenMessage = get_field('screen_notification_message_' . $sectionID, 'option');
			    if ($onScreenMessage == "") {
				    $onScreenMessage = "Your submission has been successful. A member of the team will contact you as soon as a space becomes available.";
			    }

			    ob_start();
			    $html = '<div class="alert alert-success submitAlert" role="alert" style="display: none;">'
			            . $onScreenMessage .
			            '</div>';
			    $html .= ob_get_clean();

			    echo $html;

			    //Submit to email only
			    $this->send_emails ($adminToAddress, $adminSubject, $adminMessage, $formData, $parentSubject, $parentMessage, $sectionID);
		    }

	    }

    }


	private function compileEmailMessage($formData, $customFieldsValues, $tandc, $autoSubmit): string {
		ob_start();
		include(OSM_WAITING_LIST_PLUGIN_PATH . 'public/html-templates/email-template.php');
		return ob_get_clean();
	}

	private function compileParentEmailMessage ($message): string {
		ob_start();
		include(OSM_WAITING_LIST_PLUGIN_PATH . 'public/html-templates/parent-email-template.php');
		return ob_get_clean();
	}


	private function submit_waiting_list_form_to_osm ($formData, $customFieldsValues, $adminToAddress, $adminSubject, $adminMessage, $parentSubject, $parentMessage, $sectionID) {

		$yp_record = $this->submit_child_data_to_osm($formData['sectionid'], $formData['member_FirstName'], $formData['member_LastName'], $formData['member_DateofBirth'], $formData['joinDate']);

		if ($yp_record['result'] == 'ok') {
			if ($yp_record['scoutid'] > 0) {

				$scoutID = $yp_record['scoutid'];

				$CaptureMember = get_field('member_Gender_' . $sectionID . '_visible', 'option');
				if ($CaptureMember == 1) {
					$completeRecord = $this->submit_member_Gender_data_to_osm($scoutID, $formData, $sectionID);
					$completeRecord = $this->submit_member_data_to_osm($scoutID, $formData, $sectionID);
				}

				//$sectionID . '_capture_' . $section
				$CapturePrimary1 = get_field('primary_CaptureSection_' . $sectionID, 'option');
				if ($CapturePrimary1 == 1) {
					$completeRecord = $this->submit_parent1_data_to_osm ($scoutID, $formData, $sectionID);
				}

				$CapturePrimary2 = get_field('secondary_CaptureSection_' . $sectionID, 'option');
				if ($CapturePrimary2 == 1) {
					$completeRecord = $this->submit_parent2_data_to_osm ($scoutID, $formData, $sectionID);
				}

				$CaptureEmergency = get_field('emergency_CaptureSection_' . $sectionID, 'option');
				if ($CaptureEmergency == 1) {
					$completeRecord = $this->submit_emergency_data_to_osm($scoutID, $formData, $sectionID);
				}

				$CaptureDr = get_field('doctor_CaptureSection_' . $sectionID, 'option');
				if ($CaptureDr == 1) {
					$completeRecord = $this->submit_doctor_data_to_osm($scoutID, $formData, $sectionID);
				}

				if(! empty($customFieldsValues) ) {
					$completeRecord = $this->submit_custom_data_to_osm($scoutID, $formData, $customFieldsValues, $sectionID);
				}

				//'parent_on_screen_notification_message_' . $sectionID
				$onScreenMessage = get_field('parent_on_screen_notification_message_' . $sectionID, 'option');
				if ($onScreenMessage == "") {
					$onScreenMessage = "Your submission has been successful. A member of the team will contact you as soon as a space becomes available.";
				}

				//Email record and confirmation
				$this->send_emails ($adminToAddress, $adminSubject, $adminMessage, $formData, $parentSubject, $parentMessage, $sectionID);

				ob_start();
				$html = '<div class="alert alert-success submitAlert" role="alert" style="display: none;">'
				        . $onScreenMessage .
				        '</div>';
				$html .= ob_get_clean();

				echo $html;

			} else if ($yp_record['scoutid'] == -1) {
				ob_start();
				$html = '<div class="alert alert-danger submitAlert" role="alert" style="display: none;">
  						It appears there is already a member on the waiting list with those details.
					</div>';
				$html .= ob_get_clean();

				echo $html;

			}
		}

	}

	private function send_emails ($adminToAddress, $adminSubject, $adminMessage, $formData, $parentSubject, $parentMessage, $sectionID) {
		$headers = "Content-Type: text/html; charset=UTF-8" .PHP_EOL;
		if (wp_mail($adminToAddress, $adminSubject, $adminMessage, $headers)) {

			//notify_parent_by_email_of_successful_submission_defaults
			$emailParent = get_field('notify_parent_by_email_of_successful_submission_' . $sectionID, 'option');
			if ($emailParent == 1) {
				//'email_address_to_use_' . $sectionID
				$selected_email_address = get_field('email_address_to_use_' . $sectionID, 'option');
				switch ($selected_email_address) {
					case "member" :
						$parentEmail = $formData['member_EmailAddress1'];
						break;
					case "primary" :
						$parentEmail = $formData['primary_EmailAddress1'];
						break;
					case "secondary" :
						$parentEmail = $formData['secondary_EmailAddress1'];
						break;
					case "emergency" :
						$parentEmail = $formData['emergency_EmailAddress1'];
						break;
					default:
						$parentEmail = $formData[''];
				}
				if (wp_mail($parentEmail, $parentSubject, $parentMessage, $headers)) {

				}
			}
		} else {

			ob_start();
			$html = '<div class="alert alert-danger submitAlert" role="alert" style="display: none;">
  						There appears to be a problem sending email from this site. Please try again later
					</div>';
			$html .= ob_get_clean();

			echo $html;
		}
	}

	private function submit_child_data_to_osm ($sectionID, $member_FirstName, $member_LastName, $member_DateofBirth, $joinDate) {

		$parts = array();

		$parts['sectionid'] = (int)$sectionID;
		$parts['firstname'] = $member_FirstName;
		$parts['lastname'] = $member_LastName;

		$parts['dob'] = date('d/m/Y', strtotime($member_DateofBirth));

		$parts['started'] = $joinDate;
		$parts['duplicating'] = "";

		//create_new_member
		$url = NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints::create_new_member;
		$formattedURL = (new NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID);

		return json_decode($this->oAuthCaller->osmAPICaller(false, $formattedURL, 12, $parts), true);

	}

	function submit_member_Gender_data_to_osm ($scoutID, $formData, $sectionID) {
		$groupID = 7;

		$parts = array();
		$parts['associated_id'] = $scoutID;
		$parts['service'] = "desktop";
		$parts['associated_type'] = "member";
		$parts['value'] = $formData['member_Gender'];
		$parts['column_id'] = "34";
		$parts['context'] = "members";
		$parts['group_id'] = $groupID;

		//update_custom_additional_data
		$url = NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints::update_custom_additional_data;
		$formattedURL = (new NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID);

		return json_decode($this->oAuthCaller->osmAPICaller(false, $formattedURL, 12, $parts), true);
	}

	private function submit_member_data_to_osm ($scoutID, $formData, $sectionID) {

		$groupID = 6; //Primary Contact

		$parts = array();
		$parts['associated_id'] = $scoutID;
		$parts['associated_type'] = "member";
		$parts['context'] = "members";
		$parts['data[address1]'] = $formData['member_AddressLine1'];
		$parts['data[address2]'] = $formData['member_AddressLine2'];
		$parts['data[address3]'] = "";
		$parts['data[address4]'] = "";

		//'whitespace_settings_' . $sectionID
		$whitespaceSettings = get_field('whitespace_settings_' . $sectionID, 'option');

		if ($whitespaceSettings['postcode_whitespace']  == 1) {
			$parts['data[postcode]'] = str_replace(' ', '', $formData['member_Postcode']);
		} else {
			$parts['data[postcode]'] = $formData['member_Postcode'];
		}

		$parts['data[email1]'] = $formData['member_EmailAddress1'];
		$parts['data[email1_leaders]'] = "yes";
		$parts['data[email2]'] = $formData['member_EmailAddress2'];
		$parts['data[email2_leaders]'] = "yes";
		$parts['data[phone1]'] = $formData['member_PhoneNumber'];
		$parts['data[phone1_sms]'] = "yes";
		$parts['data[phone2]'] = $formData['member_MobilePhoneNumber'];
		$parts['data[phone2_sms]'] = "yes";
		$parts['group_id'] = $groupID;
		$parts['service'] = "desktop";

		//update_member_data
		$url = NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints::update_member_data;
		$formattedURL = (new NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID);

		return json_decode($this->oAuthCaller->osmAPICaller(false, $formattedURL, 12, $parts), true);
	}

	private function submit_parent1_data_to_osm ($scoutID, $formData, $sectionID) {

		$groupID = 1; //Primary Contact

		$parts = array();
		$parts['associated_id'] = $scoutID;
		$parts['associated_type'] = "member";
		$parts['context'] = "members";
		$parts['data[address1]'] = $formData['primary_AddressLine1'];
		$parts['data[address2]'] = $formData['primary_AddressLine2'];
		$parts['data[address3]'] = "";
		$parts['data[address4]'] = "";

		//'whitespace_settings_' . $sectionID
		$whitespaceSettings = get_field('whitespace_settings_' . $sectionID, 'option');

		if ($whitespaceSettings['postcode_whitespace']  == 1) {
			$parts['data[postcode]'] = str_replace(' ', '', $formData['primary_Postcode']);
		} else {
			$parts['data[postcode]'] = $formData['primary_Postcode'];
		}

		$parts['data[email1]'] = $formData['primary_EmailAddress1'];
		$parts['data[email1_leaders]'] = "yes";
		$parts['data[email2]'] = $formData['primary_EmailAddress2'];
		$parts['data[email2_leaders]'] = "yes";
		$parts['data[phone1]'] = $formData['primary_PhoneNumber'];
		$parts['data[phone1_sms]'] = "yes";
		$parts['data[phone2]'] = $formData['primary_MobilePhoneNumber'];
		$parts['data[phone2_sms]'] = "yes";
		$parts['data[firstname]'] = $formData['primary_FirstName'];
		$parts['data[lastname]'] = $formData['primary_LastName'];
		$parts['group_id'] = $groupID;
		$parts['service'] = "desktop";

		//update_member_data
		$url = NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints::update_member_data;
		$formattedURL = (new NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID);

		return json_decode($this->oAuthCaller->osmAPICaller(false, $formattedURL, 12, $parts), true);

	}

	private function submit_parent2_data_to_osm ($scoutID, $formData, $sectionID) {

		$groupID = 2; //Secondary Contact

		$parts = array();
		$parts['associated_id'] = $scoutID;
		$parts['associated_type'] = "member";
		$parts['context'] = "members";
		$parts['data[address1]'] = $formData['secondary_AddressLine1'];
		$parts['data[address2]'] = $formData['secondary_AddressLine2'];
		$parts['data[address3]'] = "";
		$parts['data[address4]'] = "";

		//'whitespace_settings_' . $sectionID
		$whitespaceSettings = get_field('whitespace_settings_' . $sectionID, 'option');

		if ($whitespaceSettings['postcode_whitespace']  == 1) {
			$parts['data[postcode]'] = str_replace(' ', '', $formData['secondary_Postcode']);
		} else {
			$parts['data[postcode]'] = $formData['secondary_Postcode'];
		}

		$parts['data[email1]'] = $formData['secondary_EmailAddress1'];
		$parts['data[email1_leaders]'] = "yes";
		$parts['data[email2]'] = $formData['secondary_EmailAddress2'];
		$parts['data[email2_leaders]'] = "yes";
		$parts['data[phone1]'] = $formData['secondary_PhoneNumber'];
		$parts['data[phone1_sms]'] = "yes";
		$parts['data[phone2]'] = $formData['secondary_MobilePhoneNumber'];
		$parts['data[phone2_sms]'] = "yes";
		$parts['data[firstname]'] = $formData['secondary_FirstName'];
		$parts['data[lastname]'] = $formData['secondary_LastName'];
		$parts['group_id'] = $groupID;
		$parts['service'] = "desktop";

		//update_member_data
		$url = NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints::update_member_data;
		$formattedURL = (new NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID);

		return json_decode($this->oAuthCaller->osmAPICaller(false, $formattedURL, 12, $parts), true);

	}

	private function submit_emergency_data_to_osm ($scoutID, $formData, $sectionID) {

		$groupID = 3; //Emergency Contact

		$parts = array();
		$parts['associated_id'] = $scoutID;
		$parts['associated_type'] = "member";
		$parts['context'] = "members";
		$parts['data[address1]'] = $formData['emergency_AddressLine1'];
		$parts['data[address2]'] = $formData['emergency_AddressLine2'];
		$parts['data[address3]'] = "";
		$parts['data[address4]'] = "";

		//'whitespace_settings_' . $sectionID
		$whitespaceSettings = get_field('whitespace_settings_' . $sectionID, 'option');

		if ($whitespaceSettings['postcode_whitespace']  == 1) {
			$parts['data[postcode]'] = str_replace(' ', '', $formData['emergency_Postcode']);
		} else {
			$parts['data[postcode]'] = $formData['emergency_Postcode'];
		}

		$parts['data[email1]'] = $formData['emergency_EmailAddress1'];
		$parts['data[email2]'] = $formData['emergency_EmailAddress2'];
		$parts['data[phone1]'] = $formData['emergency_PhoneNumber'];
		$parts['data[phone2]'] = $formData['emergency_MobilePhoneNumber'];
		$parts['data[firstname]'] = $formData['emergency_FirstName'];
		$parts['data[lastname]'] = $formData['emergency_LastName'];
		$parts['group_id'] = $groupID;
		$parts['service'] = "desktop";

		//update_member_data
		$url = NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints::update_member_data;
		$formattedURL = (new NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID);

		return json_decode($this->oAuthCaller->osmAPICaller(false, $formattedURL, 12, $parts), true);

	}

	private function submit_doctor_data_to_osm ($scoutID, $formData, $sectionID) {

		$groupID = 4; //Doctors Contact

		$parts = array();
		$parts['associated_id'] = $scoutID;
		$parts['associated_type'] = "member";
		$parts['context'] = "members";
		$parts['data[address1]'] = $formData['doctor_AddressLine1'];
		$parts['data[address2]'] = $formData['doctor_AddressLine2'];
		$parts['data[address3]'] = "";
		$parts['data[address4]'] = "";

		//'whitespace_settings_' . $sectionID
		$whitespaceSettings = get_field('whitespace_settings_' . $sectionID, 'option');

		if ($whitespaceSettings['postcode_whitespace']  == 1) {
			$parts['data[postcode]'] = str_replace(' ', '', $formData['doctor_Postcode']);
		} else {
			$parts['data[postcode]'] = $formData['doctor_Postcode'];
		}

		$parts['data[phone1]'] = $formData['doctor_PhoneNumber2'];
		$parts['data[phone2]'] = $formData['doctor_PhoneNumber1'];
		$parts['data[firstname]'] = $formData['doctor_FirstName'];
		$parts['data[lastname]'] = $formData['doctor_LastName'];
		$parts['data[surgery]'] = $formData['doctor_Surgery'];
		$parts['group_id'] = $groupID;
		$parts['service'] = "desktop";

		//update_member_data
		$url = NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints::update_member_data;
		$formattedURL = (new NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID);

		return json_decode($this->oAuthCaller->osmAPICaller(false, $formattedURL, 12, $parts), true);

	}

	private function submit_custom_data_to_osm ($scoutID, $formData, $customFieldsValues, $sectionID) {

		foreach ($customFieldsValues as $key => $value) {
			$customFieldID = explode(":", $key);
			if (sizeof($customFieldID) > 0) {
				$additionalData = array();
				$additionalData['associated_id'] = $scoutID;
				$additionalData['service'] = "desktop";
				$additionalData['associated_type'] = "member";
				$additionalData['value'] = $value;
				$additionalData['column_id'] = $customFieldID[1];
				$additionalData['context'] = "members";
				$additionalData['group_id'] = $customFieldID[2];

				//update_custom_additional_data
				$url = NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints::update_custom_additional_data;
				$formattedURL = (new NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID);

				//TODO: need to collect all posts and check all of them is they were successful
				$data = json_decode($this->oAuthCaller->osmAPICaller(false, $formattedURL, 12, $additionalData), true);

			}
		}

		return $data;

	}
}