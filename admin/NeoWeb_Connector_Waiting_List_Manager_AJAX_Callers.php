<?php


class NeoWeb_Connector_Waiting_List_Manager_AJAX_Callers {

	private NeoWeb_Connector_Admin_Notifications $flashNotice;
	private NeoWeb_Connector_Licence_Manager $licenceManager;
	private NeoWeb_Connector_Loggers $logger;
	private NeoWeb_Connector_Waiting_List_Manager_Transient_Manager $transientManager;
	private NeoWeb_Connector_Waiting_List_Manager_Auth_Caller $oAuthCaller;
	private $plugin_data;

	/**
	 * @param $key
	 *
		 * @return string
		 */
	public function get_plugin_data($key): string {
		return $this->plugin_data[$key];
	}

	/**
	 * NeoWeb_OSM_oAuth_Connector_AJAX_Callers constructor.
	 *
	 */
	public function __construct()
	{
		$this->plugin_data = get_option('neoweb-connector-waiting-list-manager');
		$this->flashNotice = new NeoWeb_Connector_Admin_Notifications();
		$this->licenceManager = new NeoWeb_Connector_Licence_Manager();
		$this->logger = new NeoWeb_Connector_Loggers(
			plugin_dir_path( dirname( __FILE__ ) )
		);
		$this->transientManager = new NeoWeb_Connector_Waiting_List_Manager_Transient_Manager($this->get_plugin_data('pluginSlug') . "_osm");
		$this->oAuthCaller = new NeoWeb_Connector_Waiting_List_Manager_Auth_Caller();
	}

	public function trigger_create_licence_key_request() {

        $firstName = get_field($this->get_plugin_data('productSlug') . '_first_name', 'option');
        $lastName = get_field($this->get_plugin_data('productSlug') . '_last_name', 'option');
        $email = get_field($this->get_plugin_data('productSlug') . '_email_address', 'option');
        $scout_groupdistrictcounty = get_field($this->get_plugin_data('productSlug') . '_organisation', 'option');

		if ($firstName && $lastName && $email) {
			//We have valid personal details lets create a new licence key
			$licenceRequestBody = array (
				'secret_key' => '',
				'slm_action' => 'slm_create_new',
				'first_name' => $firstName,
				'last_name' => $lastName,
				'email' => $email,
				'company_name' => $scout_groupdistrictcounty,
				'max_allowed_domains' => '1',
				'date_created' =>date("Y-m-d"),
				'date_expiry' => '',
				'product_ref' => urlencode($this->get_plugin_data('pluginSlug'))

			);

			$licenceRequestResponse = $this->licenceManager->createLicenceKey($licenceRequestBody);

			// Check for error in the response
			if (is_wp_error($licenceRequestResponse)){
				$this->flashNotice->add_flash_notice("Unexpected Error! The query returned with an error.",
					"error", false);
			}

			// License data.
			$license_data = json_decode(wp_remote_retrieve_body($licenceRequestResponse));

			if($license_data->result == 'success'){

				update_field($this->get_plugin_data('pluginSlug') . '_licence_key', $license_data->key, "option");

				$licenceStatus = $this->licenceManager->getLicenceStatus();

				update_field($this->get_plugin_data('pluginSlug') . '_licence_status', $licenceStatus, "option");

				$this->flashNotice->add_flash_notice($license_data->message,
					"success", false);

			} else {

				$this->flashNotice->add_flash_notice($license_data->message,
					"error", false);

			}
		} else {
			//We do not have valid personal details, throw and error
			$this->flashNotice->add_flash_notice("You have not completed your personal details. Please complete all 
				mandatory fields and click 'Save personal details' before requesting a licence key",
				"error", false);
		}
	}

	public function trigger_activate_licence_key_request() {

		$licenceRequestBody = array(
			'slm_action' => 'slm_activate',
			'secret_key' => '',
			'license_key' => '',
			'registered_domain' => $_SERVER['SERVER_NAME']
		);

		$licenceRequestResponse = $this->licenceManager->activateLicenceKey($licenceRequestBody);

		// Check for error in the response
		if (is_wp_error($licenceRequestResponse)){
			$this->flashNotice->add_flash_notice("Unexpected Error! The query returned with an error.",
				"error", false);
		}

		$license_data = json_decode(wp_remote_retrieve_body($licenceRequestResponse));
		if($license_data->result == 'success'){//Success was returned for the license activation
			$licenceStatus = $this->licenceManager->getLicenceStatus();
			update_field($this->get_plugin_data('pluginSlug') . '_licence_status', $licenceStatus, "option");

			$this->flashNotice->add_flash_notice($license_data->message,
				"success", false);

		}
		else{
			//Show error to the user. Probably entered incorrect license key.
			$this->flashNotice->add_flash_notice($license_data->message,
				"error", false);

			if (strpos( $license_data->message, 'Reached maximum activation' ) > 0) {
				update_field($this->get_plugin_data('pluginSlug') . '_licence_key', "", "option");
				update_field($this->get_plugin_data('pluginSlug') . '_licence_status', "Activation Failed - Request a new key", "option");
			}
		}
	}

	public function trigger_log_refresh() {
		$this->deleteFolderAndContents("osmDebugLogs");

		$this->flashNotice->add_flash_notice("Debug log cleared successfully.",
			"success", false);

	}

	public function trigger_transient_log_refresh() {

		$this->deleteFolderAndContents("osmTransientDebugLogs");

		$this->flashNotice->add_flash_notice("Transient debug log cleared successfully.",
			"success", false);

	}

	public function trigger_cache_refresh() {
		if (get_field($this->get_plugin_data('pluginSlug') . '_enable_debug_logs', 'option')) {
			//Delete all transients
			$this->logger->debug_logger( "Transient CACHE clear started.. " );
			$this->logger->debug_logger(
				$this->transientManager->wds_delete_transients()
			);
		}

		$this->flashNotice->add_flash_notice("WordPress transient cache cleared successfully.",
			"success", false);
	}

	private function deleteFolderAndContents ($folderName) {
		$dirname = plugin_dir_path( dirname( __FILE__ ) ) . "/" . $folderName;
		$dir_handle = "";

		if (is_dir($dirname))
			$dir_handle = opendir($dirname);
		if (!$dir_handle)
			return;
		while($file = readdir($dir_handle)) {
			if ($file != "." && $file != "..") {
				if (!is_dir($dirname."/".$file))
					unlink($dirname."/".$file);
				else
					$this->delete_directory($dirname.'/'.$file);
			}
		}
		closedir($dir_handle);
	}

	private function delete_directory($dirname) {
		$dir_handle = "";

		if (is_dir($dirname))
			$dir_handle = opendir($dirname);
		if (!$dir_handle)
			return;
		while($file = readdir($dir_handle)) {
			if ($file != "." && $file != "..") {
				if (!is_dir($dirname."/".$file))
					unlink($dirname."/".$file);
				else
					delete_directory($dirname.'/'.$file);
			}
		}
		closedir($dir_handle);
		rmdir($dirname);
	}

	public function trigger_api_test() {
		//"https://www.onlinescoutmanager.co.uk/api.php?action=getTerms"
		$response = $this->oAuthCaller->testCallAPI(
			"https://www.onlinescoutmanager.co.uk/api.php?action=getUserRoles"
		);

		if (wp_remote_retrieve_response_code($response) == 429) {
			$message = array(
				'status' => 'ERROR: ' . wp_remote_retrieve_response_message($response),
			);
			wp_send_json_error($message);
		} else if (wp_remote_retrieve_response_code($response) == 200) {
			$body = wp_remote_retrieve_body($response);
			$message = array(
				'status' => 'API STATUS: ' . wp_remote_retrieve_response_message($response),
				'message' => $body
			);
			wp_send_json($message);
		}
		wp_die();
	}

	public function fetch_osm_waiting_list_data() {

		$sectionID = $_POST['sectionid'];
		$searchTerm = $_POST['search'];

		$currentTermID = -1;

		//getAllMembers
		$url = NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints::getAllMembers;
		$formattedURL = (new NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $currentTermID, "waiting");

		$transientID = 'waitingList_data_' . $sectionID;
		$allMemberData = json_decode($this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 48, array()), true);

		$filteredList =  new ArrayObject();
		foreach ($allMemberData['items'] as $key=>$yp) {
			$ypArry = new ArrayObject();
			if ( strpos(strtolower($yp['lastname']), strtolower($searchTerm)) !== false || strpos(strtolower($yp['firstname']), strtolower($searchTerm)) !== false  ) {

				$transientID = "yp_" . $yp['scoutid'] . "_custom_fields_" . $sectionID;
				$getCustomFields['associated_id'] = $yp['scoutid'];
				$getCustomFields['associated_type'] = "member";
				$getCustomFields['context'] = "member";
				$getCustomFields['group_order'] = "section";

				//TODO: Add custom fields back in

				/*
				//getAllMembers
				$url = NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints::getAllMembers;
				$formattedURL = (new NeoWeb_Connector_Waiting_List_Manager_OSM_Endpoints)->formatEndPoint($url, $sectionID, $currentTermID, "waiting");

				$transientID = 'waitingList_data_' . $sectionID;
				$allMemberData = json_decode($this->oAuthCaller->osmAPICaller($transientID, $formattedURL, 48, array()), true);
				$this->logger->error_logger($allMemberData);

				$summaryCustomData = perform_osm_api_call($transientID, $domain . '/ext/customdata/?action=getData&section_id='. $sectionID,
					48, $getCustomFields);

				$osm_fields = json_decode($summaryCustomData, true);

				$customFields = get_field('show_custom_fields', 'options');

				if (is_array($osm_fields) || is_object($osm_fields)) {
					foreach ($osm_fields['data'] as $customData) {
						foreach ($customData['columns'] as $key => $field) {
							if ($field['is_core'] == 'no') {
								$customFieldID = explode(":", $customFields["show_custom_fields_" . $sectionID]);
								if ($field['varname'] == $customFieldID[0]) {
									$ypArry['customField'] = $field['value'];
								}
							}
						}
					}
				}*/

				$ypArry['lastname'] = $yp['lastname'];
				$ypArry['firstname'] = substr($yp['firstname'], 0, 2) . "******";
				$ypArry['dob'] = substr($yp['dob'], 0, -2) . "**";
				$ypArry['joined'] = $yp['joined'];

				$filteredList->append($ypArry);
			}
		}

		if (count($filteredList) > 0) {
			//$summary = json_encode($filteredList, true);
			wp_send_json( $filteredList );
		} else {
			echo "No Results found: ";
		}

	}
}