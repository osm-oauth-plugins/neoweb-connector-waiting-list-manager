(function( $ ) {
	'use strict';

	$(function() {

		const productSlug = "neoweb_connector_waiting_list_manager"
		$('#'+ productSlug +'triggerAPITest').on('click', function() {
			$(this).prop('disabled', true);
			var data = {
				'action': 'trigger_api_test_' + productSlug
			};

			$.post(ajaxurl, data, function(data) {
				console.log(data);
				alert(data['status']);
			});
		});

		$('#'+ productSlug +'triggerLogRefresh').on('click', function() {
			$(this).prop('disabled', true);
			var data = {
				'action': 'trigger_log_refresh_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		$('#'+ productSlug +'triggerTransientLogRefresh').on('click', function() {
			$(this).prop('disabled', true);
			var data = {
				'action': 'trigger_transient_log_refresh_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		$('#'+ productSlug +'triggerCacheRefresh').on('click', function() {
			$(this).prop('disabled', true);
			var data = {
				'action': 'trigger_cache_refresh_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		$('#'+ productSlug +'_register').on('click', function() {
			$(this).prop('disabled', true);
			const data = {
				'action': 'trigger_create_licence_key_request_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		$('#'+ productSlug +'_activate').on('click', function() {
			$(this).prop('disabled', true);
			const data = {
				'action': 'trigger_activate_licence_key_request_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		//Disable permission checkboxes for the moment
		$('div.disabled').each(function () {
			$(this).find('input:radio:not(:checked)').prop('disabled', true);
		});


		//Hide ACF fields to prevent accidental changes
		$('.post_name').each(function () {
			const $customField = $(this);
			const $customPages = [
				"group_waiting_list_defaults",
				"group_defaults_osm_custom_fields"
			];
			$customPages.forEach(function (item, index) {
				if ($customField.text() === item) {
					console.log("Found " + item);
					//$customField.parent().parent().parent().hide();
				}
			});
		});


	});

})( jQuery );
